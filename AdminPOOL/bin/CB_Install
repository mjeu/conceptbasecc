#! /bin/sh
#
# File:         CB_Install
# Creation:     2-Apr-2009, Manfred Jeusfeld (UvT)
# Last Change:  14-Mar-2024, Manfred Jeusfeld (UvT)
#
#-------------------------------------
#
# Call: CB_install [<dir>] 
#   dir: target directory into which the files are installed; default is $CB_WORK/../conceptbase
#
# Script to install the files necessary for an executable ConceptBase product.
# Adapted from utils/CB_ExportProduct
#



CB_POOL=$CB_WORK

CB_PRODUCT=$CB_POOL/../conceptbase  # default target

if [ -n "$1" ]; then
   CB_PRODUCT=$1
fi

# echo Installing to $CB_Product ...


EXPDIR=EXPORT
#VARIANTS="i86pc sun4 linux linux64 windows mac"
VARIANTS="`CBvariant`"   # only the local variant is installed

# Required directories of CB_Product
PRODUCT_DIRS="include \
	bin \
	doc \
	doc/UserManual \
	doc/Tutorial \
	doc/TechInfo \
	doc/ExternalLicenses \
	doc/ProgManual \
	examples \
	examples/Clients \
	examples/Clients/PrologClient \
	examples/Clients/SWI-CLIENT \
	examples/Clients/C++Client \
	examples/Clients/JavaClient \
	examples/Clients/C_Client \
	examples/FLIGHT \
	examples/QUERIES \
	examples/RULES+CONSTRAINTS \
	examples/ER_MODEL \
	examples/BuiltinQueries \
	examples/ECArules \
	examples/Modules \
	examples/MetaFormulas \
	examples/Servlets \
	examples/AnswerFormat \
	examples/libtelos \
	examples/ontology \
	i86pc \
	i86pc/bin \
	i86pc/lib \
	lib \
	lib/classes \
	lib/system \
	lib/SystemDB \
	sun4 \
	sun4/bin \
	sun4/lib \
	linux \
	linux/bin \
	linux/lib \
	linux64 \
	linux64/bin \
	linux64/lib \
	windows \
	windows/bin \
	windows/lib \
	mac \
	mac/bin \
	mac/lib" 

# Die Verzeichnisse aus denen die Export-Files einfach nach
# CB_PRODUCT kopiert werden koennen
NORMAL_EXPORT="doc/UserManual \
	doc/ProgManual \
	doc/TechInfo \
	doc/ExternalLicenses \
	doc/Tutorial \
	doc \
	examples \
	examples/AnswerFormat \
	examples/Clients/JavaClient \
	examples/Clients/PrologClient \
	examples/Clients/SWI-CLIENT \
	examples/Clients/C++Client \
	examples/Clients/C_Client \
	examples/libtelos \
	examples/ECArules \
	examples/FLIGHT \
	examples/ER_MODEL \
	examples/RULES+CONSTRAINTS \
	examples/Modules \
	examples/QUERIES \
	examples/BuiltinQueries \
	examples/MetaFormulas"

for dir in $PRODUCT_DIRS
do
        mkdir -p $CB_PRODUCT/$dir
done

# Das Wichtigste: CBserver exe und lib
for CB_VARIANT in $VARIANTS
do
  if [ -d $CB_POOL/$CB_VARIANT/serverSources/$EXPDIR ]; then
	cp $CB_POOL/$CB_VARIANT/serverSources/$EXPDIR/CBserver* $CB_PRODUCT/$CB_VARIANT/bin
  fi
  if [ -d $CB_POOL/$CB_VARIANT/bin/$EXPDIR ]; then
	cp $CB_POOL/$CB_VARIANT/bin/$EXPDIR/* $CB_PRODUCT/$CB_VARIANT/bin
  fi
done

# SYSTEM files (builtin O-Telos objects) are platform-independent
cp $CB_POOL/lib/$EXPDIR/SYSTEM* $CB_PRODUCT/lib/system

# create a plain database with just the SYSTEM files
cp $CB_PRODUCT/lib/system/SYSTEM.SWI.telos $CB_PRODUCT/lib/SystemDB/OB.telos
cp $CB_PRODUCT/lib/system/SYSTEM.SWI.symbol $CB_PRODUCT/lib/SystemDB/OB.symbol
cp $CB_PRODUCT/lib/system/SYSTEM.SWI.rule $CB_PRODUCT/lib/SystemDB/OB.rule
cp $CB_PRODUCT/lib/system/SYSTEM.SWI.ruleinfo $CB_PRODUCT/lib/SystemDB/OB.ruleinfo
cp $CB_PRODUCT/lib/system/SYSTEM.SWI.ecarule $CB_PRODUCT/lib/SystemDB/OB.ecarule


# copy logos and files for desktop integration
cp $CB_POOL/lib/$EXPDIR/cb*logo.* $CB_PRODUCT/lib
cp $CB_POOL/lib/$EXPDIR/*.svg $CB_PRODUCT/lib
cp $CB_POOL/lib/$EXPDIR/cb*.xml $CB_PRODUCT/lib

# copy GEL files
cp $CB_POOL/lib/$EXPDIR/*.gel $CB_PRODUCT/lib

# copy lpi files
# cp $CB_POOL/lib/$EXPDIR/*.lpi $CB_PRODUCT/lib


cp $CB_POOL/bin/$EXPDIR/* $CB_PRODUCT/bin
chmod 755 $CB_PRODUCT/bin/*

# C/C++ programming interface
cp $CB_POOL/clientSources/libCBview/CB*.h $CB_PRODUCT/include
cp $CB_POOL/clientSources/libCB/CB*.h $CB_PRODUCT/include
cp $CB_POOL/serverSources/C_Files/libtelos/*.h $CB_PRODUCT/include


# Normaler Export: Einfach kopieren
for dir in $NORMAL_EXPORT
do
  if [ -d $CB_POOL/$dir/$EXPDIR ]; then
	cp -r $CB_POOL/$dir/$EXPDIR/* $CB_PRODUCT/$dir
  fi
done

# Java-Jars kopieren
cp $CB_POOL/java/classes/cb.jar $CB_PRODUCT/lib/classes
# Externe JARs kopieren
cp $CB_POOL/java/lib/*.jar $CB_PRODUCT/lib/classes


# Copy README.txt etc. to root directory of ConceptBase
cp $CB_POOL/$EXPDIR/*.txt $CB_PRODUCT/

# Copy icons and desktop files to root directory of ConceptBase
cp $CB_POOL/lib/$EXPDIR/cb*.ico $CB_PRODUCT/
cp $CB_POOL/lib/$EXPDIR/cb*.png $CB_PRODUCT/
cp $CB_POOL/lib/$EXPDIR/cb*.desktop $CB_PRODUCT/
cp $CB_POOL/lib/$EXPDIR/cb*.desktop $CB_PRODUCT/
cp $CB_POOL/lib/$EXPDIR/cb*mime.xml $CB_PRODUCT/
cp $CB_POOL/lib/$EXPDIR/application*.svg $CB_PRODUCT/


# Copy (new) script files to the root of the ConceptBase installation directory
cp $CB_POOL/$EXPDIR/cb* $CB_PRODUCT/
cp $CB_POOL/$EXPDIR/make-* $CB_PRODUCT/
cp $CB_POOL/$EXPDIR/updateCB* $CB_PRODUCT/
chmod 755 $CB_PRODUCT/cb*


# Libraries, Header-Files und Beispiele
for libdir in libCB libCBview libtelos
do
    # Copy libraries
    for variant in $VARIANTS
	do
	if [ -f $CB_POOL/$variant/clientSources/$libdir/$EXPDIR/*.a ]; then
      		cp $CB_POOL/$variant/clientSources/$libdir/$EXPDIR/*.a $CB_PRODUCT/$variant/lib/
	fi
	if [ -f $CB_POOL/$variant/clientSources/$libdir/$EXPDIR/*.dll ]; then
      		cp $CB_POOL/$variant/clientSources/$libdir/$EXPDIR/*.dll $CB_PRODUCT/$variant/lib/
	fi
	if [ -f $CB_POOL/$variant/clientSources/$libdir/$EXPDIR/*.lib ]; then
      		cp $CB_POOL/$variant/clientSources/$libdir/$EXPDIR/*.lib $CB_PRODUCT/$variant/lib/
	fi
        done

    # Copy header files (they are in the EXPORT-Directory of sun4 )
    if [ -f $CB_POOL/sun4/clientSources/$libdir/$EXPDIR/*.h ]; then
	cp $CB_POOL/sun4/clientSources/$libdir/$EXPDIR/*.h $CB_PRODUCT/include
    fi
done

# Copy example files 
cp $CB_POOL/examples/Clients/C_Client/README $CB_PRODUCT/examples/Clients/C_Client
cp $CB_POOL/clientSources/libCB/testlib.c $CB_PRODUCT/examples/Clients/C_Client

cp $CB_POOL/examples/Clients/C++Client/README $CB_PRODUCT/examples/Clients/C++Client
cp $CB_POOL/clientSources/libCBview/testlib.cc $CB_PRODUCT/examples/Clients/C++Client

cp $CB_POOL/examples/Clients/JavaClient/*.java $CB_PRODUCT/examples/Clients/JavaClient
cp $CB_POOL/examples/Clients/JavaClient/README $CB_PRODUCT/examples/Clients/JavaClient

cp $CB_POOL/examples/Clients/SWI-CLIENT/*.pl $CB_PRODUCT/examples/Clients/SWI-CLIENT
cp $CB_POOL/examples/Clients/SWI-CLIENT/*.txt $CB_PRODUCT/examples/Clients/SWI-CLIENT

#cp $CB_POOL/examples/Clients/PrologClient/*.pro $CB_PRODUCT/examples/Clients/PrologClient
#cp $CB_POOL/examples/Clients/PrologClient/*.o $CB_PRODUCT/examples/Clients/PrologClient
#cp $CB_POOL/examples/Clients/PrologClient/README $CB_PRODUCT/examples/Clients/PrologClient
#cp $CB_POOL/examples/Clients/PrologClient/goExClient $CB_PRODUCT/examples/Clients/PrologClient

#cp $CB_POOL/examples/Clients/TclClient/*.tcl $CB_PRODUCT/examples/Clients/TclClient
#cp $CB_POOL/examples/Clients/TclClient/README $CB_PRODUCT/examples/Clients/TclClient

cp -r $HOME/ConceptBase-Distros/CB-LATEST/CBICONS $CB_PRODUCT/


# copy required shared libraries
# cp $CB_POOL/lib/$EXPDIR/lib*so* $CB_PRODUCT/lib


# SWI und STLPort DLLs kopieren
#cp /home/cbase/MSVC/VC98/Bin/stlport_vc64*.dll $CB_PRODUCT/windows/bin
#cp /home/prolog/SWI/windows/bin/libpl.dll $CB_PRODUCT/windows/bin
#cp /home/prolog/SWI/windows/bin/pthreadVC.dll $CB_PRODUCT/windows/bin

