<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
<TITLE>What is ConceptBase</TITLE>
<LINK REL="SHORTCUT ICON" HREF="PIC/cbico.ico">
<link rel="stylesheet" type="text/css" href="cb.css">
</HEAD>
<BODY>


<EM>
<A HREF=".">ConceptBase Home</A>,
RWTH Aachen University, Germany<br>
</EM>
<HR>


<CENTER>
<H1>
                What is ConceptBase?
</H1>

<H4>
            Manfred A. Jeusfeld, Christoph Quix<br>
                RWTH Aachen, Informatik V, Germany<br>
                1998-2008<br>
</H4>
</CENTER>



<H2>
Introduction: Applications for ConceptBase
</H2>

ConceptBase means "base of concepts" which suggests that one
can use it to store and query collections of conceptual descriptions.
This is true, however we refer to ConceptBase as a deductive object
base manager because it inherited features from both object-oriented
databases and deductive databases.
<p>
The data model of ConceptBase is <A HREF="O-Telos.html">O-Telos</A>, 
a language derived from Telos,
which itself was developed by John Mylopoulos, Alex Borgida, Sol Greenspan, Manolis Koubarakis,
and others in the 2nd half of the 1980's. 
<p>
This text is aimed at potential users of ConceptBase and intends to answer
the following questions:
<OL>
<li> For what applications is ConceptBase not a good tool?
<li> For what applications is ConceptBase a good tool?
<li> For what applications has ConceptBase been used?
</OL>



<H2>
Historical remarks
</H2>

We started development of ConceptBase in 1987 at the University of Passau.
Version 1.0 was available in early 1988. Thus, ConceptBase was one of
the first deductive object managers. Version 2.0 appeared in November
1989 and added deductive integrity checking plus the client server
architecture. A few months later an interval-based time calculus was
included. Version 3.0 was released in 1991 to provide query classes for
information retrieval and a X11-based user interface.
In 1992, the ConceptBase development team
moved from Passau to the RWTH Technical University of Aachen.
Version 4.0 was finished in 1994 and then upgraded to 4.1 after one year.
It provided a dedicated object store and boosted performance. For the
first time, real meta-level rules and constraints were made available.
Version  5.0  featured complex views, active rules,
and an improved query optimizer. Version 6.1 (2003) featured a new
query evaluation engine. In 2007, ConceptBase 7.0 was released with a
significant efficiency improvement and with an extended engine for
active rules. Version 7.1, released in 2008, introduced functional
expressions and further improved the meta modeling capabilities.
ConceptBase is available free of charge for non-commercial purposes.



<H2>
Technologies used in ConceptBase with Credits to Contributors
</H2>

ConceptBase relies on the following
core technologies for object storage, query & rule
processing, integrity maintenance, meta modeling, and connectivity.
<p>
The <em>object store</em> (Rainer Gallersd&ouml;rfer, Thomas List)
 of ConceptBase is an abstract data type that
represents so-called P-tuples, the one and only base relation of
ConceptBase. Each P-tuple object stores redundantly links to
objects that are connected by any relationship to it. As a consequence,
an object frame with all its attributes and all objects that refer to
this object can be fetched in constant time. The object store
follows a main memory approach, i.e. all objects are cached into the
main memory of the computer.
<p>
The <em>query and rule processor</em> (Martin Staudt, Rene Soiron) provides the functionality
of DATALOG with negation, i.e. recursive deductive rules with
stratification on negated predicates. Evaluation is done using
the Magic Cache method (Manfred Jeusfeld). Queries are treated as classes (query classes), whose
instances are the answer to the query. Semantic query optimization
and join-ordering optimization are used to further improve the performance
of query evaluation. The answer presentation can be configured by so-called
answer formats (Hua Wang, Hans Nissen).
<p>
The <em>integrity checker</em> (Manfred Jeusfeld, Eva Kr&uuml;ger)
maintains the consistency of
the database by evaluating triggers generated from declarative
logical integrity formulas. The method originates from the simplification
idea by Nicolas and Blaustein. The trigger code base is maintained whenever
a deductive rule or integrity constraint is changed. The triggers are
activated by insert and delete events in the object store.
<p>
Finally, the <em>meta modeling</em> approach (Matthias Jarke, Thomas Rose,
Manfred Jeusfeld, Hans Nissen and others) is used to adapt ConceptBase
to various application domains. Meta classes as well as meta meta classes
can be defined to provide generic concepts of the application domain.
The syntax and the semantics of the generic concepts is defined by meta-level
rules and integrity constraints (Rene Soiron).
<p>
The <em>connectivity</em> (Martin Staudt, Christoph Quix, Christoph Radig, Manfred Jeusfeld)
of ConceptBase relies on Internet protocols.
The server accepts TELL and ASK messages. Clients connect via a library
or simply via the HTTP protocol. Clients also can be notified by the
server via a <em>view maintenance</em> paradigm. The user interface
is used to be based on Tcl/Tk (Michael Gebhardt, Ralf St&ouml;ssel) with
the notable exception of the graph browser (Stefan Eherer, Markus Baumeister,
Michael Gebhardt). The current standard user interface is written in Java
(Michael Gebhardt, Rainer Langohr, Achim Schlosser, Rainer Hermanns, and others).
<p>
The <em>view component</em> (Christoph Quix) extends the capabilities of query
classes by nesting and additional attribute categories. Views can automatically
be maintained.
<em>Functional programming</em> (Manfred Jeusfeld) is an extension for the
definition of functions that compute numeric or non-numeric values by analyzing
the database. 
<p>
ConceptBase also provides object base <em>modules</em> (Hans Nissen, Lutz Bauer),
a facility to organize portions of the database that can only refer
to objects within a module (plus its supermodules), and to objects
imported from other modules.  Furthermore, it includes <em>active rules</em>
(Hans Nissen, Farshad Lashgari; extended later by Manfred Jeusfeld) as a facility
to trigger internal or external processes whenever a certain update event happens.
<p>
Until version 3.0 ConceptBase was a full <em>temporal database system</em>
(Thomas Wenig) using Allen's interval algebra to reason on validty of statements.
Any object in the database had a validity time (a symbolic name for
the time it should be regarded as valid in the world), and a belief time
(denoting the time when the object is visible to the query processor).
Since ConceptBase has a sophisticated deductive component,
we discovered too many
undesired interactions between the temporal algebra and deductive rules,
esp. those defining the semantics of inheritance. Thus, we dropped the
validity time but kept the belief time.
<p>
Another historic component worth mentioning is the <em>view subsumption tester</em>
(Kai von Thadden).
It computes containment relations between view/query definitions. Due to
inherent undecidability, the method is incomplete. However it is complete
for decidable subclasses that have been investigated in description logics
(KL-One-like languages) and conjunctive queries.


<H2>
Applications where ConceptBase should not be used
</H2>

ConceptBase does not combine all features from object-oriented and
deductive databases. Especially, it fails to handle very large amounts
of data. Object bases exceeding the size of the main memory are currently
too big to be handled efficiently, as ConceptBase is a main memory database.
This prohibits to use ConceptBase for mass storage. Currently, ConceptBase can
handle up to one million concept definitions. 
<p>
Object-oriented (or post-relational) database systems are strong in integrating 
operations ("methods")
into the database. ConceptBase has no general methods except integrity
constraints, deductive rules, and active rules. It does have a programming interface but
this cannot be compared to what one gets from post-relational database systems.
<p>
ConceptBase currently has only primitive provisions for multi-user
support, and recovery.  Thus, if you need a system which is strong in these
points then you probably need a commercial database system.
<p>
ConceptBase has a strongly developed logical component for expressing queries,
rules, and constraints. This component is integrated into the object-oriented
data model derived from Telos. If your application is not requiring abstraction
principles such as classification, specialization and attribution, you might
be better off with a state-of-the-art deductive system such as XSB.


<H2>
Applications where ConceptBase is strong
</H2>

The most powerful feature of ConceptBase is its arbitralily high class
hierarchy, i.e., O-Telos allows to describe objects, classes, meta-classes,
meta-meta-classes etc. within the same framework. Thereby, the modeling
capabilities of ConceptBase can be adapted to the application by defining
appropriate meta-classes.
<p>
This feature is accompanied by a query facility which has the expressive
power of deductive databases and which is seamlessly integrated into
the frame language [JEUS92].
<p>
Consequently, ConceptBase is strong in applications where concepts (classes,
objects, situations, entities etc.) have to be modelled and manipulated.
The user interface supports this by textual and graphical browsers and
editors. The graphical editor can be adapted to the application by
assigning graphical representations to the concepts (e.g., each instance
of class RelationShip is displayed as a romboid).
<p>
The object model, O-Telos, already contains the well-known
knowledge representation paradigms for specialization, aggregation, and
classification. The designer can augment them by user-defined deductive
rules (e.g., closing certain attributes transitively) and integrity constraints
(e.g., forbidding certain attributes to be cyclic). The ability of ConceptBase
to accept deductive rules and integrity constraints at any (meta) class
level is unique. It basically allows you to represent heterogeneous (data)
modeling languages within the same sound formalism. We call this the
meta modeling capability of ConceptBase.



<H2>
Applications where ConceptBase was successful
</H2>

ConceptBase has first been used in the DAIDA project [JARK93] as a repository
for design objects, design decisions, and environment tools for developing
database applications. For that purpose, three meta-classes DesignObject,
DesignDecision, and DesignTool (the so-called software process data model)
have been defined to adapt ConceptBase for this application. ConceptBase
was also the tool which integrated the heterogeneous tools in the design
environment.
<p>
In the Canadian network of Excellence, ConceptBase was chosen to implement
a more sophisticated software repository [RJM92] which supports versioning and
configuration of software items (specifications, designs, implemenations,
documents etc.).
<p>
Another area of application is representation, maintenance, and discussion
of process knowledge [RL93]. There, ConceptBase serves as a tool for capturing
and reasoning with such knowledge. The goal of the system is to help
software engineers in rapidly constructing prototypes of proposed software
systems.
<p>
ConceptBase is also used for teaching, e.g. at the Aalborg University in
Denmark, University of Lausanne, University of Hamburg, and University of Muenster.
Furthermore, it is curently used in several ESPRIT and national
projects for applications like semantic query optimization (project COMPULOG),
industrial quality management (project WIBQUS), retrieval of reusable software
components (project KORSO), hypermedia authoring systems (project MULTIWORKS),
and requirements engineering and process modelling (project NATURE).
<p>
A couple of Web applications with ConceptBase came up in the late 1990's. The HTML form
language provides entry a collection of labelled fields which nicely corresponds
to the frame representation of objects in ConceptBase. 


<H2>
4. References
</H2>

[JEUS92] M. Jeusfeld (1992). &Auml;nderungskontrolle in deduktiven
         Objektbanken. Infix-Verlag, St. Augustin, Germany, 1992.<p>

[JARK93] M. Jarke (ed.,1993). Database application engineering with DAIDA.
         Springer-Verlag, 1993.<p>

[RJM92]  T. Rose, M. Jarke, J. Mylopoulos (1992). Organizing software
         repositories - modeling requirements and implementation experiences.
         Proc. 16th Intl. Computer Software & Applications Conf., Chicago,
         Ill., Sept. 23-25, 1992.<p>

[RL93]   B. Ramesh, Luqi (1993). Process knowledge based rapid prototyping for
         requirements engineering. Proc. IEEE Intl. Symp. Requirements
         Engineering (RE'93), San Diego, Ca., Jan. 4-6, 1993.<p>

[NIX94]  B.A. Nixon (1994): Representing and using performance requirements
         during the development of information systems. Proc. 4th Intl. Conf.
         EDBT'94, LNCS, 779, Springer-Verlag, 1994. <p>

[GHS94]  S. Gastinger, R. Hennicker, R. Stabl (1994): Reusing software components
         with polymorphic signatures. Technical report (KORSO project),
         LMU Muenchen, Institut Informatik, D-80802 Muenchen, Germany, 1994. <p>

<br>
More references are
<A HREF="cblit.html">here</A>.

<hr>
<P><I>&copy; ConceptBase Team 2008. Please do not mirror this document
or its parts without prior permission by us. Thank you! Last update: $Author: jeusfeld $, $Date: 2008/06/03 18:16:56 $</I></P>

</BODY>
</HTML>
