<HTML>
<HEAD>
<TITLE>ConceptBase now under a FreeBSD-style license ...</TITLE>
<META HTTP-EQUIV="Refresh" CONTENT="4; URL=http://www-i5.informatik.rwth-aachen.de/CBdoc/cbaccess.html">
</HEAD>
<BODY>
<pre>
ConceptBase is now distributed under a FreeBSD-style license. You are being redirected to the new
download page ...

Tilburg, 2009-07-20
M. Jeusfeld
</pre>
</BODY>
