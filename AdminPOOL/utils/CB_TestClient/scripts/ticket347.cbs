# Test solution to Ticket 347:  attribute expansion query find_attribute_values
# used by CBGraph
# Call: cbshell ticket347.cbs
#
# Ticket 347: 
# A wrong (explicit) attribute for another category is displayed when expanding the derived valued for an attribute category.
# 
# The find_attribute_values query is formed in CBTree.java and CBQuery.java. In the latter, the category
# 'Attribute' appears hardcoded in the answer processing of ask().
#
# To reproduce the bug, start a CBserver and tell the definitions in the above CBShell script via CBIva.
#
# Then, open a graph editor with start object 'john'. Show outgoing attributes of john, attribute category 'knows', 'show all'.
#
# ---> bill should appear.
#
# Then, do the same with bill.
#
# --> the explicit p1 attribute from bill to john is displayed. However, this explicit attribute has the category 'hasAncestor'.
#
# The bug is due to using the wrong attribute category 'Attribute' in the format CBGraphEditorResult produced in CBTree.java and CBQuery.java.
#
# In CBQuery, the answer as scanned only for attributes of category Attribute. If another category was used, an empty result set was produced.
# Now, we also allow any other attribute category (check by looking for the character '!'). 
#



startServer -u nonpersistent -t low -port 4002 


tell "
Proposition with  
  attribute
    reflexive : Proposition;
    transitive : Proposition;
    symmetric : Proposition;
    antisymmetric : Proposition;
    asymmetric : Proposition
end 

RelationSemantics in Class with  
  constraint
    asym_IC : $ forall AC/Proposition!asymmetric C/Proposition x,y/VAR M/VAR
                     P(AC,C,M,C) and (x in C) and (y in C) and
                     (x M y)  ==> not (y M x) $;
    antis_IC : $ forall AC/Proposition!antisymmetric C/Proposition x,y/VAR M/VAR
                     P(AC,C,M,C) and (x in C) and (y in C) and
                     (x M y) and (y M x)  ==> (x = y) $
  rule
    trans_R : $ forall x,z,y,M/VAR 
                     AC/Proposition!transitive C/Proposition
                     P(AC,C,M,C) and (x in C) and (y in C) and (z in C) and
                     (x M y) and (y M z) ==> (x M z) $;
    refl_R : $ forall x,M/VAR 
                    AC/Proposition!reflexive C/Proposition
                    P(AC,C,M,C) and (x in C)
                      ==> (x M x) $;
    symm_R : $ forall x,y,M/VAR 
                    AC/Proposition!symmetric C/Proposition
                    P(AC,C,M,C) and (x in C) and (y in C) and
                    (x M y)  ==> (y M x) $
end 

"
result OK yes

# Person has two different attributes, knows and hasAncestor
tell "

Person in Class with  
  symmetric,reflexive,transitive
    knows : Person
  asymmetric,transitive
    hasAncestor : Person
end 
"
result OK yes

# explicit knows link from john to bill
# explicit ancestor link from bill to john
# (bill knows john) should be a derived attribute
tell "
john in Person with  
  knows
    p1 : bill
end 

bill in Person with  
  hasAncestor
    p1 : john
end 

"
result OK yes


# this query is used by GraphEditor to display all 'knows' attributes of bill including the derived ones
# wrongly uses Attribute as category in the CBGraphEditorResult format
ask find_attribute_values[bill/objname,Person!knows/cat] OBJNAMES CBGraphEditorResult[DefaultJavaPalette/pal,bill/obj,Attribute/cat,src/objtype]
result OK "<result>
  <object>
    <name>bill</name>
    <graphtype>DefaultIndividualGT</graphtype>
    <edges>
    </edges>
  </object>


  <object>
    <name>john</name>
    <graphtype>DefaultIndividualGT</graphtype>
    <edges>
      <object>
        <name>bill!p1</name>
    <graphtype>DefaultAttributeGT</graphtype>
      </object>
    </edges>
  </object>

</result>
"


# this should be the right query
ask find_attribute_values[bill/objname,Person!knows/cat] OBJNAMES CBGraphEditorResult[DefaultJavaPalette/pal,bill/obj,Person!knows/cat,src/objtype]
result OK "<result>
  <object>
    <name>bill</name>
    <graphtype>DefaultIndividualGT</graphtype>
    <edges>
    </edges>
  </object>


  <object>
    <name>john</name>
    <graphtype>DefaultIndividualGT</graphtype>
    <edges>
    </edges>
  </object>

</result>
"


# same query from the view of john
ask find_attribute_values[john/objname,Person!knows/cat] OBJNAMES CBGraphEditorResult[DefaultJavaPalette/pal,john/obj,Person!knows/cat,src/objtype]
result OK "<result>
  <object>
    <name>bill</name>
    <graphtype>DefaultIndividualGT</graphtype>
    <edges>
      <object>
        <name>john!p1</name>
    <graphtype>DefaultAttributeGT</graphtype>
      </object>
    </edges>
  </object>


  <object>
    <name>john</name>
    <graphtype>DefaultIndividualGT</graphtype>
    <edges>
    </edges>
  </object>

</result>
"









