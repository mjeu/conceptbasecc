{*
The ConceptBase.cc Copyright

Copyright 1987-2025 The ConceptBase Team. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of
      conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE CONCEPTBASE TEAM ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE CONCEPTBASE TEAM OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the authors
and should not be interpreted as representing official policies, either expressed or implied,
of the ConceptBase Team.


The ConceptBase Team is represented by

Manfred Jeusfeld, University of Skovde, 54128 Skovde, Sweden


This license is a FreeBSD-style copyright license.
Legal home of the FreeBSD copyright license: http://www.freebsd.org/copyright/freebsd-license.html
*}
{
*
* File :       RuleOptimizer.pro
* Version :    1.1
* Creation:    Jul-24-1990 Martin Staudt (UPA)
* Last change: 7/24/90 Martin Staudt (UPA)
* Release :    1
*
*
* This file contains predicates for
*
*
*
*----------------------------------------------------------------------------
*
* Exported predicates:
*---------------------
*
*   + rewrite_rule/3
*
*
}

#MODULE(RuleOptimizer)
#EXPORT(get_binding_pattern/2)
#EXPORT(rewrite_rule/3)
#ENDMODDECL()


#IMPORT(append/3,GeneralUtilities)
#IMPORT(variable/1,GeneralUtilities)
#IMPORT(reverse/2,GeneralUtilities)
#IMPORT(member/2,GeneralUtilities)
#IMPORT(remove_multiple_elements/2,GeneralUtilities)
#IMPORT(delete_all/3,GeneralUtilities)
#IMPORT(pc_atomconcat/2,PrologCompatibility)
#IMPORT(pc_atomconcat/3,PrologCompatibility)
#IMPORT(pc_inttoatom/2,PrologCompatibility)
#IMPORT(pc_atomtolist/2,PrologCompatibility)

#IF(SWI)
:- style_check(-singleton).
#ENDIF(SWI)



{ ********************* r e w r i t e _ r u l e *************************** }
{ ************************************************************************* }

rewrite_rule(_ruleId,(_head :- _body),_rulelist) :-
	conjunction_depth(_body,_l),
	generate_a_and_b(_ruleId,_head,_l,_evaluate0,
			 (_qrulehead :- _q , _evaluaten),
			 _ev0rule),
	get_vars(_head,_headvars),
	generate_c_and_d(_ruleId,1,_headvars,_body,_evaluate0,_evaluaten,
                         _evrules,_queryrules),!,
	append([(_qrulehead :- _q , _evaluaten),_ev0rule],_evrules,_h1),
	append(_h1,_queryrules,_rulelist).


{ *********************** g e n e r a t e _ a _ a n d _ b ***************** }
{ ************************************************************************* }

generate_a_and_b(_ruleId,_head,_n,_evaluate0,(_head :- _q , _evaluaten),
	                             (_evaluate0 :- _q)) :-
	get_bound_args(_head,_bargs),
	get_free_vars(_head,_fvars),
	remove_multiple_elements(_fvars,_fsvars),
	get_bound_vars(_head,_bvars),
	remove_multiple_elements(_bvars,_bsvars),
	pc_atomconcat(evaluate,_ruleId,_h1),
	pc_atomconcat(_h1,'_0',_h2),
	_evaluate0 =.. [_h2|_bsvars],
	_head =.. [_f|_],
	pc_atomconcat('query_',_f,_nf),
	_q =..[_nf|_bargs].


{ ******************* g e n e r a t e _ c _ a n d _ d ********************* }
{ ************************************************************************* }
generate_c_and_d(_ruleId,_nr,_fheadv,(_goal,_rgoals),
                 _lastevgoal,_evn,[(_evhead :- _evbody)|_revrules],
	         _qrules) :-
	_goal \= (not(_)),!,
	build_evaluate_rule(_ruleId,_nr,_fheadv,_goal,_rgoals,_lastevgoal,
                            (_evhead :- _evbody)),
	_nnr is _nr + 1,
	generate_c_and_d(_ruleId,_nnr,_fheadv,_rgoals,_evhead,_evn,
                         _revrules,_rqrules),
	build_query_rules(_goal,_lastevgoal,_rqrules,_qrules).


generate_c_and_d(_ruleId,_nr,_fheadv,(not(_goal),_rgoals),_lastevgoal,_evn,
                 [(_evhead :- _evbody)|_revrules],_qrules) :-
	!,
	build_evaluate_rule(_ruleId,_nr,_fheadv,not(_goal),_rgoals,_lastevgoal,
                            (_evhead :- _evbody)),
	_nnr is _nr + 1,
	generate_c_and_d(_ruleId,_nnr,_fheadv,_rgoals,_evhead,_evn,
                         _revrules,_rqrules),
	build_query_rules(_goal,_lastevgoal,_rqrules,_qrules).


generate_c_and_d(_ruleId,_nr,_fheadv,_goal,_lastevgoal,_evhead,
                 [(_evhead :- _evbody)],_qrule) :-
	_goal \= (not(_)),!,
	build_evaluate_rule(_ruleId,_nr,_fheadv,_goal,true,_lastevgoal,
                            (_evhead :- _evbody)),
	build_query_rules(_goal,_lastevgoal,[],_qrule).


generate_c_and_d(_ruleId,_nr,_fheadv,not(_goal),_lastevgoal,_evhead,
                 [(_evhead :- _evbody)],_qrule) :-
	build_evaluate_rule(_ruleId,_nr,_fheadv,not(_goal),true,_lastevgoal,
                            (_evhead :- _evbody)),
	build_query_rules(_goal,_lastevgoal,[],_qrule).


{ ******************* b u i l d _ q u e r y _ r u l e s ******************** }
{ ************************************************************************** }

build_query_rules(_goal,_,_qrules,_qrules) :-
	BasePred(_goal),!.

build_query_rules(_goal,_lastevgoal,_rqrules,[_qrule|_rqrules]) :-
	_goal =..[_f|_args],
	get_bound_args(_goal,_bargs),
	pc_atomconcat('query_',_f,_qf),
	_qhead =..[_qf|_bargs],
	_qrule = (_qhead :- _lastevgoal).


{ ****************** b u i l d _ e v a l u a t e _ r u l e ***************** }
{ ************************************************************************** }

build_evaluate_rule(_ruleId,_nr,_fheadv,not(_goal),_rgoals,_lastevgoal,_evrule) :-
	!,
	get_free_vars(_goal,_fvars),
	remove_multiple_elements(_fvars,_fsvars),
	_lastevgoal =..[_|_lastevvars],
	append(_lastevvars,_fsvars,_posevvars),
	relevant_vars(_rgoals,_fheadv,_posevvars,_evvars),
	pc_atomconcat(evaluate,_ruleId,_h1),
	pc_inttoatom(_nr,_anr),
	pc_atomconcat('_',_anr,_aanr),
	pc_atomconcat(_h1,_aanr,_h2),
	_evhead =..[_h2|_evvars],
	_evrule = (_evhead :- _lastevgoal , not(_goal)).

build_evaluate_rule(_ruleId,_nr,_fheadv,_goal,_rgoals,_lastevgoal,_evrule) :-
	get_free_vars(_goal,_fvars),
	remove_multiple_elements(_fvars,_fsvars),
	_lastevgoal =..[_|_lastevvars],
	append(_lastevvars,_fsvars,_posevvars),
	relevant_vars(_rgoals,_fheadv,_posevvars,_evvars),
	pc_atomconcat(evaluate,_ruleId,_h1),
	pc_inttoatom(_nr,_anr),
	pc_atomconcat('_',_anr,_aanr),
	pc_atomconcat(_h1,_aanr,_h2),
	_evhead =..[_h2|_evvars],
	_evrule = (_evhead :- _lastevgoal , _goal).




{ ************************ r e l e v a n t _ v a r s *********************** }
{ ************************************************************************** }

relevant_vars(_t,_l,[],[]).

relevant_vars(_t,_l,['_'|_r],_nr) :-
	!,relevant_vars(_t,_l,_r,_nr).
relevant_vars(_t,_l,[_f|_r],[_f|_nr]) :-
	member(_f,_l),!,
	relevant_vars(_t,_l,_r,_nr).

relevant_vars(_t,_l,[_f|_r],[_f|_nr]) :-
	relevant_in(_t,_f),!,
	relevant_vars(_t,_l,_r,_nr).

relevant_vars(_t,_l,[_f|_r],_nr) :-
	relevant_vars(_t,_l,_r,_nr).

{ ************************ r e l e v a n t _ i n *************************** }
{ ************************************************************************** }

relevant_in(_goal,_var) :-
	_goal \= (_,_),
	_goal \= (not(_)),
	_goal =..[_f|_args],
	member(_var,_args).

relevant_in(not(_goal),_var) :-
	_goal =..[_f|_args],
	member(_var,_args).

relevant_in((_goal,_rgoals),_var) :-
	_goal \= (not(_)),
	_goal =..[_f|_args],
	member(_var,_args),!.

relevant_in((not(_goal),_rgoals),_var) :-
	_goal =..[_f|_args],
	member(_var,_args),!.

relevant_in((_goal,_rgoals),_var) :-
	relevant_in(_rgoals,_var).

{ ********************* g e t _ b o u n d _ a r g s ************************ }
{ ************************************************************************** }

get_bound_args(_t,_bargs) :-
	_t =..[_f|_args],
	_args \== [],!,
	get_binding_pattern(_f,_p),
	pc_atomtolist(_p,_lp),
	filter_arguments(bound,arg,_lp,_args,_bargs).

get_bound_args(_t,[]).


{ **************************** g e t _ v a r s ***************************** }
{ ************************************************************************** }

get_vars(_t,_vars) :-
	_t =..[_f|_args],
	filter_arguments(any,var,[],_args,_vars).

{ ********************* g e t _ f r e e _ v a r s ************************** }
{ ************************************************************************** }

get_free_vars(_t,_fvars) :-
	_t =..[_f|_args],
	_args \== [],!,
	get_binding_pattern(_f,_p),
	pc_atomtolist(_p,_lp),
	filter_arguments(free,var,_lp,_args,_fvars).

get_free_vars(_t,[]).

{ *********************** g e t _ b o u n d _ v a r s ********************** }
{ ************************************************************************** }

get_bound_vars(_t,_bvars) :-
	_t =..[_f|_args],
	_args \== [],!,
	get_binding_pattern(_f,_p),
	pc_atomtolist(_p,_lp),
	filter_arguments(bound,var,_lp,_args,_bvars).

get_bound_vars(_t,[]).

{ ********************** f i l t e r _ a r g u m e n t s ******************* }
{ ************************************************************************** }


filter_arguments(_,_,[],[],[]).

filter_arguments(any,var,[],[_f|_r],[_f|_nr]) :-
	variable(_f),
	filter_arguments(any,var,[],_r,_nr).

filter_arguments(any,var,[],[_f|_r],_nr) :-
	not(variable(_f)),
	filter_arguments(any,var,[],_r,_nr).

filter_arguments(bound,arg,[b|_pr],[_f|_r],[_f|_nr]) :-
	filter_arguments(bound,arg,_pr,_r,_nr).

filter_arguments(bound,arg,[f|_pr],[_|_r],_nr) :-
	filter_arguments(bound,arg,_pr,_r,_nr).

filter_arguments(free,var,[f|_pr],[_f|_r],[_f|_nr]) :-
	variable(_f),
	filter_arguments(free,var,_pr,_r,_nr).

filter_arguments(free,var,[f|_pr],[_f|_r],_nr) :-
	not(variable(_f)),
	filter_arguments(free,var,_pr,_r,_nr).

filter_arguments(free,var,[b|_pr],[_|_r],_nr) :-
	filter_arguments(free,var,_pr,_r,_nr).

filter_arguments(bound,var,[b|_pr],[_f|_r],[_f|_nr]) :-
	variable(_f),
	filter_arguments(bound,var,_pr,_r,_nr).

filter_arguments(bound,var,[b|_pr],[_f|_r],_nr) :-
	not(variable(_f)),
	filter_arguments(bound,var,_pr,_r,_nr).

filter_arguments(bound,var,[f|_pr],[_|_r],_nr) :-
	filter_arguments(bound,var,_pr,_r,_nr).

{ ****************** g e t _ b i n d i n g _ p a t t e r n ***************** }
{ ************************************************************************** }

get_binding_pattern(_atom,_pattern) :-
	pc_atomtolist(_atom,_al),
	search_last_and_rest('_',_al,_pl),
	pc_atomtolist(_pattern,_pl).

{ *************** s e a r c h _ l a s t _ a n d _ r e s t ****************** }
{ ************************************************************************** }

search_last_and_rest(_c,_l,_nl) :-
	reverse(_l,_rl),
	search_first_and_top(_c,_rl,_rnl),
	reverse(_rnl,_nl).

{ *************** s e a r c h _ f i r s t _ a n d _ t o p ****************** }
{ ************************************************************************** }

search_first_and_top(_c,[_c|_],[]).

search_first_and_top(_c,[_a|_r],[_a|_nr]) :-
	_c \== _a,
	search_first_and_top(_c,_r,_nr).


{ ******************* c o n j u n c t i o n _ d e p t h ******************** }
{ ************************************************************************** }

conjunction_depth((_,_r),_d) :-
	!,
	conjunction_depth(_r,_d1),
	_d is _d1 + 1 .

conjunction_depth(_c,1).

{ ************************************************************************** }


BasePred(true).






