{*
The ConceptBase.cc Copyright

Copyright 1987-2025 The ConceptBase Team. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of
      conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE CONCEPTBASE TEAM ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE CONCEPTBASE TEAM OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the authors
and should not be interpreted as representing official policies, either expressed or implied,
of the ConceptBase Team.


The ConceptBase Team is represented by

Manfred Jeusfeld, University of Skovde, 54128 Skovde, Sweden


This license is a FreeBSD-style copyright license.
Legal home of the FreeBSD copyright license: http://www.freebsd.org/copyright/freebsd-license.html
*}
{************************************************************************
*
* File:         MetaLiterals.pro
* Version:      2.2
*
*
* Date released : 96/02/12  (YY/MM/DD)
*
* SCCS-Source-Pool : /home/CBase/CB_NewStruct/ProductPOOL/serverSources/Prolog_Files/SCCS/s.MetaLiterals.pro
* Date retrieved : 96/02/12 (YY/MM/DD)
**************************************************************************
*
* Das Modul MetaLiterals beinhaltet Praedikate zur Arbeit mit
* einzelnen Literalen.
*
* Beispiel:
* computeExtension(_lit,_cons,_extList)
* berechnet fuer das Literal _lit mit den Konstanten _cons die
* Extension _ext.
*
}

#MODULE(MetaLiterals)
#EXPORT(collectArguments/2)
#EXPORT(computeExtension/3)
#EXPORT(determineArguments/3)
#EXPORT(determines/4)
#EXPORT(determines2/2)
#EXPORT(determines3/3)
#EXPORT(findPositions/3)
#EXPORT(litParts/3)
#EXPORT(not_simpleLiteral/1)
#EXPORT(simpleLiteral/1)
#EXPORT(substituteLits/4)
#EXPORT(expandMacroPredicates/2)
#ENDMODDECL()

#IMPORT(prove_literal/1,Literals)
#IMPORT(prove_upd_literal/1,Literals)
#IMPORT(append/3,GeneralUtilities)
#IMPORT(empty/1,MetaUtilities)
#IMPORT(findPositionsInList/3,MetaUtilities)
#IMPORT(getElemsInPos/3,MetaUtilities)
#IMPORT(giveNthMember/3,MetaUtilities)
#IMPORT(length/2,GeneralUtilities)
#IMPORT(listIntersection/3,MetaUtilities)
#IMPORT(memberchk/2,GeneralUtilities)
#IMPORT(nonmember/2,MetaUtilities)
#IMPORT(not_empty/1,MetaUtilities)
#IMPORT(removeMultiEntries/2,MetaUtilities)
#IMPORT(substituteArgsAtPositions/4,MetaUtilities)
#IMPORT(substElemsInList/4,MetaUtilities)
#IMPORT(subSetList/2,MetaUtilities)
#IMPORT(save_setof/3,GeneralUtilities)
#IMPORT(name2id/2,GeneralUtilities)
#IMPORT(id2name/2,GeneralUtilities)
#IMPORT(increment_counter/2,GeneralUtilities)
#IMPORT(pc_atomconcat/3,PrologCompatibility)
#IMPORT(pc_inttoatom/2,PrologCompatibility)
#IMPORT(isComplexComparisonLit/1,QO_preproc)
#IMPORT(isComplexQlit/1,QO_preproc)
#IMPORT(isSubsetOf/2,GeneralUtilities)
#IMPORT(is_id/1,MetaUtilities)

#IF(SWI)
:- style_check(-singleton).
#ENDIF(SWI)




{--------------EXPORT PART -------------------------}
{--------------------------------------------------}
{--------------------------------------------------}


{collectArguments(litList,argList)
litlist is a List of Literals,
argList is a list of the arguments of those literals, with duplicates
removed.
}

collectArguments([],[]).
collectArguments(_litList,_argList) :-
	not_empty(_litList),
	collectArguments2(_litList,_argList1),
 	removeMultiEntries(_argList1,_argList).

{* Instanzen von Query-Klassen werden nicht berechnet,
   um beispielsweise Endlosschleifen zu vermeiden RS, 12.2.96*}

computeExtension(In(_x,_c),_cons,[]) :-
	prepareLiteral(In(_x,_c),_cons,_newLit),
	_newLit =..[In,_xNew,_id],
	ground(_id),
	prove_upd_literal(In(_id,id_65)),!.   {* id_65 = QueryClass *}

computeExtension(_lit,_cons,_extList) :-
	prepareLiteral(_lit,_cons,_newLit),
	save_setof(_newLit,(prove_upd_literal(_newLit),ground(_newLit)),_extList),!.


{
determines(_l,_c,_v,_cost)
cost function of literal evaluation:
If _l is evaluated with _c ground and _v non-ground, then cost
_cost is assigned to this evaluation
}

determines(TRUE,_,_,1).
determines(FALSE,_,_,1).


determines(A(_x,_m,_y),[_x,_m,_y],[],1).
determines(A(_x,_m,_y),[_x,_m],[_y],10).
determines(A(_x,_m,_y),[_x,_y],[_m],10).
determines(A(_x,_m,_y),[_m,_y],[_x],10).
determines(A(_x,_m,_y),[_x],[_m,_y],100).
determines(A(_x,_m,_y),[_m],[_x,_y],100) :- _m \== 'IN'.
determines(A(_x,_m,_y),[_m],[_x,_y],12) :- _m == 'IN'.   {* ticket #404: special support for DeepTelos *}
determines(A(_x,_m,_y),[_y],[_x,_m],100) .
determines(A(_x,_m,_y),[],[_x,_m,_y],1000) .

{* 1-Nov-2006/M.Jeusfeld: Also support A_label as *}
{* literal that may bind meta variables. See also *}
{* ticket #124                                    *}
determines(A_label(_x,_m,_y,_n),[_x,_m,_y,_n],[],1).
determines(A_label(_x,_m,_y,_n),[_x,_m,_y],[_n],2).
determines(A_label(_x,_m,_y,_n),[_x,_y],[_m,_n],10).
determines(A_label(_x,_m,_y,_n),[_x,_m],[_n,_y],100).
determines(A_label(_x,_m,_y,_n),[_m,_x],[_n,_y],100).
determines(A_label(_x,_m,_y,_n),[_y,_m],[_x,_n],100).
determines(A_label(_x,_m,_y,_n),[_m,_y],[_x,_n],100).
determines(A_label(_x,_m,_y,_n),[_m],[_x,_n,_y],120).

determines(Ae_label(_x,_m,_y,_n),[_x,_m,_y,_n],[],1).
determines(Ae_label(_x,_m,_y,_n),[_x,_m,_y],[_n],2).
determines(Ae_label(_x,_m,_y,_n),[_x,_y],[_m,_n],8).
determines(Ae_label(_x,_m,_y,_n),[_x,_m],[_n,_y],90).
determines(Ae_label(_x,_m,_y,_n),[_m,_x],[_n,_y],90).
determines(Ae_label(_x,_m,_y,_n),[_y,_m],[_x,_n],90).
determines(Ae_label(_x,_m,_y,_n),[_m,_y],[_x,_n],90).
determines(Ae_label(_x,_m,_y,_n),[_m],[_x,_n,_y],100).

determines(Ai(_x,_m,_y),[_x,_m,_y],[],1).
determines(Ai(_x,_m,_y),[_x,_m],[_y],10).
determines(Ai(_x,_m,_y),[_x,_y],[_m],10).
determines(Ai(_x,_m,_y),[_m,_y],[_x],10).
determines(Ai(_x,_m,_y),[_x],[_m,_y],100).
determines(Ai(_x,_m,_y),[_m],[_x,_y],100).
determines(Ai(_x,_m,_y),[_y],[_x,_m],1000) .
determines(Ai(_x,_m,_y),[],[_x,_m,_y],1000) .

{* ticket #207: support A_e like A *}
determines(A_e(_x,_m,_y),[_x,_m,_y],[],1).
determines(A_e(_x,_m,_y),[_x,_m],[_y],2).
determines(A_e(_x,_m,_y),[_x,_y],[_m],5).
determines(A_e(_x,_m,_y),[_m,_y],[_x],10).
determines(A_e(_x,_m,_y),[_x],[_m,_y],20).
determines(A_e(_x,_m,_y),[_m],[_x,_y],50).
determines(A_e(_x,_m,_y),[_y],[_x,_m],60) .
determines(A_e(_x,_m,_y),[],[_x,_m,_y],100) .



determines(In(_x,_id),[_id],[_x],10000):- id2name(_id,Proposition),!.

determines(In(_x,_id),[_id],[_x],5000):- id2name(_id,Individual),!.
determines(In(_x,_id),[_id],[_x],5000):- id2name(_id,Attribute),!.

determines(In(_x,_c),[_c],[_x],1000) :- is_id(_c),prove_upd_literal(A(_,Proposition,IN,_c)),!.  {* _c is defined in DeepTelos *}
determines(In(_x,_c),[_c],[_x],1000) :- is_id(_c),prove_upd_literal(A(_c,Proposition,isPowerTypeOf,_)),!.  {* _c is defined in MLT-Telos *}
determines(In(_x,_c),[_c],[_x],12).
determines(In(_x,_c),[_x,_c],[],1) .
determines(In(_x,_c),[_x],[_c],5) .
determines(In(_x,_c),[],[_x,_c],200) .   {* this would be a meta predicate *}

determines(Isa(_x,_c),[_x,_c],[],1) .
determines(Isa(_x,_c),[_x],[_c],5) .
determines(Isa(_x,_c),[_c],[_x],5) .
determines(Isa(_x,_c),[],[_x,_c],1000) .  {* the object store even generates an alert when called like this; ticket #404 *}


determines(P(_p,_c,_m,_d),[],[_p,_c,_m,_d],10000) .
determines(P(_p,_c,_m,_d),[_p],[_c,_m,_d],1) .
determines(P(_p,_c,_m,_d),[_p,_c],[_m,_d],100) .
determines(P(_p,_c,_m,_d),[_p,_m],[_c,_d],100) .
determines(P(_p,_c,_m,_d),[_p,_d],[_c,_m],100) .
determines(P(_p,_c,_m,_d),[_p,_c,_m],[_d],100) .
determines(P(_p,_c,_m,_d),[_p,_c,_d],[_m],100) .
determines(P(_p,_c,_m,_d),[_p,_m,_d],[_c],100) .
determines(P(_p,_c,_m,_d),[_p,_c,_m,_d],[],100) .
determines(P(_p,_c,_m,_d),[_c],[_p,_m,_d],10000).
determines(P(_p,_c,_m,_d),[_c,_m],[_p,_d],10000).
determines(P(_p,_c,_m,_d),[_c,_d],[_p,_m],10000).
determines(P(_p,_c,_m,_d),[_c,_m,_d],[_p],10000).
determines(P(_p,_c,_m,_d),[_m],[_p,_c,_d],10000).
determines(P(_p,_c,_m,_d),[_m,_d],[_p,_c],10000).
determines(P(_p,_c,_m,_d),[_d],[_p,_c,_m],10000) .

determines(Pa(_p,_c,_m,_d),_args1,_args2,_cost) :-
  determines(P(_p,_c,_m,_d),_args1,_args2,_cost).


determines(From(_p,_c),[],[_p,_c],1000).
determines(From(_p,_c),[_p],[_c],1).
determines(From(_p,_c),[_c],[_p],100).
determines(From(_p,_c),[_p,_c],[],100).

determines(To(_p,_c),[],[_p,_c],1000).
determines(To(_p,_c),[_p],[_c],1).
determines(To(_p,_c),[_c],[_p],100).
determines(To(_p,_c),[_p,_c],[],100).

determines(Label(_p,_c),[],[_p,_c],1000).
determines(Label(_p,_c),[_p],[_c],1).
determines(Label(_p,_c),[_c],[_p],100).
determines(Label(_p,_c),[_p,_c],[],1).

determines(EQ(_x,_y),[_x,_y],[],1) .
determines(EQ(_x,_y),[_x],[_y],1) .
determines(EQ(_x,_y),[_y],[_x],1) .
determines(EQ(_x,_y),[],[_x,_y],100) .

determines(LT(_x,_y),[_x,_y],[],1) .
determines(LT(_x,_y),[_x],[_y],1000) .
determines(LT(_x,_y),[_y],[_x],1000) .
determines(LT(_x,_y),[],[_x,_y],1000) .

determines(GT(_x,_y),[_x,_y],[],1) .
determines(GT(_x,_y),[_x],[_y],1000) .
determines(GT(_x,_y),[_y],[_x],1000) .
determines(GT(_x,_y),[],[_x,_y],1000) .


determines(LE(_x,_y),[_x,_y],[],1) .
determines(LE(_x,_y),[_x],[_y],1000) .
determines(LE(_x,_y),[_y],[_x],1000) .
determines(LE(_x,_y),[],[_x,_y],1000) .


determines(GE(_x,_y),[_x,_y],[],1) .
determines(GE(_x,_y),[_x],[_y],1000) .
determines(GE(_x,_y),[_y],[_x],1000) .
determines(GE(_x,_y),[],[_x,_y],1000) .


determines(NE(_x,_y),[_x,_y],[],1) .
determines(NE(_x,_y),[_x],[_y],1000) .
determines(NE(_x,_y),[_y],[_x],1000) .
determines(NE(_x,_y),[],[_x,_y],1000) .

determines(IDENTICAL(_x,_y),[_x,_y],[],1) .
determines(IDENTICAL(_x,_y),[_x],[_y],1000) .
determines(IDENTICAL(_x,_y),[_y],[_x],1000) .
determines(IDENTICAL(_x,_y),[],[_x,_y],1000) .

determines(UNIFIES(_x,_y),_l1,_l2,_cost) :-
	determines(EQ(_x,_y),_l1,_l2,_cost).


{* determines1 excludes those patterns where a variable position is being filled by a constant *}

determines1(_allconsts,_lit,_cons,_vars,_cost) :-
   determines(_lit,_cons,_vars,_cost),
   checkConstVars(_allconsts,_cons,_vars).

checkConstVars(_allconsts,_cons,_vars) :-
   varNotConst(_vars,_allconsts),     {* allconsts is known in advance, vars is generated here *}
   !.


{* an argument known to be constant can never be a free variable *}
varNotConst([],_) :- !.

varNotConst([_v|_restvars],_allconsts) :- 
  member(_v,_allconsts),
  !,
  fail.

varNotConst([_|_restvars],_allconsts) :- 
  varNotConst(_restvars,_allconsts).





{
determines2(__lit,_detInfoList)
_detInfoList is a List of "DI" - predicates, sorted by cost
DI predicates have the same semantics as determines predicates
}
determines2(_lit,_diList) :-
	findall(DI(_lit,_cons,_vars,_cost),determines(_lit,_cons,_vars,_cost),_list1),
	sortDIListCostAsc(_list1,_diList).



{* determines3 is like determines2 but excludes those lit patterns where a variable *}
{* position is being filled by a known constant.                                    *}

determines3(_allconsts,_lit,_diList) :-
	findall(DI(_lit,_cons,_vars,_cost),determines1(_allconsts,_lit,_cons,_vars,_cost),_list1),
	sortDIListCostAsc(_list1,_diList).




{----------------------------------------------
determineArguments(l1,l2,l3)
l3 contains the members of l1, which are on the positions given in l2
constraint: All elements of l1 given in an element of l2 must be equal
All elements of l2 must be not empty
example
determineArguments([a,a,b],[[1,2],[3]],[a,b]).
}
determineArguments(_,[],[]) .
determineArguments(_args,[_x|_xs],[_c|_cs]) :-
	not_empty(_x),
 	determineArgument(_args,_x,_c),
 	determineArguments(_args,_xs,_cs).

filterGroundLiterals([],[]).
filterGroundLiterals([_lit|_lits],[_lit|_newLits]) :-
	ground(_lit),!,
	filterGroundLiterals(_lits,_newLits).
filterGroundLiterals([_lit|_lits],_newLits) :-
	filterGroundLiterals(_lits,_newLits).



{ findPositions(args,lit,pos)
For each member of args in pos the Argument - Position is determined:
e.g. findPositions([a,b,c],In(a,a)),[[2,1],[],[]]).
fails, if _lit is not simple literal}

findPositions(_xs,_lit,_pos) :-
	litParts(_lit,_,_args),
 	findPositionsInList(_xs,_args,_pos).


{litParts(_lit,_str,_subLitList,_argumentList)
str is the string representation of the toplevel literal - functor
subLitList is empty if lit is a simple literal, otherwise it contains the sub - Literals
Argumentlist contains the Arguments of the literal if lit is a simple literal
otherwise it is empty
}
litParts(not(_lit),_functor,_argList) :- !,
	_lit =.. [_functor|_subList],
	_argList = _subList.
litParts(_lit,_functor,_argList) :-
	simpleLiteral(_lit),
	_lit =.. [_functor|_subList],
	_argList = _subList.


{
not_simpleLiteral(_lit) succeeds if _lit is a literal with functor 'not'
}
not_simpleLiteral(not(_lit)).

{
prepareLiteral(_lit,_cons,_newLit):
all arguments of _lit, nit occuring in _cons are made
_variable
}
prepareLiteral(_lit,_cons,_newLit) :-
	findPositions(_cons,_lit,_pos),
	litParts(_lit,_func,_args),
	length(_args,_arity),
	functor(_newLit1,_func,_arity),
	_newLit1 =.. [_|_arguments],
	substituteArgsAtPositions(_arguments,_cons,_pos,_newArguments),
	_newLit =.. [_func|_newArguments].








{* simpleLiteral(_lit) succeeds if _lit is a literal without functor 'not'  *}
{* or is not unifiable with 'not'                                           *}
{* 18-Jul-2007/M.Jeusfeld: omit testAtomic(_args) because ConceptBase 7.0   *}
{* supports complex literals such as EQ(x,IPLUS[..]).                       *}
{* See also ticket #142.                                                    *}
simpleLiteral(_lit) :-
	_lit =.. [_functor|_args],
	nonmember(_functor,[forall,exists,and,or,'not']).



{
substituteDIs(_diList,_vars,_cons,_newDIList):
_diList: list of determiner infos
_vars: List of Variables
_cons: List of corresponding constants which are substituted
_newDIList: list of determiner Infos with variables substituted by constants
}
substituteDIs([],_,_,[]) .
substituteDIs([DI(_lit,_c,_v,_cost)|_path],_vars,_cons,[DI(_lit,_c,_v,_cost)|_newPath]) :-
	litParts(_lit,_,_arguments) ,
	listIntersection(_arguments,_vars,_testList) ,
	empty(_testList) ,
	substituteDIs(_path,_vars,_cons,_newPath).

substituteDIs([DI(_lit,_c,_v,_cost)|_path],_vars,_cons,[DI(_newLit,_newC,_newV,_cost)|_newPath]) :-
	litParts(_lit,_functor,_arguments),
	listIntersection(_arguments,_vars,_testList),
	not_empty(_testList),
	substElemsInList(_arguments,_vars,_cons,_newArgs),
	_newLit =.. [_functor|_newArgs],
	substElemsInList(_c,_vars,_cons,_newC) ,
	substElemsInList(_v,_vars,_cons,_newV) ,
	substituteDIs(_path,_vars,_cons,_newPath).



substituteLits([],_,_,[]) .

substituteLits([_lit|_lits],_vars,_cons,[_newLit|_newLits]) :-
        isComplexComparisonLit(_lit),
        litParts(_lit,_functor,[_arg1,_arg2]) ,
        substituteComplexArg(_vars,_cons,_arg1,_newArg1),
        substituteComplexArg(_vars,_cons,_arg2,_newArg2),
        _newLit =..[ _functor|[_newArg1,_newArg2]],
        substituteLits(_lits,_vars,_cons,_newLits).

substituteLits([_lit|_lits],_vars,_cons,[_newLit|_newLits]) :-
        isComplexQlit(_lit),
        litParts(_lit,_functor,_args) ,
        substituteComplexArgs(_vars,_cons,_args,_newArgs),
         _newLit =..[ _functor|_newArgs],
        substituteLits(_lits,_vars,_cons,_newLits).

substituteLits([_lit|_lits],_vars,_cons,[_lit|_newLits]) :-
	litParts(_lit,_functor,_arguments) ,
	listIntersection(_arguments,_vars,[]) ,!,
	substituteLits(_lits,_vars,_cons,_newLits).


substituteLits([not(_lit)|_lits],_vars,_cons,[not(_newLit)|_newLits]) :-
	litParts(_lit,_functor,_arguments) ,
	listIntersection(_arguments,_vars,_testList) ,
	not_empty(_testList),!,
	substElemsInList(_arguments,_vars,_cons,_newArgs),
	_newLit =..[ _functor|_newArgs],
	substituteLits(_lits,_vars,_cons,_newLits).

substituteLits([_lit|_lits],_vars,_cons,[_newLit|_newLits]) :-
	simpleLiteral(_lit),
	litParts(_lit,_functor,_arguments) ,
	listIntersection(_arguments,_vars,_testList) ,
	not_empty(_testList),!,
	substElemsInList(_arguments,_vars,_cons,_newArgs),
	_newLit =..[ _functor|_newArgs],
	substituteLits(_lits,_vars,_cons,_newLits).


substituteComplexArg(_vars,_cons,_arg,_newarg) :-
	simpleLiteral(_arg),
	substituteLits([_arg],_vars,_cons,[_newarg]),
	!.


substituteComplexArg(_vars,_cons,_arg,_newarg) :-
	substElemsInList([_arg],_vars,_cons,[_newarg]).

substituteComplexArgs(_vars,_cons,[],[]) :- !.

substituteComplexArgs(_vars,_cons,[_arg|_rest],[_newarg|_newrest]) :-
	substituteComplexArg(_vars,_cons,_arg,_newarg),
	substituteComplexArgs(_vars,_cons,_rest,_newrest).



{-----------------------------LOCAL PART --------------------------}
collectArguments2([],[]) .
collectArguments2([_lit|_lits],_argList) :-
	litParts(_lit,_functor,_subList),
	collectArguments2(_lits,_subList2),
	append(_subList,_subList2,_argList).



determineArgument(_units,[_x],_c) :-
	giveNthMember(_units,_x,_c).

determineArgument(_units,[_x|_xs],_c) :-
	giveNthMember(_units,_x,_c),
 	determineArgument(_units,_xs,_c).

sortDIListCostAsc(_listUnsorted,_listSorted) :-
    insertCostKey(_listUnsorted,_listUnsortedWithKey),
	keysort(_listUnsortedWithKey,_listSortedWithKey),
	insertCostKey(_listSorted,_listSortedWithKey).

insertCostKey([],[]).
insertCostKey(['DI'(_a,_b,_c,_cost)|_r],[_cost - 'DI'(_a,_b,_c,_cost)|_rk]) :-
    insertCostKey(_r,_rk).

testAtomic([]).
testAtomic([_a|_rest]) :-
	atomic(_a),
	testAtomic(_rest).


{ ************ e x p a n d M a c r o P r e d i c a t e s  ******************* }
{                                                             MJf/6-Jun-2007  }
{ expandMacroPredicates(_inputFormula,_outputFormula)                         }
{    _inputFormula: ground                                                    }
{    _outputFormula: any: ground                                              }
{                                                                             }
{ expandMacroPredicates replaces all 'macro predicated' in inputFormula  by   }
{ their expansions. Macro predicates are (x [in] mc) (or In2(x,mc)) and       }
{ (x [m] y) (or A2(x,m,y). The expansion is working on the syntax tree        }
{ generated for rules and constraints. See ao ticket #124.                    }
{                                                                             }
{ *************************************************************************** }


expandMacroPredicates(_inputFormula,_outputFormula) :-
  reset_counter('var@counter'),
  substituteTerm(_inputFormula,_outputFormula).


{* pattern 1:  (x [in] mc) ==> (exists c/mc (x in c)) *}
{* mc must be ground! *}
expandPattern(lit('In2'(_x,_mc)), exists(c,_mc,lit('In'(_x,c))) ) :- ground(_mc),!.

{* pattern 2:  (x [m] y) ==> ??? *}
{* not yet implemented *}
{*
expandPattern(lit('A2'(_x,_m,_y)),
              exists(_c,'Proposition',
                     exists(_d,'Proposition',
                            exists(_n,'VAR',
                                  and([lit(In(_x,_c)),
                                      lit(In(_y,_d)),
                                      lit(AL(_c,_m,_n,_d)),
                                      lit(A(_x,_n,_y))
                                      ]
                                     )
                                  )
                           )
                    )
             ) :-
  ground(_m),
  increment_counter('var@counter',_i),
  makeVarAtom(cX,_i,_c),
  makeVarAtom(dX,_i,_d),
  makeVarAtom(nX,_i,_n),
  !.
*}


makeVarAtom(_prefix,_i,_at) :-
  pc_inttoatom(_i,_iat),
  pc_atomconcat(_prefix,_iat,_at).


substituteTerm(_term,_newterm) :-
  expandPattern(_term,_newterm),
  !.

substituteTerm(_term,_newterm) :-
  _term=..[_f|_args],
  substituteTermArgs(_args,_newargs),
  _newterm =..[_f|_newargs],
  !.

substituteTerm(_term,_term).


substituteTermArgs([],[]) :- !.

substituteTermArgs([_arg|_restargs],[_newarg|_newrestargs]) :-
  substituteTerm(_arg,_newarg),
  substituteTermArgs(_restargs,_newrestargs).




