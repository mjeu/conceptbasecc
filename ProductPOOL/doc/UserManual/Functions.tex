\section{Functions}
\label{sec:functions}

Functions are special queries which have mandatory input parameters
and return at most one result for a given input.
Functions can either be user-defined by a membership constraint like
for regular query classes, or they may be implemented by a PROLOG
code, which is defined either in the OB.builtin file (this file is part of
every ConceptBase database) or in a LPI-file (see also section \ref{subsec:counter}).

A couple of aggregation functions are predefined for counting, summing up, and computing the minimum/maximum/average.
Furthermore, there are functions for arithmetic and string manipulation.
See section \ref{cap:builtin-queries:functions} for the complete list.
Since functions are defined as regular Telos objects, you can load their
definition with the Telos editor of the ConceptBase User Interface. 

Unlike as for user-defined generic query classes, you have to provide fillers for
all parameters of a function.
We will refer to any query expression whose outer-most query is a function as
a {\em functional expression}.

The intrinsic property of a function is that it returns {\em at most one\/} answer object%
\footnote{A function returns the empty set {\em nil} if it is undefined for the provided input
values.} for a given combination of input parameters. This property allows to form complex
functional expressions including arithmetic expressions. Functions are also
special query classes, hence you you use them whereever a query class is expected.
Subsequently, we introduce first how to define and use functions
like queries. Then, we define the syntax of functional expressions and the definition of
recursive functions such as the computation of the length of the shortest path between
two nodes.



\subsection{Functions as special queries}

Assume that an attribute (either explicit or derived) has at most one filler. For example,
a class {\tt Project} may have attributes {\tt budget}  and {\tt managedBy} that both are single-valued.
A third attribute {\tt projMember} is multi-valued (default for category 'attribute').

\begin{verbatim}
Project with
  single
    budget: Integer;
    managedBy: Employee
  attribute
    projMember: Employee
end
\end{verbatim}

The two functions {\tt getBudget} and {\tt getManager} return the corresponding objects:

\begin{verbatim}
getBudget in Function isA Integer with
  parameter
    proj: Project
  constraint
    c1: $ (proj budget this) $
end

getManager in Function isA Employee with
  parameter
    proj: Project
  constraint
    c1: $ (proj managedBy this) $
end
\end{verbatim}

The two functions share all capabilities of query classes, except that the parameters are required
(one cannot call a function without providing fillers for all parameters) and that there is at most
one return object per input value.

Function can be called just like queries, for example {\tt getBudget[P1/proj]} shall return
the project budget of project P1 (if existent). You can also use the shortcut {\tt getBudget[P1]}
like for any other query, and the functional form {\tt getBudget(P1)}. The latter is the preferred
form for function calls. Note that ConceptBase adopts the mathematical style for function calls rather
that the object-oriented one\footnote{An object-oriented style for the first function would be
{\tt P1.getBudget()} rather than {\tt getBudget(P1)}.}.

If your function has several arguments, then use alphabetically sorted 
parameter names in the function definition:

\begin{verbatim}
f in Function isA D with
  parameter
    a1: R1;
    a2: R2
  constraint
    c: $ ... $
end
\end{verbatim}

This corresponds to the mathematical function signature
$$
f: R_1 \times R_2 \rightarrow D
$$

\noindent
Functions without parameters are also possibe, e.g. a function {\tt magicnumber} that returns 
a fixed number:

\begin{verbatim}
magicnumber in Function isA Integer with
  constraint
    cmagic: $ (this = 42) $
end
\end{verbatim}

You can call it with an empty argument list: {\tt magicnumber()}. 


\subsection{Shortcuts for function calls and functional expressions}

Since functions require  fillers for all parameters, ConceptBase offers also the functional syntax
{\tt f(x)} to refer to function calls. The expression {\tt f(x)} is a shortcut for {\tt f[x/param1]},
where {\tt param1} is the only parameter of function {\tt f}. If a function has more than one parameter,
then they are replaced according their alphabetic order:

\begin{verbatim}
g in Function isA T with
  parameter
    x: T1;
    y: T2
  constraint
    ...
end
\end{verbatim}

A call like {\tt g(bill,1000)} is a shortcut for {\tt g[bill/x,1000/y]} because {\tt x} is occurs before
{\tt y} in the ASCII alphabet.
The shortcut can be used to form complex functional expressions such as {\tt f(g(bill,getBudget(P1)))}. 
There is no limitation in nesting function calls. Function calls are only allowed as left or right side
of a comparison operator. They are always evaluated before the comparison operator is evaluated.
For example, the equality operator

\begin{verbatim}
    $  ... (f(g(bill,getBudget(P1))) = f(g(mary,1000))) $
\end{verbatim}

will be evaluated by evaluating first the inner functions and then the outer functions. Note that the
parameters must be compliant with the parameter definitions.


As a special case of functional expressions, ConceptBase supports arithmetic expressions in infix syntax.
The operator symbols {\tt +}, {\tt -}, {\tt *}, and {\tt /} are defined both for integer and
real values. ConceptBase shall determine the type of a sub-expression to deduce whether to use
the real-valued or integer-valued variant of the operation. Examples of admissable arithmetic expressions are

\begin{verbatim}
  a+2*(b-15)
  n+f(m)/3 
\end{verbatim}

Provided that {\tt a} and {\tt b} are variables holding integers, the first expresssion is equivalent to the function
shortcut
\begin{verbatim}
  IPLUS(a,IMULT(2,IMINUS(b,15)))
\end{verbatim}
and to the query call
\begin{verbatim}
  IPLUS[a/i1,IMULT[2/i1,IMINUS[b/i1,15/i2]/i2]/i2]
\end{verbatim}

The second arithmetic expression includes a division which in general results in a real number.
Hence, the function shortcut for this expression is

\begin{verbatim}
  PLUS(n,DIV(f(m),3))
\end{verbatim}
The whole expression returns a real number.

The arguments in functional expressions must be object names (including instances of Integer and Real) or variables. 


% Examples for Usage

\subsection{Example function calls and definitions}

\begin{enumerate}
\item The following three variants all count the number of instances of {\tt Class}:
\begin{verbatim}
COUNT[Class/class]
COUNT(Class)
#Class
\end{verbatim}

The result is an integer number, e.g.\
\begin{verbatim}
119
\end{verbatim}

The operator {\tt \#} is a special shortcut for {\tt COUNT}. 

\item The subsequent query sums up the salaries of an Employee:
\begin{verbatim}
SUM_Attribute[bill/objname,Employee!salary/attrcat]
SUM_Attribute(Employee!salary,bill)
\end{verbatim}


Note that the parameter label {\tt attrcat} is sorted before {\tt objname} for the function shortcut. 
The result is returned as a real number, even if the input numbers were integers.
{\small
\begin{verbatim}
2.5001000000000e+04
\end{verbatim}
}

You can also use functions also in query class to assign a value to a ``computed\_attribute'':
{\small
\begin{verbatim}
QueryClass EmployeesWithSumSalaries isA Employee with
computed_attribute
    sumsalary : Real
constraint
    c: $ (sumsalary = SUM_Attribute(Employee!salary,this)) $
end
\end{verbatim}
}

\item Complex computations can be made by using multiple functions in a row. This query returns the
percentage of query classes wrt. the total number of classes.
{\small
\begin{verbatim}
Function PercentageOfQueryClasses isA Real with
constraint
    c: $ exists i1,i2/Integer r/Real
    (i1 = COUNT[QueryClass/class]) and (i2 = COUNT[Class/class]) and
    (r = DIV[i1/r1,i2/r2]) and (this = MULT[100/r1,r/r2]) $
end
\end{verbatim}
}

The query can be simplified with the use of function shortcuts to 

{\small
\begin{verbatim}
Function PercentageOfQueryClasses isA Real with
constraint
    c: $ (this = MULT(100,DIV(COUNT(QueryClass),
                              COUNT(Class)))) $
end
\end{verbatim}
}

and with arithmetic expressions to

{\small
\begin{verbatim}
Function PercentageOfQueryClasses isA Real with
constraint
    c: $ (this = 100 * #QueryClass / #Class) $ 
end
\end{verbatim}
}

The function {\tt PercentageOfQueryClasses} has zero parameters.
You can use it as follows in logical expressions

{\small
\begin{verbatim}
   $ ... (PercentageOfQueryClasses() > 25.5) ... $
\end{verbatim}
}

So, a function call {\tt F()} calls a function {\tt F} that has no parameter.

\end{enumerate}

Functions that yield a single numerical value can directly be incorporated in comparison
predicates. For example, the following query will return all individual objects that have more than
two attributes:

{\small
\begin{verbatim}
ObjectWithMoreThanTwoAttributes in QueryClass isA Individual with
  constraint
    c1 : $ (COUNT_Attribute(Proposition!attribute,this) > 2) $
end
\end{verbatim}
}

The functional expression used in the comparison can be nested. See section
\ref{sec:cqc} for details. You can also re-use the above query to form
further functional expressions, e.g. for counting the number of objects that
have more than two attribute. You find below all three representations for
the expression. 

{\small
\begin{verbatim}
COUNT[ObjectWithMoreThanTwoAttributes/class]
COUNT(ObjectWithMoreThanTwoAttributes)
#ObjectWithMoreThanTwoAttributes
\end{verbatim}
}


\subsection{Programmed functions}
\label{sec:UserFunctions}

If your application demands functional expressions beyond the set of predefined-functions,
you can extend the capabilities of your ConceptBase installation by adding more
functions. There are two ways: first, you can extend the capabilities of a certain
ConceptBase database,
or secondly, you can add the new functionality to your ConceptBase system files.
We will discuss the first option in more details using the function {\tt sin} as
an example, and then give some hints on how to achieve the
second option.

A function (like any builtin query class) has two aspects. First, the ConceptBase server requires
a regular Telos definition of the function declaring its name and parameters. This can look like:

{\small
\begin{verbatim}
sin in Function isA Real with
  parameter
    x : Real
  comment
    c : "computes the trigonometric function sin(x)" 
end
\end{verbatim}
}

The super-class {\tt Real} is the range of the function. i.e. any result is declared to be
an instance of {\tt Real}. The parameters are listed like for any regular generic query
class. The comment is optional.
We recommend to use short names to simplify the constructions of functional expressions.
The above Telos frame must be permanently stored in any ConceptBase database that is supposed to
use the new function.

The second aspect of a function is its implementation. The implementation can be in principal
in any programming language but we only support PROLOG because it can be incrementally addded
to a ConceptBase database. An implementation in another programming language would
require a re-compilation of the ConceptBase server source code. The syntax of the PROLOG code
must be conformant to the Prolog compiler with which ConceptBase was compiled. This is
in all cases SWI-Prolog (www.swi-prolog.org). For our {\tt sin} example, the PROLOG code would look like:

{\small
\begin{verbatim}
compute_sin(_res,_x,_C) :-
        cbserver:arg2val(_x,_v),
        number(_v),
        _vres is sin(_v),
        cbserver:val2arg(_vres,_res).

tell: 'sin in Function isA Real with parameter x : Real end'.
\end{verbatim}
}

The first argument {\tt \_res} is reserved for the result. then, for each parameter of the function
there are two arguments. The first is for the input parameter ({\tt \_x}), the second holds the
identifier of the class of the parameter (here: {\tt \_C}). It has to be included for technical
reasons. The clause 'tell:' instructs ConceptBase to tell the Telos definition when the
LPI file is loaded. Instead of this clause you may also tell the frame manually via the ConceptBase
user interface.

There are a few ConceptBase procedures in the body of the  {\tt compute\_sin} that are of importance here.
The procedure {\tt cbserver:arg2val}  converts the input parameter to a Prolog value. ConceptBase 
internally always uses object identifiers. They have to be converted to the Prolog representation in
order to enter them into some computation. The reverse procedure is {\tt cbserver:val2arg}. It converts
a Prolog value (e.g.\ a number) into an object identifier that represents the value. If necessary, a new
object is created for holding the new value.

The above code should be stored in a file like {\tt sin.swi.lpi}. This file has to be
copied into the ConceptBase database which holds the Telos definition of {\tt sin}.
You will have to restart the ConceptBase server after you have copied the LPI file into the
directory of the ConceptBase database.

If you want the new function to be available for all databases you construct, then you
have to copy the file  {\tt sin.swi.lpi} to the subdirectory {\tt lib/SystemDB} of your ConceptBase installation.
Note that your code might be incompatible with future ConceptBase releases. If you think that your code is
of general interest, you can share it with other ConceptBase users in the Software section
of the CB-Forum
(\url{http://merkur.informatik.rwth-aachen.de/pub/bscw.cgi/2768063}).


\subsection{Recursive function definitions}

Some functions like the Fibonacci numbers are defined recursively. ConceptBase supports
such recursive definitions. If the function is defined in terms of itself, then 
express the recursive definition in the membership constraint of the function:

{\small
\begin{verbatim}
fib in Function isA Integer with
  parameter
    n: Integer
  constraint
    cfib: $ (n=0) and (this=0) or
            (n=1) and (this=1) or
            (n>1) and (this=fib(n-1)+fib(n-2))
          $
end
\end{verbatim}
}

The variable {\tt this} stands for an answer object that fulfills the constraint {\tt cfib}.
Note that ConceptBase regards integers also as objects.
ConceptBase shall internally compile the disjunction into three formulas:

{\small
\begin{verbatim}
forall n,this/Integer (n=0) and (this=0) ==> fib(this)
forall n,this/Integer (n=1) and (this=1) ==> fib(this)
forall n,this/Integer (n>1) and (this = fib(n-1)+fib(n-2)) ==> fib(this)
\end{verbatim}
}

ConceptBase employs a bottom-up query evaluator to evaluate the recursive function.
Thus, the result of a function call {\tt fib(n)} shall only be computed once and then
re-used in subsequent calls.

If the recursion is not inside a single function definition but rather a property of a set
of function/query definitions, then you must use so-called {\em forward declarations}. They
declare the signature of a function/query before it is actually defined. A good example is
the computation of the length of the shortest path between two nodes.

{\small
\begin{verbatim}
spSet in GenericQueryClass isA Integer with
  parameter
    x: Node;
    y: Node
end
sp in Function isA Integer with
  parameter
    x: Node;
    y: Node
  constraint
    csp: $ (x=y) and (this=0) or
           (x nexttrans y) and (this = MIN(spSet[x,y])+1)
         $
end
spSet in GenericQueryClass isA Integer with
  parameter
    x: Node;
    y: Node
  constraint
   csps: $ exists x1/Node (x next x1) and (this=sp(x1,y)) $
end
\end{verbatim}
}

Here, the query class {\tt spSet} computes the set of length of shortest path between
the successors of a node {\tt x} and a node {y}. The length of the shortest path is then
simply 0 if {\tt x=y} or the minimum of the {\tt spSet[x,y]} plus 1 if there is a path from
x to y, and undefined else.
The signature of {\tt spSet} must be known for compiling {\tt sp} and vice versa. ConceptBase
has a single-pass compiler. Hence, it requires the forward declaration. The query
{\tt spSet} is not a function because it returns in general several numbers.
The complete example for computing the length of the shortest path is in the
CB-Forum, see \url{http://merkur.informatik.rwth-aachen.de/pub/bscw.cgi/1694234}.

Recursive function definitions require much care. Deductive rules shall always return
a result after a finite computation. This does not hold in general for recursive
function definitions when they use arithmetic subexpressions. These subexpressions
can create new objects (numbers) on the fly and could thus force ConceotBase into an
infinite computation. On the other hand, they are more expressive than pure
deductive rules and thus useful to analyze large models in a quantitative way.


