\chapter{Answer Formats for Queries}
\label{sec:answerformat}

The ConceptBase server provides an ASK command in its interface, which
allows to specify in which text-based format the answer should be returned. There
are two pre-defined formats: one for returning a list of object names,
and one for returning a list of object frames. These two formats can
be extended by user-defined answer formats.


Examples for answer formats are available from the CB-Forum,
see \url{http://merkur.informatik.rwth-aachen.de/pub/bscw.cgi/861803}.

To understand ConceptBase answer formats, we first look at
the syntax of the ASK command at the programming interface. We use here the syntax
of CBshell (section \ref{cha:cbshell}) since it can be tested directly in a command terminal:

\begin{verbatim}
   ask "<querydefinition>" FRAMES <answer-format> <roll-backtime>
   ask <querycall> OBJNAMES <answer-format> <roll-backtime>
\end{verbatim}

The first variant is for queries where the query is provided
as a Telos frame, the second one
is for parameterized query calls like {\tt Q[abc/param1]}.
If the query call contains special characters or is computes of several query calls
then surround them by double quotes. The rollback time is
typically {\tt Now}, i.e. the query refers to the current database state.
There are four options for the answer format:

\begin{description}
\item[{\tt LABEL}:]
      the answer shall be returned as a comma-separated list of object names, e.g.\ 
      {\tt bill, mary, john}
\item[{\tt FRAME}:]
      the answer shall be returned as a list of Telos frames. 
\item[{\tt JSONIC}:]
      the answer shall be returned as a list of JSON-like frames (experimental).
\item[{\tt FRAGMENT}:]
      return the answer objects as so-called SMLfragments (deprecated)
\item[{\tt FRAGMENTswi}:]
      return the answer objects as a SWI-Prolog list of terms
      smlFragment(O,In1,In2,Isa,With), each representing one answer object
\item[{\tt VIEW}:]
      return the as complex Telos frames (a frame can contain another frame as attribute); 
      to be used when you ask a View query
\item[{\em answer-format-name}:]
      Finally, the name of a user-defined answer format can be provided, like {\tt MyFormat}.
      This will override any answer format that is assigned via the {\tt forQuery} attribute
      for the given query. Hence, one can maintain several answer formats for the same query.
\item[{\tt default}:]
      This value is leaving the choice to the ConceptBase server. For function
      calls, the answer format {\tt LABEL} is selected, for other query calls,
      ConceptBase first checks if an answer format was defined for the query (attribute {\tt forQuery}).
      If that exists, it is selected, otherwise the format
      {\tt FRAME} is selected. If there are more than one,
      the first one is selected.
\end{description}

The answer format {\tt JSONIC} is an alternative to {\tt FRAME}. Here is an example contrasting
the two formats. The first two entries are in {\tt FRAME} format, the latter two in {\tt JSONIC}.


\begin{verbatim}
Employee in Class isA Person,Agent with
  attribute
     salary: Integer;
     name: String
  rule
     r1: $ forall e/Employee exists s/Integer (e salary s) $
end

bill in Employee with
  salary
     bsal: 1000
  name
     firstname: "William";
     lastname: "Smith"
  attribute
     creator: mjeu
end


{ "id" : "Employee",
  "type" : ["Class"],
  "super" : ["Person","Agent"],
  "salary" : "Integer",
  "rule/r1" : "$ forall e/Employee exists s/Integer (e salary s) $"
}

{ "id" : "bill",
  "type" : ["Employee"],
  "salary/bsal" : "1000",
  "name/firstname" : "\"William\"",
  "name/lastname" : "\"Smith\"",
  "attribute/creator": "mjeu"
}
\end{verbatim}




The command {\tt ask <querycall>}
is a shortcut for
\begin{verbatim}
   ask <querycall> OBJNAMES LABEL Now
\end{verbatim}

Assume, we want to execute a query call {\tt Q[abc/param1]} on the current database and
have the answer formatted according as frames, we would issue:
\begin{verbatim}
   ask Q[abc/param1] OBJNAMES FRAME Now
\end{verbatim}
We assume here that there is no user-defined answer format for query Q. If there is a 
user-defined answer-format like {\tt MyFormatA} and {\tt MyFormatB}, but the answer formats are not 
assigned via {\tt forQuery} to Q, we can call:
\begin{verbatim}
   ask Q[abc/param1] OBJNAMES MyFormatA Now
\end{verbatim}
and also
\begin{verbatim}
   ask Q[abc/param1] OBJNAMES MyFormatB Now
\end{verbatim}
If we assign {\tt MyFormatB} to Q via the {\tt forQuery} attribute, the last call is equivalent to
the first call using {\tt default} as answer format name.

Subsequently, the specification of such user-defined answer formats is presented.


\section{Basic definitions}

By default, ConceptBase displays answers to queries in the FRAME format (see `A' and `B' below). 
For many applications, other answer
representations are more useful. For example, relational data is more readable in a table structure. Another important example are XML data. If
ConceptBase is integrated into a Web-based information system, then answers in HTML format are quite useful. For this reason, answer format
definitions are provided.

Answer formats in ConceptBase are based on term substitutions where terms are evaluated against substitution rules defined by the answers to
a query. A substitution rule has the form $L \longrightarrow R$ with the intended meaning that a substring $L$ in a string is replaced by the substring $R$. The
object of a term substitution is a string in which terms may occur, for example:

\begin{quotation}
    this is a string called \verb+{a}+ with a term \verb+{b}+
\end{quotation}

Assume the substitution rules:

\begin{itemize}
\item \verb+{a}+ $\longrightarrow$ string no. \verb+{x}+
\item \verb+{b}+ $\longrightarrow$ that was subject to substitution
\item \verb+{x}+ $\longrightarrow$ 123
\end{itemize}

The derivation of a string with terms proceeds from left to right. First, the term occurence \verb+{a}+ is dealt with. The next term in the string is then \verb+{x}+
which is evaluated to \verb+123+. Finally, \verb+{b}+ is substituted and the result string is
{\tt this is a string called string no.\ 123 with a term that was subject to substitution.}

We denote a single derivation step of a string $S_1$ to a string $S_2$ by $S_1 \Longrightarrow S_2$. It is defined when there occurs a substring $L$ in $S_1$, i.e.
$S_1=V+L+W$ and a substitution rule $ L \longrightarrow R$ and $S_2=V+R+W$. The substrings $V$ and $W$ may be empty. A string $S$ is called ground when no
substition rule can be applied. A sequence $ S \Longrightarrow S_1 \Longrightarrow \ldots \Longrightarrow S_n$ is called a derivation of S. A complete derivation of S ends with a
ground string. In our example, the complete derivation is:


\begin{tabular}{rl}
     & {\tt this is a string called \verb+{a}+ with a term \verb+{b}+.}\\
 $\Longrightarrow$ & {\tt this is a string called string no.\ \verb+{x}+ with a term \verb+{b}+.}  \\
 $\Longrightarrow$ & {\tt this is a string called string no.\ 123 with a term that was} \\
  &  {\tt subject to substitution.} \\
\end{tabular}

An exception to the left-to-right rule are complex terms like \verb+{do({y})}+. Here, the inner term \verb+{y}+ is first evaluated (e.g. to 20) and then the result \verb+{do(20)}+ is evaluated.

In general, term substitution can result in infinite loops. This looping can be prevented either by restricting the structure of the substitution rule or
by terminating the substitution process after a finite number of steps. The end result of a substitution process of a string is called its derivation. In
ConceptBase, the substitution rules are guaranteeing termination except for the case of external procedures. The problem with the exception is
solved by prohibiting cyclic calls of the same external procedure during the substitution of
a call.
A cyclic call is a call that has the same function name (e.w. query class) and
the same arguments (expressed as parameter substitutions).

In ConceptBase, an answer format is an instance of the new pre-defined class `AnswerFormat'.

\begin{verbatim}
Individual AnswerFormat in Class with
  attribute
     forQuery : QueryClass;
     order : Order;
     orderBy : String;
     head : String;
     pattern : String;
     tail : String;
     fileType: String
end
\end{verbatim}

The first attribute assigns an answer format to a query class (a query may have at most one answer format). The second and third attribute specify
the sorting order of the answer, i.e. one can specify by which field an answer is sorted, and whether the answer objects are sorted `ascending' or
`descending' (much like in SQL). The 'orderBy' attribute specifies the property by which the answer shall be sorted.
The most common value is the expression "this", i.e.\ sort the answer by the name of the objects in the answer. You can also
specify an attribute expression such as "this.name" referring to an answer variable. If you specify "none" for 'orderBy',
then the answer is not sorted. If the number of objects in an answer exceeds 5000, then no sorting is applied due to
memory limitations.


The `head', `pattern', and `tail' arguments are strings that define the substring substitution when the answer is formatted. They contain substrings of
type {expr} that are replaced. The head and tail strings are evaluated once, independent form the answer to the query. Usually, they do not
contain expressions but only text. The response to a query is a set of answer objects {A1,A2,...}. The pattern string is evaluated against each
answer object. For each answer object, the derivation of the pattern is put into the answer text. Hence, the complete answer consists of

\begin{tabular}{rl}
   &  derivation of head string \\
   + & derivation of pattern string for answer object A1 \\
   + & derivation of pattern string for answer object A2 \\
   &     $\ldots$ \\
   + & derivation of tail string \\
\end{tabular}

The {\tt fileType} attribute is explained in section \ref{sec:af_filetype}.

In the next sections, we will explain more details about answer formats using the following example:
An answer object A to a query class QC has by default a `frame' structure

\begin{verbatim}
   A in QC with
     cat1
        label11: v11;
        label12: v12
        [...]
     cat2
        label21: v21
     [...]
   end
\end{verbatim}

In case of a complex view definition VC, the values vij can be answer objects themselves, e.g.

\begin{verbatim}
   B in VC with
     cat1
        label11: v11;
        label12: v12
        [...]
     cat2
        label21: v21 with
                   cat21
                     label211: v211
                 [...]
                 end
     [...]
   end
\end{verbatim}

\section{Constructs in answer formats}

\subsection{Simple expressions in patterns}

We first concentrate on the pattern attribute, i.e. they are not applicable in the head or tail attribute
of an answer format. The pattern of an answer format is applied to {\em each\/} answer object 
of a given query call, effectively transforming it according to the pattern.
The following expressions are allowed. Capital letters in the list below indicate that the 
term is a placeholder for some label occuring in the query definition or the answer objects. 

\begin{description}
\item[\{this\}]
  denotes the object name of an answer object (e.g.\ `A'). The syntax for object names is defined in
  section \ref{sec:representation}. In particular, attribution objects have names like {\tt mary!earns}.

\item[\{this.ATTRIBUTE\_CAT\}]
denotes the value(s) of the attribute `ATTRIBUTE\_CAT' of the current answer object `this'. The attribute must be defined in the query class
(retrieved\_attribute, computed\_attribute) or view (inherited\_attribute). Note that some attributes are multi-valued. For the answer object `A',
\verb+{this.cat1}+ evaluates to {\tt v11,v12} and \verb+{this.cat2}+ evaluates to {\tt v21}. For the complex object `B', a path expression like \verb+{this.cat2.cat21}+ is allowed
and yields {\tt v21}. Note that all such expressions are set-valued.

\item[\{this\^{}ATTRIBUTE\_LABEL\}]
denotes the value of the attribute `ATTRIBUTE\_LABEL' of the current answer object `this'. The attribute\_label is at the level of the answer
object, i.e. not at the class level but at the instance level. Therefore, this expression is rarely used because the instance level attribute labels are
usually unknown at query definition time. Example A and B: \verb+{this^label12}+ evaluates to {\tt v12}.

\item[\{this$\mid$ATTRIBUTE\_CAT\}]
denotes all attribute labels of the answer object `this' that are grouped under the category `ATTRIBUTE\_CAT' (defined in the query class).
Example: \verb+{this|cat1}+ evaluates to \verb+label11,label12+.
\end{description}


The derivation of pattern is performed for each answer object that is in the result set of a query. The answer object `A' induces the following
substition rules:


\begin{tabular}{rcl}
    \verb+{this}+ & $\longrightarrow$ & {\tt A} \\
    \verb+{this.cat1}+ & $\longrightarrow$ & {\tt v11,v12}\\
    \verb+{this.cat2}+ & $\longrightarrow$ & {\tt v21}\\
    \verb+{this^label11}+ & $\longrightarrow$ & {\tt v11}\\
    \verb+{this^label12}+ & $\longrightarrow$ & {\tt v12}\\
    \verb+{this^label21}+ & $\longrightarrow$ & {\tt v21}\\
    \verb+{this|cat1}+ & $\longrightarrow$ & {\tt label1,label2}\\
    \verb+{this|cat2}+ & $\longrightarrow$ & {\tt label21}\\
\end{tabular}

Extended examples for these simple expressions are given in {\tt simple\_answerformats1.sml} and {\tt simple\_answerformats2.sml} in
the directory \verb+$CB_HOME/examples/AnswerFormat/+.

Note that only objects that match the query constraint are in the answer. Particularly, the categories {\tt computed\_attribute} and {\tt retrieved\_attribute}
require that at least one filler for the respective attribute is present in an answer object!
Use ConceptBase views (`View') and {\tt inherited\_attribute} in case that
zero fillers are also allowed for answers.

There are few other simple expressions that may be useful. They just list attributes without having to refer to specific attributes.

\begin{description}
\item[\{this.attrCategory\}]
denotes all attribute categories that are present in an answer object. Example: For answer object `A' \verb+{this.attrCategory}+ evaluates to {\tt cat1,cat2}.

\item[\{this$\mid$attribute\}]
lists all attribute labels occuring in an answer object. Example: for answer object `A', \verb+{this|attribute}+ evaluates to {\tt label11,label12,label21}.

\item[\{this.attribute\}]
lists all attribute values occuring in an answer object. Example: for answer object `A', \verb+{this.attribute}+ evaluates to {\tt v11,v12,v21}.

\item[\{this.oid\}]
displays the internal object identifier of the answer object \{this\}.
\end{description}

For the answer object A, these expressions induce the following additional substitution rules:

\begin{tabular}{rcl}
    \verb+{this.attrCategory}+ & $\longrightarrow$ & {\tt cat1,cat2} \\
    \verb+{this|attribute}+ & $\longrightarrow$ & {\tt label11,label12,label21} \\
    \verb+{this.attribute}+ & $\longrightarrow$ & {\tt v11,v12,v21}\\
\end{tabular}


\subsection{Pre-defined variables}

The following variables can be used in the head, tail, and pattern of an answer format. They
do not refer to the variable {\em this}.

\begin{description}

\item[\{user\}]
outputs the user name of the current transaction; typically has the structure name@address

\item[\{transactiontime\}]
the time when the current transaction was started; has format YYYY-MM-DD hh:mm:ss and is based on Coordinated Universal Time (UTC),
formerly known as Greenwhich Mean Time

\item[\{cb\_version\}]
the version number of ConceptBase

\item[\{cb\_date\_of\_release\}]
the version number of ConceptBase

\item[\{currentmodule\}] is expanded to the name of the current module.

\item[\{currentpath\}] is expanded to the complete module path (starting with root module
   {\tt System}) that was active when starting the transaction

\item[\{database\}] is expanded to relative or absolute path of the database that was specified with the
   {\tt -d} option of the CBserver (section \ref{cha:cbserver}); if no database was specified, the
   variable is expanded to {\tt $<$none$>$}

\end{description}

ConceptBase also adds those command line parameters that deviate from their defaults to the set
of pre-defined variables. The most common ones are (see also section \ref{sec:module_views}):

\begin{description}

\item[\{loadDir\}]: directory from which the CBserver loads Telos source files at start-up;
                    command-line parameter {\tt -load}

\item[\{saveDir\}]: directory into which the CBserver saves Telos sources of modules at shut-down
                    or client logout;
                    command-line parameter {\tt -save}

\item[\{viewDir\}]: directory into which the CBserver materializes results of certain queries
                    command-line parameter {\tt -views}

\end{description}

Moreover, if the current transaction was a call of a query like {\tt MyQuery[v1/param1,...]} then
\{param1\} will be evaluated to {\tt v1}. This makes all parameter substitutions of a query call
available to answer formatting.

\subsection{Iterations over expressions}

In case of expressions with multiple values, the user may want to generate a 
complex text that uses one value after the other as a parameter. 
This is in particular useful to transform multiple attribute values like {\tt this.cat1}.
The `Foreach' construct has the format:

\verb+{Foreach( (expr1,expr2,...), (x1,x2,...), expr )}+

The expression {\tt expr1} is evaluated yielding each a list of solutions {\tt s11,s12,...} The same is applied to {\tt expr2} yielding a list
{\tt s21,s22,...} Then, the
variables {\tt x1,x2,...} are matched against the first entries of all lists, i.e.\ {\tt x1=s11,x2=s21,...} This binding is then applied to the expression
{\tt expr}
which should contain occurences of \verb+{x1}+, \verb+{x2}+, ... This replacement is continued with the second entries in all lists yielding bindings
{\tt x1=s12,x2=s22,...} This is continued until all elements of all lists are iterated. If some lists are smaller than others, the missing entries are replaced by {\tt NULL}.

During each iteration, the new bindings induce substitution rules for the binding

Iteration 1: \\
\begin{tabular}{rcl}
   \verb+{x1}+ & $ \longrightarrow $ & {\tt s11} \\
   \verb+{x2}+ & $ \longrightarrow $ & {\tt s21} \\
   ... \\
\end{tabular}

Iteration 2: \\
\begin{tabular}{rcl}
   \verb+{x1}+ & $ \longrightarrow $ & {\tt s12}\\
   \verb+{x2}+ & $ \longrightarrow $ & {\tt s22}\\
   ... \\
\end{tabular}

Note that the third argument {\tt expr} may contain other subexpressions, even a nested `Foreach'.
An example for iterations is given in
\verb+$CB_HOME/examples/AnswerFormat/iterations.sml+.

The Foreach construct contains three arguments separated by commas. 
These two commas are used by ConceptBase to parse the arguments
of the Foreach-construct and similar answer formatting expressions.
They are not printed to the answer stream.

\subsection{Special characters}

If one wants a comma inside an expresssion that shall be
visible in the answer, then one has to escape it `$\backslash$,'.
The same holds for the other special characters like `(', `)' etc.
Here is a short list of supported special characters. Some require
a double backslash. \newline

\begin{tabular}{lcl}
   {\tt \textbackslash\textbackslash n} & : & new line (ASCII character 10)\\
   {\tt \textbackslash\textbackslash t} & : & tab character\\
   {\tt \textbackslash\textbackslash b} & : & backspace character\\
   {\tt \textbackslash 0} & :  & empty string (no character)\\
   {\tt \textbackslash (} & :  & left parenthesis\\
   {\tt \textbackslash )} & :  & right parenthesis\\
   {\tt \textbackslash ,} & :  & comma\\
\end{tabular}

\vspace{0.5cm}
In principal, any non-alphanumerical character like '(',')','\{','\}',
'[', ']' can be referred to by the backslash operator. Note that the
vanilla versions of these characters are used to denote expressions in
answer formats. Hence, we need to 'escape' them by the backslash if they
shall appear in the answer.


\subsection{Function patterns}
\label{subsec:externalproc}

The substitution mechanism for answer formats recognizes patterns such as

   \verb+{F(expr1,expr2,...)}+ 

as function calls. An example is the {\tt ASKquery}  construct from from section \ref{subsec:askquery}.
The mechanism is however very general and can be used to realize almost arbitrary substititions.
The parentheses and the commas separating the arguments of {\tt F} are parsed by ConceptBase
and not placed on the output.
The following simple function patterns are pre-defined:

\begin{itemize}

\item \verb+{QT(expr)}+ puts the {\tt expr} into double quotes if not already quoted. 

\item \verb+{UQ(expr)}+ removes double quotes from {\tt expr} if present.

\item \verb+{ALPHANUM(expr)}+ outputs an alphanumeric transcription of {\tt expr}.
This is useful when an object names contains special characters but the
output format requires an alphanumeric label. If {\tt expr} already
evaluates to an alphanumeric label, then it is inserted unchanged.

\item \verb+{From(expr)}+ inserts the source object of {\tt expr}. The source object is
computed by the predicate {\tt From(x,o)} of section \ref{sec:CBL}. This pattern
is useful for printing attributes. ConceptBase will first apply pattern substitution to
{\tt expr} and then to the whole term \verb+{From(expr)}+.

\item \verb+{To(expr)}+ inserts the destination object of {\tt expr}. The destination object is
computed by the predicate {\tt To(x,o)} of section \ref{sec:CBL}.

\item \verb+{Label(expr)}+ inserts the label of the object referenced by {\tt expr}. The label is
computed by the predicate {\tt Label(x,l)} of section \ref{sec:CBL}.

\item \verb+{LabelAC(expr)}+ inserts the combination of the attribute category plus the label of the object referenced by {\tt expr} if that object is itself an attribute. Otherwise, only the label of the referenced object is inserted like for {\tt Label(expr)}. "AC" stands for "attribute category" An example output of LabelAC looks like {\tt name/billsname}. 

\item \verb+{Oid(expr)}+ inserts the identifier of the object referenced by {\tt expr}. 
\end{itemize}

Note that the argument {\tt expr} can be another pattern such as \verb+{this}+. Specifically, the expression
\verb+{Oid({this})}+ is equivalent to \verb+{this.oid}+. However, the {\tt Oid} pattern is
also applicable to patterns not including \verb+{this}+.

Examples are available from the CB-Forum, see \url{http://merkur.informatik.rwth-aachen.de/pub/bscw.cgi/2502545}.

The above set of patterns can be extended by user-defined functions via LPI plugins.
In principle, any routine that can be called from
the ConceptBase server, can also be called in an answer format. 
The programming interface is not documented here since this requires
extensive knowledge of the ConceptBase server source code. 
For the experienced user, we provide an example in  the 
subdirectory {\tt examples/AnswerFormat} of the ConceptBase installation
directory, see files {\tt externalcall.sml} and
{\tt externalcall.swi.lpi} ({\tt externalcall.bim.lpi} for BIM-Prolog variant). 
Note that one has to create a persistent database, load the model {\tt externalcall.sml} into it, then terminate
the ConceptBase server, and then copy the file {\tt externalcall.swi.lpi} or {\tt externalcall.bim.lpi} 
into the database directory (see also appendix \ref{cap:lpi}). 
Thereafter, restart ConceptBase and call the query {\tt EmpDept}.


\subsection{Calling queries in answer formats}
\label{subsec:askquery}

A query call within an answer format is an example of a so-called external procedure.
The pattern as well as head and tail of an answer format may contain the call to a query (possibly the same for which the answer format was
defined for). This allows to generate arbitrarily complex answers.

\verb+{ASKquery(Q[subst1,subst2,...],formatname)}+

The argument {\tt Q} is the name of a query. The arguments {\tt subst1,subst2} are parameter substitutions (see section \ref{sec:CBQL}). The argument
{\tt formatname} specifies the answer format for the query call {\tt Q[subst1,subst2,...]}. The answer format may also have parameters (see below).
If you use {\tt default} as answer format name, then the ConceptBase server will pick the default format. This is {\tt LABEL} (list of object names) for
function calls.
For other queries, it is the first answer format that list the query {\tt Q} in its {\tt forQuery} attribute. If no such answer format exists,
the format {\tt FRAME} (Telos frames) is chosen.


The effect of {\tt ASKquery} in an answer format is that the above query call is evaluated
and the {\tt ASKquery} expression is replaced by the complete answer to the query.
In terms of the substitution, the following rule is applied:

   \verb+{ASKquery(Q[subst1,subst2,...],default)}+ $\longrightarrow$ {\tt X}

where {\tt X} is the result of the query call \verb+Q[subst1,subst2,...]+ after derivation of the arguments subst1, subst2 etc. This sequencing is important since
an ASKquery call can contain terms that are subject to substitution, e.g.\
\newline
\verb+{ASKquery(MyQuery[{this.cat1}/param1,{this.{x1}}/{x2}],default)}+.
\newline
ConceptBase will always start to evaluate left to right and the
innermost terms before evaluating the terms that contain inner terms. Hence, the derivation sequence is

{\small
\begin{tabular}{rll}
   & \verb+{ASKquery(MyQuery[{this.cat1}/param1,{this.{x1}}/{x2}],default)}+ & \\
 $\Longrightarrow$ & \verb+{ASKquery(MyQuery[alpha/param1,{this.{x1}}/{x2}],default)}+ & (r2)\\
 $\Longrightarrow$ & \verb+{ASKquery(MyQuery[alpha/param1,{this.name}/{x2}],default)}+ & (r1)\\
 $\Longrightarrow$ & \verb+{ASKquery(MyQuery[alpha/param1,"smith"/{x2}],default)}+     & (r3)\\
 $\Longrightarrow$ & \verb+{ASKquery(MyQuery[alpha/param1,"smith"/param2],default)}+   & (r4)\\
 $\Longrightarrow$ & \verb+The answer is ...+                                        & (r5)\\
\end{tabular}
}

where we assume the following example substitution rules

\begin{tabular}{lrcl}
r1: & \verb+{x1}+ & $\longrightarrow$ & {\tt name} \\
r2: & \verb+{this.cat1}+ & $\longrightarrow$ & {\tt alpha} \\
r3: & \verb+{this.name}+ & $\longrightarrow$ & \verb+"smith"+ \\
r4: & \verb+{x2}+ & $\longrightarrow$ & {\tt param2} \\
r5:  & \verb+{ASKquery(MyQuery[...],default)}+ & $\longrightarrow$ & {\tt The answer is ...}\\
\end{tabular}

This guarantees that the query call is `ground', i.e. does not contains terms which are subject to substitution.

The {\tt ASKquery} construct allows to introduce recursive calls during the derivation of a query since there can (and should) be an answer format for Q
which may contain expressions {\tt ASKquery} itself. In principle, this allows infinite looping. However, the answer format evaluator prevents such
loops by halting the expansion when a recursive call with same parameters has occured. The answer then contains an `\~' character at the position
where the loop was detected. Additionally, an error message is written on the console window
of the ConceptBase server (tracemode must be at least low).

A simple example for use of {\tt ASKquery} is given in {\tt recursive-answers.sml}. The example uses a view instead of a query class in order to include
also answers into the solution which not have a filler for the requested attribute, i.e. hasChild is {\tt inherited\_attribute}, not
{\tt retrieved\_attribute}.

It is common practice to combine the {\tt ASKquery} construct with `Foreach' in order to display an iteration of objects in the same way. The user
should define an answer format for the iterated query Q as well.

Do not mix the use of {\tt ASKquery} with the view definitions in ConceptBase! The nesting depth of a view is determined by the view definition. The
nesting depth of an answer generated by expansion of {\tt ASKquery} is only limited by the complexity of the database. For example, one can set up
an ancestor database and display all descendants of a person and recursivley their descendants in a single answer string for that person.
The nested {\tt ASKquery} inside an answer format usually results in the unfolding also using
an answer format (possibly the same as used for the original query). This feature
allows the user to specify very complex structured answers that might even contain the
complete database. In particular, complex XML representations can be constructed
in this way.


\subsection{Expressions in head and tail}

The features `head' and `tail' are similar to pattern. The difference is that any expression using `this' (the running variable for answer objects) is disallowed.
This only leaves function patterns such as {\tt ASKquery} expressions and pre-defined patterns such as  \verb+{user}+.
Of course, the head and tail strings can contain multiple occurences
of ASKquery or other function patterns.


\subsection{Conditional expressions}

Conditional expressions allow to expand a substring based on the evaluation of a condition. The syntax is:

\verb+{IFTHENELSE(predicate,thenstring,elsestring)}+

The `predicate' can be one of
\begin{itemize}
\item \verb+{GREATER(expr1,expr2)}+
\item \verb+{LOWER(expr1,expr2)}+
\item \verb+{EQUAL(expr1,expr2)}+
\item \verb+{AND(expr1,expr2)}+
\item \verb+{OR(expr1,expr2)}+
\item \verb+{ISFIRSTFRAME()}+
\item \verb+{ISLASTFRAME()}+
\end{itemize}

Example: \verb+{GREATER({this.salary},10000)}+.
Note that the arguments may also contain expressions.
The predicate \verb+{ISFIRSTFRAME()}+ is true, when ConceptBase starts with processing
answer frames. It is false, when the first frame has been processed. The predicate \verb+{ISlASTFRAME()}+
is true when ConceptBase starts with processing the last answer frame for a given query. Otherwise,
it is false.
An example for conditional expressions is provided in the CB-Forum, see
file "csv.sml" in \url{http://merkur.informatik.rwth-aachen.de/pub/bscw.cgi/861803}.
In most cases, the {\tt IFTHENELSE} construct can be avoided by a more elegant query class
formulation.



\subsection{Views and path expressions}

If the answer format is defined for a complex view, then path expressions like {this.cat2.cat21...} for the parts of the complex answer can be
defined.
An example for use of answer formats for views is given in {\tt views.sml}.

The reader should note that complex path expressions can only refer to components that were defined as retrieved, computed, or inherited
attributes in the view definition. For example, one cannot refer to {this.dept.budget} in the example view {\tt EmpDept} in {\tt views.sml}
since it is not a retrieved attribute of the
{\tt dept} component of the view definition {\tt EmpDept}. The second expression of the answer format {\tt EmpDeptFormat}
uses the builtin procedure {\tt UQ}. It removes the quotes `\verb+"+' from a
string. Analogously, a procedure {\tt QT} can be used to put quotes around a term.


\subsection{Encoding answers via answer formats}

The result of applying an answer format to an answer is always a text. You can use the 'encoding' attribute of answer formats
to transform the text according to the desired encoding. This is specified by a simple attribute like for example
{\small
\begin{verbatim}
FormatX in AnswerFormat with
  attribute encoding: "string"
  ...
end
\end{verbatim}
}

ConceptBase implements the following encoding types:

\begin{itemize}
\item {\bf string}: enclose the answer in double quotes.
\item {\bf telosname}: enclose the answer in double quotes if not a legal Telos label, otherwise leave unchanged.
\item {\bf alphanumeric}: replace all non-alphanumeric characters by the sequence {\tt \_Cxy\_}, where {\tt xy} is the
ASCII code of the character.
\end{itemize}

If an answerformat has no encoding attribute, then the answer is left unchanged. Encodings can be useful to guarantee
that an answer is syntactically correct, e.g. a syntactically correct XML text. Note that answer formats support
embedded queries and some elements of an answer may be computed by the {\tt resultOf} function, which also applies
answerformats to their result.
If a specified encoding is not implemented by the ConceptBase server, then no encoding is applied.




\section{Parameterized answer formats}

The general way to use an answer format for a query is to define the attribute {\tt forQuery}. Another possibility
is to specify the answer format for a query is to use the answer representation field of the ASK method in the IPC interface.

The following code is an example for specifying a user-defined answer in the ASK method.
This example is written in Java and uses the standard Java API of ConceptBase (see
the Programmer's Manual for details).

{\small
\begin{verbatim}
import i5.cb.api.*;

public class CBAnswerFormat {

    public static void main(String[] argv) throws Exception {

        CBclient cb=new CBclient("localhost",4001,null,null);

        CBanswer ans=cb.ask("find_specializations[Class/class,TRUE/ded]",
                        "OBJNAMES","AFParameter[bla/somevar]","Now");
        System.out.println(ans.getResult());
        cb.cancelMe();
    }
}
\end{verbatim}
}

In the example, a connection is made to a ConceptBase server on localhost listening on port 4001.
The {\tt ask}-method of the CBclient class sends a query to the server. The first argument
is the query, the second argument is the format of the query (in this example, it is just one object name),
the third argument is the answer representation, and the last argument is the rollback time.

The third argument, is the the answer representation.
There are four predefined answer representations. {\tt FRAME} returns the answers as
Telos frames, including retrieved and computed attributes. {\tt LABEL} returns only
the names of the answer objects as a comma-separated list. Thirdly, the format 
{\tt JSONIC} returns the answer in JSON-like frames.
Finally, {\tt default}
lets the CBserver choose between {\tt LABEL}  (for function calls), the explicit answer format assigned for
a query (attribute {\tt forQuery}),
and {\tt FRAME} (otherwise). Besides these pre-defined answer representations, one can specify
user-defined answer formats. This is also the preferred way.
In our case, it is a parameterized answer format: \verb+AFParameter[somevalue/somevar]+.
This means that the result of the query will be formatted according to
the answer format {\tt AFParameter} and the variable {\tt somevar}
will be replaced with {\tt somevalue}. The variable can be used like any other
expression, i.e.\ it must be enclosed in \verb+{}+.

The following definition of {\tt AFParameter} is an example, how
the parameter can be used in the pattern. If the parameter is not
specified, the string \verb+{somevar}+ will not be replaced.

{\small
\begin{verbatim}
Individual AFParameter in AnswerFormat with
  head hd : "<result>"
  tail tl : "</result>"
  pattern
     p : "
<object>
  <type>{somevar}</type>
  <name>{this}</name>
</object>"
end
\end{verbatim}
}

Note, that you can use any answer format (with or without parameters) as
answer representation in the ASK method.


\section{File type of answer formats}
\label{sec:af_filetype}

The optional {\tt fileType} attribute of answer formats is used by the server-side materialization
of query results (section \ref{sec:module_views}). ConceptBase will use the specified file type when
storing the query results in the file system. The default value is "txt". 
The attribute is single-valued though single-valuedness is not enforced. 


\section{Bulk query calls}
\label{sec:bulkqueries}

It is sometimes useful to call the same query class with multiple arguments in a single call
rather than in a sequence of calls. Each individual call from a ConceptBase client to the server
comes with a certain latency time. Thus, if one would have to call the same query 
for dozens of arguments in a sequence, most of the answer time would actually be the latency time.

To address this problem, the CBserver offers a query call pattern for bulk queries:

{\small
\begin{verbatim}
bulk[q,x1,x2,x3,...]
\end{verbatim}
}

The query {\tt q} stands for a query class with a single parameter. The bulk query is converted
by the CBserver into the following sequence of query calls:

{\small
\begin{verbatim}
q[x1],q[x2],q[x3],...
\end{verbatim}
}

The answers to the query call are collected into a single answer set and then transformed with
the answer format of the query call. Hence, the answer format is applied to the whole answer
and not to the part answers.

Example:

{\small
\begin{verbatim}
   ask bulk[Q,abc,def] OBJNAMES MyFormatA Now
\end{verbatim}
}

Sorting of answers is disabled for bulk queries in order to return the answers in the sequence
indicated by the arguments of the bulk query call. Here, the answer to the argument {\tt abc}
shall precede the answer to argument {\tt def}. Arguments that do not reference an existing
object are removed from the argument list by the CBserver before answering the query.
Bulk queries are only supported for generic query classes with a single parameter.
The query class may not be a builtin query class.
%
The main purpose of bulk queries is to speed up the interaction between the ConceptBase clients,
such as CBGraph, and the CBserver. You can however use them with the CBShell.


