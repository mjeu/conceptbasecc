
\chapter{Examples}
\label{cap:examples}

\section{Example model: the employee model}
\label{sec:employee-model}

The Employee model can be found in the directory \verb+$CB_HOME/examples/QUERIES/+.
It consists out of the following files:

\begin{description}
\item[Employee\_Classes.sml:] The class definition
\item[Employee\_Instances.sml:] Some instances for this model
\item[Employee\_Queries.sml:] Queries for this model
\end{description}

Note, that the files must be loaded in this order into the server.

\section{A Telos modeling example - ER diagrams}
\label{sec:ER-diagrams}

\subsection{The basic model}
\label{subsec:ER-basic-model}
This example gives a first introduction into some features introduced in
ConceptBase version 4.0. It demonstrates the use of {\em meta formulas} and
{\em graphical types} while building a Telos model describing
Entity-Relationship-Diagrams. The following model forms the basis:\\

{\small
\begin{verbatim}
{**************************}
{*                        *}
{* File: ERModelClasses   *}
{*                        *}
{**************************}

Class Domain
end

Class EntityType with attribute
     eAttr : Domain;
     keyeAttr : Domain

end

Class RelationshipType with attribute
     role : EntityType
end

Class MinMax
end

"(1,*)" in MinMax with
end

"(1,1)" in MinMax with
end

Attribute RelationshipType!role with attribute
     minmax: MinMax
end
\end{verbatim}
}

The model defines the concepts of {\em EntityTypes} and
{\em RelationshipTypes}.
Each entity that participates in a relationship plays a
particular {\em role}.
This role is modelled as a Telos {\em attribute-link} of the
object {\em RelationshipType}.
The {\em attributes} describing the entities are modelled
as Telos {\em attribute-links}
to a class {\em Domain} containing the value-sets. Roles
can be restricted by
the ``(min,max)''-constraints ``{\em (1,*)}'' or ``{\em (1,1)}''.
The next model contains a concrete ER-model.

{\small
\begin{verbatim}
{**************************}
{*                        *}
{* File: Emp_ERModel      *}
{*                        *}
{**************************}

Class Employee in EntityType with
  keyeAttr,attribute
     ssn : Integer
  eAttr,attribute
     name : String
end

Class Project in EntityType with
  keyeAttr,attribute
     pno : Integer
  eAttr,attribute
     budget : Real
end

Integer in Domain end

Real in Domain end

String in Domain end

Class WorksIn in RelationshipType  with role,attribute
     emp : Employee;
     prj : Project
end

WorksIn!emp with minmax
     mProjForEmp: "(1,*)"
end

WorksIn!prj with minmax
     mEmpForProj: "(1,*)"
end



ConceptBase in Project with pno
     cb_pno : 4711
end

Martin in Employee with ssn
      martinSSN : 4712
end

M_CB in WorksIn with
   emp
      mIsEmp : Martin
   prj
      cbIsPrj : ConceptBase
end

Hans in Employee with ssn
     hans_ssn : 4714
end
\end{verbatim}
}

The entity-types {\em Employee} and {\em Project} participate in a
binary relationship {\em WorksIn}. The attributes {\tt Employee!ssn}
and {\tt Project!pno} are key-attributes of the respective objects.

\subsection{The use of meta formulas}
The above model distinguishes {\em attributes} and {\em key attri\-butes}.
One important constraint on key attributes is monovalence.
In the previous releases of ConceptBase it was possible to declare
Telos-attributes as instance of the attribute-categories {\em single} or
{\em necessary}, but the constraint ensuring this property could not be
formulated in a general manner, because the use of variables as
placeholders for Telos-classes e.g in an {\em In}-Literal was prohibited.
 To overcome this restriction, {\em meta formulas}
have been integrated into the system. An assertion is a {\em meta formula} if
it contains such a class-variable. The system tries to replace this
{\em meta formula} by a set of semantic equivalent formulas which contain
no class-variables. In previous releases properties as
{\em single} or {\em necessary} had to be  ensured ``manually'' by adding a
constraint for each such attribute.  This job is now performed automatically
by the system.

\subsubsection{Example: {\em necessary} and {\em single}}
The following meta formula ensures the {\em necessary} property of attributes,
which are instances of the category {\tt Proposition!necessary}. The semantics of
this property is, that for every instance of the source class of this
attribute there must exist an instantiation of this attribute.

{\small
\begin{verbatim}
Class with constraint, attribute
        necConstraint:
       $ forall c,d/Proposition p/Proposition!necessary x,m/VAR
            P(p,c,m,d) and (x in c) ==>
             exists y/VAR (y in d) and (x m y) $
end
\end{verbatim}
}

It reads as follows:\\

\begin{quote}
For each attribute with label {\tt m} between the classes {\tt c} and
{\tt d}, which instantiates the attribute {\tt Proposition!necessary} and for each
instance {\tt x} of {\tt c} there should exist an instance {\tt y}
of {\tt d} which is destination of an attribute of {\tt x} with category {\tt m}.
\end{quote}

One should notice that the predicates {\tt In(x,c)} and {\tt In(y,d)} cause
this formula to be a meta formula. The instantiation of {\tt x} and {\tt m}
to the class VAR is just a syntactical construct. Every variable in a
constraint has to be bound to a class. This restriction is somehow contrary
to the concept of meta formulas and the VAR-construct is a kind of compromise.
The resulting {\tt In}-predicates are discarded during the processing of
meta formulas. The VAR-construct is only allowed in meta formulas. It enables
the user to leave the concrete classes of {\tt x} and {\tt m} open, without
instantiating them to for example to {\em Proposition}.

The {\em single}-constraint can be defined in analogy.
%
These constraints can be added to the system as if they were ``normal''
constraints. Their effect becomes visible, when declaring attributes
as {\em necessary} or {\em single}. This is done in the following model.

{\small
\begin{verbatim}
{**************************}
{*                        *}
{ File: ERSingNec         *}
{*                        *}
{**************************}

{* necessary constraint (metaformula) *}
Class with constraint
    necConstraint:
    $ forall c,d/Proposition p/Proposition!necessary x,m/VAR
            P(p,c,m,d) and (x in c) ==>
             exists y/VAR (y in d) and (x m y) $
end




{* every Entity has a key *}
Class EntityType with
  necessary
     keyeAttr : Domain
end


{* single constraint (metaformula) *}
Class with constraint
    singleConstraint :
    $ forall c,d/Proposition p/Proposition!single x,m/VAR
              P(p,c,m,d) and (x in c) ==>
                (
                  forall a1,a2/VAR
                    (a1 in p) and (a2 in p) and Ai(x,m,a1) and Ai(x,m,a2) ==>
                   (a1=a2)
                ) $
end

{* every Entity key is monovalued ( = necessary and single)*}
Class EntityType with rule
     keys_are_necessary:
        $forall a/EntityType!keyeAttr In(a,Proposition!necessary)$;
     keys_are_single:
        $forall a/EntityType!keyeAttr In(a,Proposition!single)$
end
\end{verbatim}
}


The effects of this transaction can be shown by displaying
the instances of instances of the class {\em metaMSFOLconstraint}.
The {\em single}- and {\em necessary} constraints are inserted into this
class after adding them to the system. These constraints themselves can have
specializations: constraints which are added automatically to the system
when inserting objects into the attribute-category {\em single} resp.
{\em necessary}.

For the ER-example, one of the created formulas reads as:

{\small
\begin{verbatim}
$ forall x/EntityType (exists y/Domain   (x keyeAttr y)) $
\end{verbatim}
}

We observe the relationship to the {\tt necConstraint}-formula:
the formula has been generated by computing one extension of
{\tt In(p,Proposition!necessary) and  P(p,c,m,d)} and
replacing the predicates {\tt In(p, Proposition!necessary} and {\tt P(p,c,m,d)} by this
extension, which
results in the following substitution for the remaining formula:



\begin{center}
{\tt
\begin{tabular} {|l|l|} \hline
c & EntityType \\ \hline
d & Domain  \\ \hline
m & keyeAttr \\ \hline
\end{tabular}
}
\end{center}

\subsubsection{Metaformulas defining sets of rules}
Another use of {\em Metaformulas} is the formulation of deductive
rules. Metaformulas defining deductive rules extend the possibilities
of defining derived knowledge.

\paragraph{Assignment  of graphical types:}
This example first demonstrates the use of meta formulas
to assign graphical types to object-categories. The
minimal graphical convention for ER-diagrams is to use
rectangular boxes for entities and diamond-shaped boxes
for relationships. In our modelling example these
graphical types are assigned to objects which are
instances of {\em EntityType} or {\em RelationshipType}
and to instances of these objects.

{\small
\begin{verbatim}
{*******************************************}
{*                                         *}
{* File: ERModelGTs                        *}
{* Definition of the graphical palette for *}
{* ER-Diagrams for use on color displays   *}
{*                                         *}
{*******************************************}


{* graphical type for inconsistent roles *}
Class InconsistentGtype in JavaGraphicalType with
  attribute,property
     textcolor : "0,0,0";
     edgecolor : "255,0,0";
     edgewidth : "2"
implementedBy
     implBy : "i5.cb.graph.cbeditor.CBLink"
priority
     p : 14
end


{* graphical type for entities *}
Class EntityTypeGtype in JavaGraphicalType with
property
     bgcolor : "10,0,250";
     textcolor : "0,0,0";
     linecolor : "0,55,144";
     shape : "i5.cb.graph.shapes.Rect"
implementedBy
     implBy : "i5.cb.graph.cbeditor.CBIndividual"
priority
    p : 12
end

{* graphical type for relationships *}
Class RelationshipGtype in JavaGraphicalType with
property
     bgcolor : "255,0,0";
     textcolor : "0,0,0";
     linecolor : "0,0,255";
     shape : "i5.cb.graph.shapes.Diamond"
implementedBy
     implBy : "i5.cb.graph.cbeditor.CBIndividual"
priority
    p : 13
end


{* graphical palette *}
Class ER_GraphBrowserPalette in JavaGraphicalPalette with
  contains,defaultIndividual
     c1 : DefaultIndividualGT
  contains,defaultLink
     c2 : DefaultLinkGT
  contains,implicitIsA
     c3 : ImplicitIsAGT
  contains,implicitInstanceOf
     c4 : ImplicitInstanceOfGT
  contains,implicitAttribute
     c5 : ImplicitAttributeGT
  contains
     c6 : DefaultIsAGT;
     c7 : DefaultInstanceOfGT;
     c8 : DefaultAttributeGT;
     c14 : EntityTypeGtype;
     c15 : RelationshipGtype;
     c16 : InconsistentGtype
end


EntityType with rule
    EntityGTRule:
        $ forall e/EntityType  A(e,graphtype,EntityTypeGtype)$ ;
    EntityGTMetaRule:
        $ forall x/VAR (exists e/EntityType In(x,e)) ==>
          A(x,graphtype,EntityTypeGtype)$
end


RelationshipType with rule
     RelationshipGTRule:
         $ forall r/RelationshipType A(r,graphtype,RelationshipGtype)$ ;
     RelationshipGTMetaRule:
         $ forall x/VAR (exists r/RelationshipType In(x,r))  ==>
           A(x,graphtype,RelationshipGtype) $
end
\end{verbatim}
}

To activate the {\em ER\_GraphBrowserPalette }, select this graphical
palette when you start the Graph Editor or make a new connection in
the Graph Editor (see section \ref{sec:graphed}).

\paragraph{Handling inconsistencies}
The {\em necessary} and {\em single} conditions on attributes from
the previous section could also be expressed as deductive rule.
The difference is, that if they are formulated as constraints,
every transaction violating the constraint would be rejected.
The definition of rules handling {\em necessary} and {\em single} enables
the user to handle inconsistencies in his model.
The following example demonstrates this concept in the context of
our ER-model. The example defines rules handling
the restriction of roles by the ``(min,max)''-constraint
``{\em (1,*)}''. This restriction is  not implemented
using constraints. Instead a new Class {\em Inconsistent} is
defined, containing all role-links which violate the
``{\em (1,*)}'' constraint. These inconsistent links get
a different graphical type (e.g a red coloured attribute link)
than consistent role links to visualize the inconsistency graphically.

{\small
\begin{verbatim}
{*******************************************}
{*                                         *}
{* File: ERIncons                          *}
{* Definition of a Class "Inconsistent"    *}
{* containing roles violating the "(1,*)"  *}
{* constraint                              *}
{*                                         *}
{*******************************************}

{* new attribute category revNec for attributes
   which are "reverse necessary" *}
Class with attribute
           revNec : Proposition
end

{* roles with "(1,*)" must fullfill revNec property *}
RelationshipType with rule, attribute
    revNecRule:
    $ forall ro/RelationshipType!role
        A(ro,minmax,"(1,*)")  ==> In(ro,Class!revNec) $
end

{* definition of Class "Inconsistent" *}
{* forall instances "p" of Class!revNec:
   If there exists a destination class "d", there must
   be a source class "c" with an attribute instantiating "p",
   otherwise "p" is inconsistent
*}
Class Inconsistent with rule, attribute
   revNecInc:
   $ forall p/Class!revNec
       (exists c,m,d/VAR  y/VAR  P(p,c,m,d) and In(y,d) and
          not(exists x/VAR  In(x,c) and A(x,m,y))) ==>
          In(p,Inconsistent)$
end
\end{verbatim}
}

To activate the different graphical representation of inconsistent roles,
the definition of the graphical type for attributes has to be modified.
Tell the following frame:

{\small
\begin{verbatim}
Inconsistent with rule,attribute
     incRule :
        $ forall e/Attribute In(e,Inconsistent) ==>
          (e graphtype InconsistentGtype)$
end
\end{verbatim}
}

The effect of these transactions can be shown when starting the
{\em Graph Editor} and displaying the attributes of the {\tt RelationshipType}
instance {\tt WorksIn}. Be sure to switch the graph editor to the palette
{\tt ER\_GraphBrowserPalette}
before doing so (see section \ref{sec:graphed}). The attribute
{\tt WorksIn!emp} is displayed as red link like in figure \ref{fig:er_incons}.
If you had already started the graph editor before telling the last frame, you should
synchronize it with the CBserver via the menu Current connection.
By telling

{\small
\begin{verbatim}
H_CB in WorksIn with
   emp
      hIsEmp : Hans
   prj
      cbIsPrj : ConceptBase
end
\end{verbatim}
}

the inconsistency is removed from the model. To see the update
of the graphical type in the Graph Editor, you have to select the
inconsistent link and select ``Validate and update selected objects''
from the ``Current connection'' menu.

\cbfigure{er_incons}{0.9\textwidth}{Graph Editor showing the example model with inconsistent link}

\subsection{Limitations and final remarks}
Metaformulas extend the expressive power of defining rules
and constraints for  ConceptBase Objectbases. The implementation
of this mechanism is not complete at the moment, but should
enable the use of the most frequently requested concepts like {\em single} and
{\em necessary}. Some limitations are listed below:

\begin{itemize}
\item    {\bf limited partial evaluation} \\
    The partial evaluation procedure is limited to a
    conjunction of predicates, preceeded by
    {\em forall}-quantors.
\item    {\bf no source-to-source transformation} \\
    Formulas are not converted automatically into the form mentioned above
    supported by the meta formula mechanism, even if they could be transformed.
    If the class-variables can't be bound using partial evaluation of the
    input-formula, the formula is rejected, even if there exists an equivalent
    formula, in which partial evaluation could be used to bind the class-variables.
\item    {\bf not all classes are supported} \\
    Generated formulas where variables are quantified over
    instances of the following classes or attributes of them
    will be ignored. Those classes are: \\
    {\em Boolean,Integer,Real,String,TransactionTime, \\
        MSFOLassertion,MSFOLrule,MSFOLconstraint, \\
    metaMSFOLconstraint,metaMSFOLrule, \\
        BDMConstraintCheck,BDMRuleCheck,LTruleEvaluator,ExternalReference, \\
        QueryClass, BuiltinQueryClass,AnswerRepresentation, \\
    GraphicalType,X11\_Color,ATK\_TextAlign, \\
        ATK\_Fonts,ATK\_LineCap,ATK\_ShapeStyle} \\
    The justification is twofold. Some of these generated formulas, e.g. a
    formula beginning with
    \begin{verbatim} $ forall x/1 ...$ \end{verbatim}
    are regarded as redundant, because the object {\em 1} as instance
    of {\em Integer} should have no instances.
        Another justification is, that the use of meta formulas should be
    restricted to user-defined modeling tasks. Manipulation of the most system classes
        is disabled for reasons of efficiency and safety.
\end{itemize}

In the case of deductive rules, additional problems arise similar to
the straticfication problem. At the moment only monotonous transactions
are allowed. This means that generated formulas can only be
inserted or deleted during one transaction, both operations
at the same time are not permitted.


The meta formula mechanism also influences the efficiency of the
system: every transaction has to be supervised whether it affects the
meta formulas or one of the generated formulas. If the preconditions
of the generated formulas don't hold anymore, e.g. if
the instances of {\tt EntityType!eAttr} are no longer instances of
{\tt Proposition!necessary} in the previous model, the corresponding
generated formula has to be deleted. If additional
attributes are instantiated to {\tt Proposition!necessary}, additional
formulas have to be created. The process of supervising
the transactions is quite expensive and if it slows down the
overall performance too much, some of the meta formuals can
be disabled temporarily (Untelling a meta formula removes all
the code generated).


Many more examples for meta formulas, e.g. for defining
transitivity of attributes, are available from
the CB-Forum (\url{http://merkur.informatik.rwth-aachen.de/pub/bscw.cgi/1042523}).

