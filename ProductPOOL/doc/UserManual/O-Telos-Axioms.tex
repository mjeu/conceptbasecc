
\chapter{O-Telos Axioms}
\label{cha:otelos-axioms}

O-Telos is the variant of Telos (originally defined by John Mylopoulos,
Alex Borgida, Manolis Koubarakis and others) that is used by the
ConceptBase system.
This list is the complete set of pre-defined axioms of O-Telos
and thus defines the semantics of a O-Telos database (without user-defined
rules and constraints).
The subsequent axioms are written in a first-order logic syntax but all
can be converted to Datalog with negation (though there is some choice
in the conversion wrt. mapping to rules or constraints).



\begin{itemize}

\item Axiom 1: Object identifiers are unique.\newline
$\forall ~o,x_1,n_1,y_1,x_2,n_2,y_2 ~P(o,x_1,n_1,y_1) \land
         P(o,x_2,n_2,y_2) \Rightarrow$\newline
$(x_1=x_2) \land (n_1=n_2) \land (y_1=y_2)$
%\bigskip
\item Axiom 2: The name of individual objects is unique.\newline
$\forall ~o_1,o_2,n ~P(o_1,o_1,n,o_1) \land P(o_2,o_2,n,o_2) \Rightarrow
                     (o_1=o_2)$
%\bigskip
\item Axiom 3: Names of attributes are unique in 
conjunction with the source object.\newline
$\forall ~o_1,x,n,y_1,o_2,y_2 ~P(o_1,x,n,y_1) \land
    P(o_2,x,n,y_2) \Rightarrow
(o_1=o_2) \lor (n=in) \lor (n=isa)$
%\bigskip
\item Axiom 4: The name of instantiation and specialization objects
({\it in, isa\/}) is unique in conjunction with source and destination
objects.\newline
$\forall ~o_1,x,n,y,o_2 ~P(o_1,x,n,y) \land
              P(o_2,x,n,y) \land
((n = in) \lor (n = isa)) \Rightarrow (o_1=o_2)$
%\bigskip
\item Axioms 5,6,7,8: Solutions for the predicates \In, \Isa,
and A are derived from the object base.\newline
$\forall ~o,x,c ~P(o,x,in,c) \Rightarrow \In(x,c) $\newline
$\forall ~o,c,d ~P(o,c,isa,d) \Rightarrow \Isa(c,d)$\newline
$\forall ~o,x,n,y,p,c,m,d ~P(o,x,n,y) \land P(p,c,m,d) \land \In(o,p)
        \Rightarrow AL(x,m,n,y)$\newline
$\forall ~x,m,n,y ~AL(x,m,n,y) \Rightarrow A(x,m,y)$
%\bigskip
\item Axiom 9: An object $x$ may not neglect an attribute definition
in one of its classes.\newline
$\forall ~x,y,p,c,m,d ~\In(x,c) \land A(x,m,y) \land
         P(p,c,m,d) \Rightarrow$\newline
$\exists ~o,n ~P(o,x,n,y) \land \In(o,p)$
%\bigskip
\item Axioms 10,11,12: The {\it isa\/} relation is a partial order
on the object identifiers.\newline
$\forall ~c ~\In(c,\hbox{\#Obj}) \Rightarrow \Isa(c,c)$\newline
$\forall ~c,d,e ~\Isa(c,d) \land \Isa(d,e) \Rightarrow
        \Isa(c,e)$\newline
$\forall ~c,d ~\Isa(c,d) \land \Isa(d,c) \Rightarrow
         (c=d)$
%\bigskip
\item Axiom 13: Class membership of objects is inherited upwardly to
the superclasses.\newline
$\forall ~p,x,c,d ~\In(x,d) \land P(p,d,isa,c) \Rightarrow \In(x,c)$
%\bigskip
\item Axiom 14: Attributes are "typed" by their attribute classes.\newline
$\forall ~o,x,n,y,p ~P(o,x,n,y) \land \In(o,p) \Rightarrow
\exists ~c,m,d ~P(p,c,m,d) \land \In(x,c) \land \In(y,d)$
%\bigskip
\item Axiom 15: Subclasses which define attributes with the same
name as attributes of their superclasses must refine these attributes.\newline
$\forall ~c,d,a_1,a_2,m,e,f$\newline
$\Isa(d,c) \land P(a_1,c,m,e) \land
          P(a_2,d,m,f) \Rightarrow \Isa(f,e) \land \Isa(a_2,a_1)$
%\bigskip
\item Axiom 16: If an attribute is a refinement (subclass) of another
attribute then it must also refine the source and destination
components. \newline
$\forall ~c,d,a_1,a_2,m_1,m_2,e,f$\newline
$Isa(a_2,a_1) \land P(a_1,c,m_1,e) \land
          P(a_2,d,m_2,f) \Rightarrow \Isa(d,c) \land \Isa(f,e)$
%\bigskip
\item Axiom 17: For any object there is always a unique "smallest"
attribute class with a given label {\it m}.\newline
$\forall ~x,m,y,c,d,a_1,a_2,e,f
         ~(\In(x,c) \land \In(x,d) \land
         P(a_1,c,m,e) \land P(a_2,d,m,f)$\newline
$\Rightarrow \exists ~g,a_3,h ~\In(x,g) \land P(a_3,g,m,h)
          \land \Isa(g,c) \land \Isa(g,d))$
%\bigskip
\item Axioms 18-22: Membership to the builtin classes is determined
by the object's format.\newline
$\forall ~o,x,n,y ~(P(o,x,n,y) \Leftrightarrow
                   \In(o,\hbox{\#Obj}))$\newline
$\forall ~o,n ~(P(o,o,n,o) \land (n \ne in) \land (n \ne isa) \Leftrightarrow
                   \In(o,\hbox{\#Indiv}))$\newline
$\forall ~o,x,c ~(P(o,x,in,c) \land (o \ne x) \land (o \ne c) \Leftrightarrow
                   \In(o,\hbox{\#Inst}))$\newline
$\forall ~o,c,d ~(P(o,c,isa,d) \land (o \ne c) \land (o \ne d) \Leftrightarrow
                   \In(o,\hbox{\#Spec}))$\newline
% \item Axiom 22:\newline
$\forall ~o,x,n,y ~(P(o,x,n,y) \land (o \ne x) \land (o \ne y) \land
                  (n \ne in) \land (n \ne isa)
\Leftrightarrow \In(o,\hbox{\#Attr}))$
%\bigskip
\item Axiom 23: Any object falls into one of the four builtin classes. \newline
$\forall ~o ~\In(o,\hbox{\#Obj}) \Rightarrow
                    \In(o,\hbox{\#Indiv}) \lor
                    \In(o,\hbox{\#Inst}) \lor
\In(o,\hbox{\#Spec}) \lor
                        \In(o,\hbox{\#Attr})$
%\bigskip
\item Axioms 24-28: There are five builtin classes. \newline
$P(\#Obj,\#Obj,\hbox{Proposition},\#Obj)$\newline
$P(\#Indiv,\#Indiv,\hbox{Individual},\#Indiv)$\newline
$P(\#Attr,\#Obj,\hbox{attribute},\#Obj)$\newline
$P(\#Inst,\#Obj,\hbox{InstanceOf},\#Obj)$\newline
$P(\#Spec,\#Obj,\hbox{IsA},\#Obj)$
%\bigskip
\item Axiom 29: Objects must be known before they are
referenced. The operator $\preceq$ is a (predefined) total
order on the set of identifiers.\newline
$\forall ~o,x,n,y ~P(o,x,n,y) \Rightarrow (x \preceq o) \land (y \preceq o)$
%\bigskip
\item Axioms 30,31 (axiom schemas): For any object $P(p,c,m,d)$ in the extensional
object base we have two formulas for "rewriting" the $\In$ and
$A$ predicates. The $\In$ is mapped to a unary predicate where the class name
is forming part of the predicate name and the $A$ predicates is mapped to a binary
predicate that carries the identifier of the class of the attribute in its predicate name.
Internally, user-defined deductive rules that derive $\In$ and $A$ predicates will
also be rewritten accordingly. This extends the choices for static stratification.
\newline
$\forall ~o ~\In(o,p) \Rightarrow \In.p(o)$\newline
$\forall ~o,x,n,y ~P(o,x,n,y) \land \In(o,p) \Rightarrow A.p(x,y)$

\end{itemize}

The following axioms are taken from papers on Telos (i.e.\ formulated by Mylopoulos,
Borgida, Kouba\-ra\-kis, Stanley and Greenspan): axioms 2, 3, 4, 10, 12, 13, 14.
Axiom 1 is probably also in an earlier Telos paper though we could not 
immediately find it there. The axioms 15 and 16 are similar to the structural
ISA constraint of Taxis \cite{Taxis80} for attributes. 
In O-Telos, we do however not inherit attributes downward to subclasses 
but rather constrain refined attributes at subclasses in the sense
of co-variance. 
Moreover, attributes in O-Telos are objects as well, hence the notion of specialization is
more complicated than for the Taxis case.
Axiom 17 is needed
to be able to uniquely match an attribution predicate to a most specific attribute.
This is utilized in the compilation of logical expressions, in particular for
generating triggers that only evaluate the affected logical expressions when an update
occurs. The remaining axioms 18-28 are also specific to O-Telos. They define the five
predefined objects in O-Telos. Axiom 29 takes care that objects cannot refer via
its source/destination parts to objects that were defined later than the object itself.
This virtually forbids to define an link between two objects when one of the objects is
not yet defined. While this sounds natural, we need to posutlate it. Otherwise, we can't
guarantee that we can refer to any object by a name.
Axioms 30 and 31 are used to transfer instantiation
and attribution facts from the extensional databases to the intensional database.
They have more a technical purpose in the mapping of logical expressions to
Datalog.

While O-Telos has just five predefined objects and 31 predefined axioms, the
ConceptBase system has many more pre-defined objects to provide a better
modeling experience and for representing concepts like query classes, active
rules, functions etc. They are in a way also predefined but are less essential
in understanding the foundations. So, O-Telos is the foundation of ConceptBase
but ConceptBase has more pre-defined constructs than those mentioned in
the axioms of O-Telos. 

% 2023-03-18
Axiom 15 is only applicable to attribute classes, i.e.\ where the attribute value is
an object that potentially can have instances. If the attribute value is for example
a number, then ConceptBase will not enforce the axiom. We leave the formula unchanged,
since classes for numbers such as {\tt Integer}, are not part of the axiomatization.


ConceptBase allows to add user-defined rules and constraints. The semantics
of an O-Telos database including such rules and constraints is 
the perfect model of the deductive database with the $P(o,x,n,y)$
as the only extensional predicate and all axioms and user-defined rules/constraints
as deductive rules. Note that integrity constraints can be rewritten to deductive rules
deriving the predicate {\it inconsistent}.

This list of axioms is excerpted from
M.A.~Jeusfeld: \"Anderungskontrolle in deduktiven Objektbanken.
Dissertation Universit\"at Passau, Germany, 1992. Available
as Volume DISKI-19 from INFIX-Verlag, St. Augustin, Germany or
via \url{http://merkur.informatik.rwth-aachen.de/pub/bscw.cgi/d340216/diski19.pdf} (in German).

Axioms 19-21 have been corrected after Christoph Radig found
an example that led to the undesired instantiation of an
individual object to \#Inst or \#Spec, respectively.




