Some Documentation about the Java Graphical Types
=================================================


An instance of JavaGraphicalType has the following attributes:

implementedBy: (necessary)
    specifies the class name of the class implementing this graphical type,
    e.g. "i5.cb.graph.cbeditor.CBIndividual"
    This class has to be a subclass of "i5.cb.graph.cbeditor.CBUserObject".
    If you are going to implement your own class, it most useful to extend
    CBIndividual or CBLink

priority:
    specifies the priority level of this graphical type. Objects may have
    multiple graphical types. If that is the case, the graphical type
    with the highest priority is used. Default is 0.   

properties:
    each graphical type can have a number of properties. These properties
    will be used by the implementing class. There are a number of 
    predefined properties that are used by the default classes
    CBIndividual and CBLink (see below). 



Semantic of Properties used in Java Graphical Types
---------------------------------------------------


bgcolor:
    Background color of the shape (default: transparent)

textcolor:
    Foreground color of the shape (i.e. text color)  (default: black "0,0,0")

linecolor:
    Color of the border of the shape (default: transparent)

linewidth:
    Width of the border of the shape (default: 1)
    
edgecolor:
    Color of the edge (default: black "0,0,0")

edgewidth:
    Width of the edge (default: 1)

edgestyle:
    possible values are: continuous, dashed, dotted   (default: continuous)
    
shape:
    The name of the class representing the shape, the class has
    to implement i5.cb.graph.shape.IGraphShape . (default: no shape)

label:
    The label to be used for this object instead of the object name

align:
    Alignment of the label (possible values are "center", "left", and "right"; default is "center")

size:
    Size of the node in pixels, e.g. "20x20"  (default: done automatically by Java LayoutManager)

font:
    Name of the font to be used for the shape (e.g., "Arial", default: Default font of Java)

fontsize:
    Size of the font in pixels (default: default font size of Java)

fontstyle:
    The style of the font (e.g., "bold", "italic", "underlined", "bold,italic", ... )

