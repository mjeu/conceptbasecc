.
.
. File:        CBBUGS
. Version:     1.5
. Creation:    5-Mar-1991, Manfred Jeusfeld (UPA)
. Last Change: 02-Jun-2004, Manfred Jeusfeld (UPA)
. Release:     1
. -----------------------------------------------------------------------------
.
. This file contains a list of known bugs of ConceptBase V3.2.
. New bugs at top.
.
.


20-Nov-2003/M.Jeusfeld: Incomplete integrity check when query classes are used
02-Jun-2004/M.Jeusfeld: now resolved! See CBNEWS.doc[213] and Ticket #16
..............................................................................

ConceptBase version: 6.1.1 (20-Nov-2003) and earlier

Symptom:
========

An object definition that apparently violates an integrity constraint
passes the IC test if part of the integrity test involves the call
of a query.

Reproduction of the error:

First tell:
===========

Unit in Class with
  attribute
    sub: Unit
end
SimpleUnit isA Unit end
BaseUnit in QueryClass isA Unit with
  constraint
    c1: $ not exists s/Unit!sub From(s,~this) $
end
SimpleUnit in Class with
  constraint
    c: $ forall s/SimpleUnit (s in BaseUnit) $
end
U1 in Unit with
  sub s1: U2; s2: BU3
end
U2 in Unit end
BU3 in SimpleUnit end


Second tell:
============

BU3 in SimpleUnit with
  sub s1: U2
end

Effect:
=======

The second TELL is accepted even though it violates the integrity constraint
of SimpleUnit.


Possible cause of the problem:
==============================

The integrity checker generates triggers for insertions and deletions of facts 
matching predicates that occur in the integrity constraint. It does NOT
generate triggers for predicates that occur in query classes.
The relevant trigger here is
  ON TELL In(x,SimpleUnit) CHECK In(x,BaseUnit)
However, the second TELL operation doesn't generate a fact 
In(BU3,SimpleUnit) because this fact was already inserted in the first
TELL operation. 
Consequently, the trigger isn't evaluated at all. 


Workaround:
===========

Avoid use of query classes in the definition of constraints (or rules).


Possible correction:
====================

a) Re-design the trigger generation in BDM*.pro (very expensive!).

b) Artificially extend the facts to be tested against the triggers (difficult to
   prove to be correct)

c) Forbid reference to query classes in any rule/constraint or at least
   issue a warning.

I have researched two methods for a possible correction. The first method
was to extend the definition of the trigger evaluation (BDMEvaluation.pro).
The idea was that a query class is a subclass to some ordinary class. Whenever
the ordinary class extension is changed, the system would look-up whether
some query class has a trigger and would evaluate the trigger with those
derived facts. This solution is WRONG because the extension of a query class does
not only depend on the extension of the super-classes BUT also on the 
constraint of the query. 

The second method is to include query classes in the trigger network, i.e.
generate simplified forms for the query rule (rangeform) that is created from
the query definition. This approach is principally correct. The problem is
that the existing trigger network is tiied to BDMRule and BDMconstraint.
The simplified forms are linked back to the original formula that must either
be a BDMRile or BDMConstraint. Hence, it is difficult to store the generated code
correctly without violating Telos axioms. Another difficulty is that queries
have two conclusion predicates In(x,q) and q(x). For both we have to generate
trigger (simplified forms). Nonetheless, we should follow this line when we want
to fully integrate queries into constraint evaluation.

Modules to look at:

   QueryCompiler.pro:
     contains procedure compileQuery which among others calls
     generateRangeform; here we should call the procedure tell_BDMRule to
     make sure that the query rule is treated like an ordinary deductive rule
     diificulty: at least two rules need to be considered, one with conclusion
     q(x,...), the other with conclusion In(x,q)
     Note that we miss a ruleID for the call of tell_BDMrule. 

   AssertionCompiler.pro
     shows how tell_BDMrule is called for ordinary deductive rules; see
     handleRangeform(rule,...)

   







1-Jul/1991/MJf: Data loss on long answers
.........................................

  If an answer from the CBserver exceeds the size of the IPC buffer
  (currently 32K) then data can be lost.


16-Jul/1991/MJf: Referencing formulas to the object base
...............................................

  Restrictions 2 in section 2 of the user manual demands that the
  literals of a formula (integrity constraint, deductive rule or query) must
  refer to a class (or attribute definition) in the OB. It may be necessary
  to split an object definition into two parts: the structural
  part and the part with the formulas. By this, you can achieve
  that all the information is in the OB that is needed to translate
  the formula. Example:

      Class Employee with
        attribute
          salary: Integer;
          dept: Department
      end Employee

     Class Department with
        attribute
          manager: Employee
     end Department
  
     Employee with
       constraint
         ic1: $ forall e/Employee exists d/Department
                  (e salary 10000) and
                  (e dept d) and
                   (d manager e) $
     end Employee


14-Jul-1993/MJf: Recursion within deductive rules
................................................

  Recursive rules should work now. There is a problem with recursive rules
  when they are defined before any integrity constraint using them. Then, an
  endless loop occurs. See also remark in the documentation of
  Examples/RULES+CONSTRAINTS.



