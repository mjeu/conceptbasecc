
\chapter{Server Interface}
\label{serverinterface}
\label{cap:Server-Interface}

This chapter provides basic information necessary for communication with the
ConceptBase server.
It is possible that the
CBserver and the clients 'live' on different machines because communication
with the CBserver is realized through a message protocol using
inter-process communication (IPC) based on standard Internet sockets.
It is even possible (but not recommended :-) ) to use a standard telnet program
for the communication with a CBserver.
The following chapter describes this
protocol as it is necessary to know for a specialized client which wants to
request services from the CBserver. Readers who intend only to use one of
the programming interfaces for C, C++ or Java may skip this chapter, but it contains
some useful basic information.

From a client's point of view the CBserver can be seen as an abstract data
type exporting several parameterized operations. These operations comprise
methods for storing/retrieving information into/from the KB, methods for
establishing and closing the connection to a CBserver and methods for
testing the KB. Since the client and CBserver are two different processes a client cannot
directly call these methods like procedures but must access them using a
message protocol. However, the use of one of the application programming interfaces (API)
for C, C++ and Java simplifies the communication and interaction with the CBserver
from the viewpoint of an application programmer.

This chapter is organized as follows:
Section \ref{sec:MessageFormat} describes the message protocol which is used to communicate
with other processes. Section \ref{sec:methods} describes the interface to the CBserver,
i.e. the data structures and operations which the ConceptBase kernel offers
and the message protocol which makes these operations accessible to other
processes.



%Achtung, hier fehlt: Fig. 4.1: Client-Server Architecture of ConceptBase



\section{Message Format}
\label{sec:MessageFormat}

As already mentioned, any client that wants to use the methods of a
CBserver has to communicate with CBserver according to a message protocol.
So called ipcmessages can be sent via IPC to the port reserved for this
CBserver. The CBserver handles such a message and reports back an answer:
the ipcanswer.

\subsection{ipcmessage}

{\bf ipcmessage ( sender, receiver, method, args ).} where

\begin{description}
\item[sender] is the identifier for the sender of the message,
\item[receiver] is the identifier for the receiver of the message
  (usually the CBserver itself, but could be any other client connected to CBserver
  as well),
\item[method] is one of the methods exported by the CBserver (or a method known
  to another client which is adressed by the message),
\item[args] are the arguments for method.
\end{description}


Note, that it is necessary to ``encode'' the parameters of an ipcmessage.
This means, that the strings must begin and end with \verb+"+. If the string
contains the characters \verb+"+ or \verb+\+, they must be escaped with a backslash (\verb+\+).
Please refer to the grammar definition in appendix \ref{cha:SyntaxSpec} for full details.

Messages can also be directed to other clients of the
CBserver by using a different ID than the server ID as a receiver of message.
If messages are sent from client to client, clients have to poll for messages
using the method NEXT\_MESSAGE. This function has not been tested recently.

A message can be prefixed by the length of the message, which is specified
in five bytes. The first byte is always the character 'X', the next bytes
are computed by the following formulas (len is the length of the message without
this prefix):
\begin{enumerate}
\item (len /$256^3$) modulo 256
\item (len /$256^2$) modulo 256
\item (len /$256$) modulo 256
\item len modulo 256
\end{enumerate}
e.g., the first byte is the highest byte and
the last byte is the lowest byte of an unsigned integer. Note, that
specifying the length of an IPC-message is optional. IPC-messages without
the length information should also be accepted by the server but communication problems
might occur in rare circumstances.

\subsection{ipcanswer}

{\bf ipcanswer ( sender, completion, return ).} where

\begin{description}
\item[sender] is the identifier of the answering program (usually the
  CBserver since other programs cannot answer directly but only
  receive the message and send back another message via the CBserver).
  This is sent as an encoded string.
\item[completion] signals success (=ok) or failure (=error)
  or unability (=not\_handled) of handling the message
\item[return] contains the return value(s) of the handled message. This
  is sent as an encoded string.
\end{description}

Additionally the CBserver administrates message queues for all
connected clients. Whenever a client X sends a message to another
client Y (which is not CBserver) the CBserver stores this message into
the message queue of the client Y and gives it back to X.

\noindent {\bf Remark:} If clients are likely to exchange messages they should periodically
poll their message queue.

\section{Methods Exported by the CBserver}
\label{sec:methods}


The CBserver offers the following methods (list is incomplete):

\begin{description}
\item[general methods:] {\tt TELL}, {\tt UNTELL}, {\tt TELL\_MODEL},
  {\tt ASK}, {\tt HYPO\_ASK}, {\tt NEXT\_MESSAGE}, {\tt ENROLL\_ME}, {\tt CANCEL\_ME},
  {\tt GET\_MODULE\_CONTEXT}
\item[privileged methods:]  {\tt STOP\_SERVER}, {\tt REPORT\_CLIENTS}
\item [internal methods:] {\tt LPI\_CALL}
\end{description}

Privileged methods affect other clients connected to CBserver as well and should
only be executed by an authorized client. That means, that only the {\em owner}
of the ConceptBase server process may execute this methods.

The internal method {\tt LPI\_CALL} gives a client the possibility to call internal
procedures of the ConceptBase server. This method is mainly useful for ConceptBase
developers for debugging and analysing.

In the following description of the methods {\tt return} refers to the respective
parameter of {\tt ipcanswer(sender, completion, return)}.

For each error occuring during the execution of a method an error message
is stored by the CBserver {\tt receiver} in the message queue of the client
{\tt sender}. This error message can be fetched via a call of method
{\tt NEXT\_MESSAGE}, see below.

\subsection{TELL}

\noindent {\bf ipcmessage ( sender, receiver, TELL,  [ objects ] )}

\begin{quote}
\begin{description}
\item[objects] encoded string containing object descriptions in Telos represented as frames
\item[return] \verb+"yes"+ in case of success, \verb+"no"+ otherwise
\end{description}

The CBserver receiver checks the syntax of {\tt objects} creating a parse
tree for each object description, called {\em SMLfragment}.
If no syntax error occurs the SMLfragments are transformed into an
internal network representation with specialized rules and
constraints. Those facts which are not already retrievable are
temporarily added to the KB. A check is then performed to determine
whether the updated KB still satisfies the integrity constraints. In
the case of satisfaction the new information is made permanent,
otherwise it is deleted.
\end{quote}

\subsection{UNTELL}

\noindent {\bf ipcmessage ( sender, receiver, UNTELL, [ objects ] )}

\begin{quote}
\begin{description}
\item[objects] encoded string containing object descriptions in Telos represented as frames
\item[return] \verb+"yes"+ in case of success, \verb+"no"+ otherwise
\end{description}

The {\tt objects} will be untold, i.e. the upper bound of their
transaction time interval is set to the time the UNTELL operation
takes place. That means from this time on the system does not believe
this information anymore. Questions about the current state of the
knowledge base yield the same answer as if the objects were never
inserted into the system. However questions about earlier states will
regard all information (even untold) the transaction time of which
contains the time in question (= rollback time). Like in the TELL
method, if the UNTELL operation would result in an inconsistent KB
state it is rejected by the integrity checker.
\end{quote}

\subsection{TELL\_MODEL}

\noindent {\bf ipcmessage ( sender, receiver, TELL\_MODEL, [ [ filelist ] ] ). }

\begin{quote}
\begin{description}
\item[filelist] A list of comma-separeted ipc strings, which contain the full filenames of
files to be loaded by ConceptBase server.
\end{description}

This method is similar to the {\tt TELL} method, except that the frames which are
told to ConceptBase are loaded from the given files and not passed directly to ConceptBase.

{\bf Remark:} The files to be loaded by ConceptBase must be accessible for the server.
This is not always the case, when server and client are running on different machines
with different filesystems mounted on. Another problem may occur, due to access protections,
because the user running the ConceptBase server is not allowed to read the specified files.
\end{quote}

\subsection{ASK}

\noindent {\bf ipcmessage ( sender, receiver, ASK, [Format, Query, AnswerRep, RollbackTime ]  )}

\begin{quote}
\begin{description}
\item[Format] is either \verb+FRAMES+ or \verb+OBJNAMES+, depending of the format of {\tt Query}.
If in {\tt Query} only the object name of a query is given (e.g.\ {\tt AllEmployees}) then
the format must be \verb+OBJNAMES+. If the query is specified as frame
(e.g.\ {\tt "QueryClass AllEmployees isA Employee end"}) then the format must be \verb+FRAMES+.

\item[Query] depending on the {\tt Format} this may be simple object names or frames
representing queries. In the later case, the query is temporarily told to the object base
and after evaluating deleted from the object base, if it does not already exist in the object
base before the transaction.

\item[AnswerRep] answer format specification, possible values are:
{\tt  FRAGMENT}, {\tt FRAME}, {\tt LABEL} or an instance of {\tt AnswerFormat}\footnote{See the {\em ConceptBase User Manual}
for details about user-defined answer formats}. The syntax of the {\tt FRAGMENT} and {\tt FRAME} formats are
explained in the appendix of the {\em ConceptBase User Manual}. If the answer representation is {\tt LABEL}
a comma-separated list of object names is returned.
\item[RollbackTime] rollback time specification
\item[return] list of answers in case of success, \verb+"no"+ otherwise
\end{description}

The values of the {\tt Format} argument (\verb+FRAMES+ and \verb+OBJNAMES+) are ipc message
keywords and must not be encoded as the other arguments {\tt Query}, {\tt AnswerRep} and {\tt RollbackTime}.
\end{quote}

\noindent {\bf Example:}

\begin{quote}
The following two queries are predefined builtin queries and
available after booting the {\it ConceptBase\/} server. These queries
additionally give good examples for derived expressions by
instantiating parameters of generic query classes.

\begin{itemize}
\item \verb+exists[x/objname]+

  The answer return is \verb+"yes"+ if there is an object named {\tt x},
  otherwise \verb+"no"+.

\item \verb+get_object[x/objname]+

  The answer is the frame representing the object {\tt x} if
  there is an object {\tt x}. Otherwise, the answer is \verb+"no"+. Only
  information that is explicitly stored (i.e. not inherited or
  deduced) is considered. If you want deduced information, you must specify
  additional parameters. For example, the answer of the following query
  is the {\tt Class} object with stored and deduced attributes:
  \begin{center}
  \verb+get_object[Class/objname,FALSE/dedIn,FALSE/dedIsa,TRUE/dedWith]+
  \end{center}

\end{itemize}
\end{quote}

\subsection{HYPO\_ASK}

\noindent {\bf ipcmessage(sender, receiver, HYPO\_ASK,[ ObjList, Format, Query, AnswerRep, RollbackTime])}

\begin{quote}
\begin{description}
\item[ObjList] string of objects in frame syntax
\item[Format] see ASK
\item[Query] see ASK
\item[AnswerRep] see ASK
\item[RollbackTime] see ASK
\item[return] list of answers in case of success, no otherwise
\end{description}


This method allows to process so called 'hypothetical' queries
against the KB. The objects in objList are temporarily told.
This list may contain query objects which may in turn be
referred to by names contained in Query.  Then the queries in
queryList are evaluated as if the temporary information would
belong to the KB. Afterwards the temporary information will be
removed.
\end{quote}

\subsection{NEXT\_MESSAGE}

\noindent {\bf ipcmessage ( sender, receiver, NEXT\_MESSAGE,  [type] )}

\begin{quote}
\begin{description}
\item[type] identifier describing the type of the message (e.g.\ \verb+ERROR_REPORT+).
This argument may not be encoded as other string, but may be {\em empty}.
\item[return] contains the next message for the client if its message
  queue contains at least one message, {\tt empty\_queue} if no message
  exists
\end{description}

Client sender requests a message from the CBserver receiver stored in
its message queue. Usually, this method is called after the CBserver returns
error for a previous method. The client program must then get all error messages
until it gets \verb+"empty_queue"+ as answer.

\end{quote}


\subsection{STOP\_SERVER}

\noindent {\bf ipcmessage ( sender, receiver, STOP\_SERVER,  [password] )}

\begin{quote}

\begin{description}
\item[password] password allowing a client to stop a CBserver (may be {\em empty})
\item[return] \verb+"yes"+ in case of success, \verb+"no"+ otherwise
\end{description}

The CBserver receiver is terminated if the
password is correct and the user running the client is also the owner
of the CBserver to be stopped. To the requesting client {\tt STOP\_SERVER}
has the same effect as {\tt CANCEL\_ME}. It is recommended to terminate the
CBserver by using the respective menu choice from the ``Server Menu'' of
ConceptBase Workbench if you want to stop the CBserver process.

\end{quote}



\subsection{REPORT\_CLIENTS}

\noindent {\bf ipcmessage ( sender, receiver, REPORT\_CLIENTS, [ ] )}

\begin{quote}
\begin{description}
\item[return] list of all clients currently connected to CBserver receiver
\end{description}

CBserver receiver reports back the identifier, toolclass and owner
name of all currently connected clients including itself.
\end{quote}


\subsection{ENROLL\_ME}

\noindent {\bf ipcmessage ( sender, receiver, ENROLL\_ME, [toolclass,username] )}

\begin{quote}
\begin{description}
\item[toolclass] 'class' the client belongs to
\item[username] name of the user running the client
\item[return] identifier assigned to the client by CBserver
\end{description}

{\tt sender} and {\tt receiver} have value \verb+""+ since they are not known. The
sending client will be registered as a new client of the CBserver with
its own identifier and message queue. This message must be sent to the
CBserver before any other message can be sent, since all other
messages require valid identifiers to be assigned to sender and
receiver.
If the user specified by {\tt username} is stored as an instance of
the class {\tt CB\_User} of the CBserver, then the value of the attribute
{\tt homeModule} of that user is taken as the initial module context
of the client. Otherwise, the default module context {\tt System} is
assigned to the client.
In a variant of ENROLL\_ME, one can specify a third parameter {\tt module}
which will set the module context explicitely.

\end{quote}


\subsection{CANCEL\_ME}

\noindent {\bf ipcmessage ( sender, receiver, CANCEL\_ME, [ ] )}

\begin{quote}
\begin{description}
\item[return] \verb+"yes"+ in case of successful disconnection, \verb+"no"+ otherwise
\end{description}

Client sender will be disconnected from CBserver. This means
that from now on the sender is no longer known to the CBserver (no
further messages can be sent) and its message queue is deleted.
After successfully canceling the connection, the ipc sockets
to the server must be closed by the client program.
\end{quote}


\subsection{GET\_MODULE\_CONTEXT}

\noindent {\bf ipcmessage ( sender, receiver, GET\_MODULE\_CONTEXT,  []  )}

\begin{quote}
\begin{description}
\item[return] name of the module currently assigned to client {\tt sender}
\end{description}

This service allows clients to interrogate the CBserver about the currently active
module context in which they operate.
\end{quote}



\subsection{LPI\_CALL}

\noindent {\bf ipcmessage ( sender, receiver, LPI\_CALL,  [call]  )}

\begin{quote}
\begin{description}
\item[call] an internal routine
\item[return] \verb+"yes"+ if call succeeded, \verb+"no"+ otherwise
\end{description}

This is for debugging and testing purposes only.
\end{quote}

