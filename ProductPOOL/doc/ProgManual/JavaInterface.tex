
\chapter{Programming Interface for a Java Client}

The Java Application Programming Interface (Java API) consists
of a package for the communication with the ConceptBase server,
and the Telos Parser which uses the Java Generic Library 3.1.0
of ObjectSpace Inc (\url{http://www.objectspace.com}). The Telos Parser was generated with the tool
JavaCC of SunTest. All classes relevant to ConceptBase are
in the packages under i5.cb.

This chapter gives only an overview on how to use the Java API
of ConceptBase. Detailed documentation of the classes and
their methods can be found in the API documentation generated
by javadoc. This should be included in the package with
programmers information, otherwise contact the ConceptBase
Team (cb@i5.informatik.rwth-aachen.de).

The Java API for ConceptBase consists of three main packages:
\begin{description}
\item[i5.cb.api] contains classes that handle the communication
with a ConceptBase, e.g. create a connection, send messages,
retrieve answers,
\item[i5.cb.telos.frame] contains a Telos parser to parse
Telos frames and classes to represent the structure of
Telos frames in Java, and
\item[i5.cb.telos.object] provides a one-to-one representation
of the Telos objects of the ConceptBase server in a Java client.
Methods provide facilities to retrieve all instances, subclasses,
attributes, etc. of an object.
\end{description}

The preferred method for the interaction with ConceptBase is the
usage of the package i5.cb.telos.object. The classes of the
other packages can be used, too, but then more programming
in your client application is required.

Some examples for the communication with ConceptBase
can be found in the directory \\\verb+$CB_HOME/examples/Clients/JavaClient+.

\section{Communication with ConceptBase: i5.cb.api}

The main class of the package {\ttfamily i5.cb.api} is the class
{\ttfamily CBclient}. The connection with a ConceptBase server can
be established during the construction of an object of this
class or with the method {\ttfamily enrollMe}.

This class has methods like {\ttfamily tell}, {\ttfamily untell}, {\ttfamily ask} etc.
to perform the usual operations on the ConceptBase server.
They return in most cases an object of the class CBanswer,
which represents the answer delivered by the ConceptBase
server. The methods and the structures are similar to
the methods and structures defined in the C and C++
API.

Furthermore, several get...-methods allow to retrieve
status information of the client object and some
set-methods change some parameters of the client,
e.g. the timeout value or the current module.

The class CButil contains some static methods for
decoding and encoding of strings, so that they
are accepted by ConceptBase.

The class CBterm is used only internally, it parses
Prolog-like terms.

Nearly every method throws an exception if some
unexpected error has occured during the operation.
All exceptions are derived from the class
i5.cb.CBException. The exceptions of the
class CBIOException are thrown if, for example,
the communication between client and
server is broken or a timeout has occured.
CBUtilExceptions are thrown if a string
cannot be decoded or encoded.


\section{Parsing Telos Frames: i5.cb.telos.frame}

The package i5.cb.telos.frame provides all classes and
methods that are necessary to parse (and unparse)
Telos frames or list of Telos object names, and to
represent frames and objects as Java objects.

The parsing of Telos frames or a list of object names
requires two steps. First you have to construct a
TelosParser object:

\begin{verbatim}
TelosParser tpParser=new
   TelosParser(new StringBufferInputStream(sFrame));
\end{verbatim}

The constructor of TelosParser requires an InputStream
object as parameter, therefore it is necessary to construct
a StringBufferInputStream out of a String object.

In the second step, you have to call a method of TelosParser
to start the parsing. Possible methods are:

\begin{description}
\item[telosFrames] to parse a set of Telos frames,
\item[telosFrame] to parse one Telos frame, and
\item[objectNames] to parse a list of Telos object names.
\end{description}

The methods return TelosFrame(s) or ObjectName objects,
that can be accessed with several methods. For details,
see the API documentation or the examples provided
in \verb+$CB_HOME/examples/Clients/JavaClient+.

It is also possible to construct a TelosFrame object
step by step, without parsing a string. This is shown
in the method {\ttfamily test2} of ExampleParser.java.
The TelosFrame class has a method
toString which converts the TelosFrame into a string, which
can be given as an argument to the tell method of CBclient.


\section{ObjectBaseInterface: i5.cb.telos.object}

This package provides methods and classes to represent
Telos objects and sets of them as Java objects.
There are two possible ways of using this package:

\begin{itemize}
\item {\em without a connection to a ConceptBase server:}
Telos objects are created directly in the Java program by using
the static methods {\ttfamily getIndividual}, {\ttfamily getSpecialization}, {\ttfamily getInstantiation}
and {\ttfamily getAttribute}. Objects may be added to
{\ttfamily ITelosObjectSets} that have been created by the
{\ttfamily TelosObjectSetFactory}. Within an {\ttfamily ITelosObjectSet},
one can search for instances, subclasses, attributes, etc.
of a Telos object.
\item {\em with a connection to a ConceptBase server:}
An instance of the class {\ttfamily ObjectBaseInterface} has to be created
(using a CBclient object). Then, this object can be used
to retrieve an object from the ConceptBase server ({\ttfamily getIndividual}),
to list all instances of an object ({\ttfamily getAllInstancesOf}),
to retrieve all attributes of an object ({\ttfamily getAttributesOf}), etc.
The class {\ttfamily ObjectBaseInterface} is an implementation of {\ttfamily ITelosObjectSet}.
Also, insertion and deletion of objects in the CBserver via the methods {\ttfamily add} and {\ttfamily remove}
is possible. However, it is usually easier to construct a Telos frame
and use the method {\ttfamily tell} of {\ttfamily CBclient} than
constructing a set of Telos objects.
\end{itemize}

Note that the relationships of a Telos object to other Telos objects
are specific to a Telos object set, e.g. X might be an instance of Y
in one set and not in another set. Therefore, methods such as {\ttfamily getAllInstancesOf}
are methods of the ITelosObjectSet and not of TelosObject.

The file ExampleOBI.java in \verb+$CB_HOME/examples/Clients/JavaClient+
contains uses the {\ttfamily ObjectBaseInterface} to test various operations.

