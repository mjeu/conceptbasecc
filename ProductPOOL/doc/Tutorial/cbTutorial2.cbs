#
# File: cbTutorial2.cbs
# Author: Manfred Jeusfeld
# Creation: 20-May-2009 (20-May-2009)
# -----------------------------------
# Solution to the 15 exercises of the $CB_HOME/doc/Tutorial/cbTutorial2.pdf
# You are advised to perform the exercises via the graphical
# user interface CBIva. This script is just for validating that the
# master solution actually works.
#
# (c) 2009 by M. Jeusfeld. 
# This script is licensed under the terms of Attribution-Non-Commercial 2.0 Germany 
#   http://creativecommons.org/licenses/by-nc/2.0/de/legalcode  (German)
#   http://creativecommons.org/licenses/by-nc/2.0/legalcode     (generic)
# A summary of your rights and obligations concerning this work is available from
#   http://creativecommons.org/licenses/by-nc/2.0/de/deed.en_GB
# Extended rights can be obtained via the author.
#
# Requires ConceptBase 7.1.x released January-2009 or later
#
# Execute this script in a command/shell window by:
#   $CB_HOME/bin/CBshell -f cbTutorial2.cbs

startServer -u nonpersistent 

# Exercise 1

tell "
EntityType end

RelationshipType with
  attribute
     role: EntityType
end
"


# Exercise 2

tell "
EntityType with
  attribute
    attr: Domain
end

Domain end
"


# Exercise 3

tell "
Integer in Domain end
String in Domain end
"

# Exercise 4

tell "
Date in Domain end
\"2009-05-19\" in Date end
\"2001-01-01\" in Date end
"


# Exercise 5

tell "
Customer in EntityType with
  attr
    name: String;
    address: String
end

Policy in EntityType with
  attr
   startdate: Date;
   enddate: Date;
   premium: Integer
end

holds in RelationshipType with
  role
    customer: Customer;
    policy: Policy
end

Claim in EntityType with
  attr
    description: String
end

claim_policy in RelationshipType with
  role
    claim: Claim;
    policy: Policy
end
"

# Exercise 6

tell "
mary in Customer end
policy1 in Policy with
  startdate d: \"2009-05-19\"
  premium p: 1000
end

holds1 in holds with
  customer c: mary
  policy p: policy1
end
"

# Exercise 7
# includes the definition of transitivity

tell "
Proposition in Class with
  attribute
    transitive: Proposition
  rule
    trans_R: $ forall x,y,z,R/VAR
                      AC/Proposition!transitive C/Proposition
                     P(AC,C,R,C) and (x in C) and (y in C) and (z in C) and
                     A_e(x,R,y) and (y R z) ==> (x R z) $
end

Task with
  attribute,transitive
     successor: Task
end

Agent with
   attribute
     executes: Task
end 
"
# You can ingnore parse error messages about the variable x; the display of the error
# message is actually a small bug

# Exercise 8

tell "
StartStatement in QueryClass isA Task with
  constraint
    c1: $ not exists t/Task (t successor this) $
end

PredicateTask in QueryClass isA Task with
  constraint
    c1: $ exists s1,s2/Task A_e(this,successor,s1) and
          A_e(this,successor,s2) and (s1 \= s2) $
end
"

# Exercise 9

tell "
LoopTaskOf in GenericQueryClass isA Task with
  parameter
    rep: Task 
  constraint
    c: $ (this successor rep) and (rep successor this) and
         (exists s/Task A_e(rep,successor,s) and (s successor rep)) $
end

LoopTask in QueryClass isA LoopTaskOf 
end
"

# The parameter {\tt rep} in the first query stands a representative of a loop. Note that
# there may be many loops inside a process model and we would like to be able to query,
# which tasks belong to the same loop. The second query just returns all loop statements regardless
# of the representative. It is sufficient to leave out a value for parameter {\tt rep} in this case.

# Exercise 10

tell "
AgentWithSplitResponsibility in QueryClass isA Agent with
   constraint
     c1: $ exists t1,t2,t/Task a/Agent (this executes t1) and
              (this executes t2) and (t1 successor t) and
              (t successor t2) and (a executes t) and (a \= this)$
end
"

# Exercise 11

tell "
start in Task with
  successor
     n: receiveClaim
end

receiveClaim in Task with
   successor
     n: checkPolicy
end

checkPolicy in Task with
    successor
      n1: assignAssessor;
      n2: proposePayment
end

assignAssessor in Task with
     successor
       n: assessDamage
end

assessDamage in Task with
   successor
     n: proposePayment
end

proposePayment in Task with
    successor
      accept: processPayment;
      reject: checkPolicy
end

processPayment in Task with
   successor
     n: finish
end

finish in Task end


Assessor in Agent with
  executes
    t1: assessDamage
end

InsuranceAgent in Agent with
   executes
     t1: receiveClaim;
     t2: proposePayment
end
"

#  Exercise 12
ask "LoopTask" OBJNAMES LABEL Now
# expected answer: checkPolicy, assignAssessor, assessDamage, proposePayment
ask "AgentWithSplitResponsibility" OBJNAMES LABEL Now
# expected answer: InsuranceAgent, Assessor

# Exercise 13

tell "
ObjectType end
EntityType isA ObjectType end
RelationshipType isA ObjectType end
"

# Exercise 14

tell "
Task with
 attribute
    input: ObjectType;
    output: ObjectType
end
"

# Exercise 15

tell "
receiveClaim with
  output o1: Claim
end

checkPolicy with
  input i1: claim_policy
end
"



