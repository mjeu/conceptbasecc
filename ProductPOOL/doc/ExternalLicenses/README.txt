Licenses of third party components that might be included
with the ConceptBase+ distribution:


(1) The SWI-Prolog runtime environment is copyrighted by SWI, www.swi-prolog.org.
License copy:  Swi-Prolog-License-14Jan2008.txt

(2) The Grappa class files are copyrighted by AT&T, 
licensed under www.graphviz.org/License.php.
License copy:  Grappa-License-14Jan2008.txt

(3) ConceptBase logos copyrighted by Manfred Jeusfeld
licensed under https://creativecommons.org/licenses/by-nd/4.0/legalcode#languages.
License copy: CBLogo-CC-BY-ND-40.txt

(4) ConceptBase.cc source code copyrighted by ConceptBase Team
licensed under a FreeBSD-style license
License copy: CB-FreeBSD-License.txt

(5) FlatLaf class files copyrighted by Karl Tauber, FormDev Software GmbH
Licensed under a Apache 2.0 License
License copy: FlatLaf-LICENSE-9Mar2024.txt
Developer site: https://www.formdev.com/flatlaf/


Distributed under the terms of their respective licenses 
available from the web sites above.


All trademarks are property of their respective owners!

--
2024-03-09

