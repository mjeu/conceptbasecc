#
#
# File:        $Source: /home/cbase/CVS/ProductPOOL/rules.mk,v $
# Version:     $Revision: 14.32 $
# Creation:    16-10-95, Christoph Quix (RWTH)
# Last Change: $Date: 2018/10/04 10:47:28 $ , Christoph Quix (RWTH)
# Release:     %R%
#
# -----------------------------------------
#
# Globales Makefile fuer SourcePool von ConceptBase
# enthaelt alle Regeln zur Uebersetzung von Files zu ConceptBase
#
# config.mk muss vorher includiert werden
#


# Do not include RULES_MK twice
ifndef RULES_MK_INC
RULES_MK_INC=1

#-----------------------------------------------
# vpath Anweisung die nicht mehr in config.mk stehen koennen
#-----------------------------------------------
vpath $(OBJDIR)/%.$(OBJSUFFIX) .
vpath $(OBJDIR)/%.wic .
vpath $(OBJDIR)/%.$(LIBSUFFIX) .
vpath %.class $(POOL_ROOT)/$(JAVA_CUR_CLASSPATH)

#-----------------------------------------------
# C files
#-----------------------------------------------
$(OBJDIR)/%.$(OBJSUFFIX): %.c
	$(CC) $(CFLAGS) -c $< -o $@

$(CB_POOL)/$(CurrDir)$(OBJDIR)/%.$(OBJSUFFIX): %.c
	$(CC) $(CFLAGS) -c $< -o $(OBJDIR)/$(notdir $@)

#-----------------------------------------------
# Doc++
#-----------------------------------------------
docpp: $(DOCPPFILES)
	-rm -rf $(DOCPPDIR)
	$(DOCPP) $(DOCPPFLAGS) $?

docpptex: $(DOCPPFILES)
	-rm $(DOCPPTEXFILE)
	$(DOCPP) $(DOCPPTEXFLAGS) -o $(DOCPPTEXFILE) $?
#	perl -e 'open(FILE, "docpp.tex");' \
#	    -e '$$zeile=<FILE>;' \
#	    -e 'print "$$zeile";' \
#	    -e 'read FILE,$$zeile,160000;' \
#	    -e '$$zeile =~ s/([^\\])_/$$1\\_/g;' \
#	    -e 'print "$$zeile";' > docpp.new
#	mv docpp.new docpp.tex

#-----------------------------------------------
# C++ files
#-----------------------------------------------
$(OBJDIR)/%.$(OBJSUFFIX): %.cc
	$(CXX) $(CXXFLAGS) -c $< -o $@

$(CB_POOL)/$(CurrDir)$(OBJDIR)/%.$(OBJSUFFIX): %.cc
	$(CXX) $(CXXFLAGS) -c $< -o $(OBJDIR)/$(notdir $@)

#-----------------------------------------------
# Libraries
#-----------------------------------------------
$(OBJDIR)/%.$(LIBSUFFIX): $(OBJDIR) $(C_OBJECTS) $(CXX_OBJECTS)
	$(AR) $(AR_FLAGS) $(AR_OUTFLAG)$@ $(foreach ofile,$(notdir $(filter-out $(OBJDIR), $^)),\
		$(word 1,$(wildcard $(OBJDIR)/$(ofile) $(CB_POOL)/$(CurrDir)$(OBJDIR)/$(ofile)) $(OBJDIR)/$(ofile)))

$(CB_POOL)/$(CurrDir)$(OBJDIR)/%.$(LIBSUFFIX): $(OBJDIR) $(C_OBJECTS) $(CXX_OBJECTS)
	$(AR) $(AR_FLAGS) $(AR_OUTFLAG)$(OBJDIR)/$(notdir $@) $(foreach ofile,$(notdir $(filter-out $(OBJDIR), $^)),\
		$(word 1,$(wildcard $(OBJDIR)/$(ofile) $(CB_POOL)/$(CurrDir)$(OBJDIR)/$(ofile)) $(OBJDIR)/$(ofile)))

$(OBJDIR)/%.$(LIBDLLSUFFIX): $(OBJDIR) $(C_OBJECTS) $(CXX_OBJECTS)
	$(ARDLL) $(ARDLL_FLAGS) $(ARDLL_OUTFLAG)$@ $(foreach ofile,$(notdir $(filter-out $(OBJDIR), $^)),\
		$(word 1,$(wildcard $(OBJDIR)/$(ofile) $(CB_POOL)/$(CurrDir)$(OBJDIR)/$(ofile)) $(OBJDIR)/$(ofile)))

$(CB_POOL)/$(CurrDir)$(OBJDIR)/%.$(LIBDLLSUFFIX): $(OBJDIR) $(C_OBJECTS) $(CXX_OBJECTS)
	$(ARDLL) $(ARDLL_FLAGS) $(ARDLL_OUTFLAG)$(OBJDIR)/$(notdir $@) $(foreach ofile,$(notdir $(filter-out $(OBJDIR), $^)),\
		$(word 1,$(wildcard $(OBJDIR)/$(ofile) $(CB_POOL)/$(CurrDir)$(OBJDIR)/$(ofile)) $(OBJDIR)/$(ofile)))

#-----------------------------------------------
# Lex/Flex files
#-----------------------------------------------
%.yy.c: %.l
	$(LEX) -t $(LFLAGS) $< > $@


#-----------------------------------------------
# Yacc/Bison files
#-----------------------------------------------
%.tab.c: %.y
	$(YACC) $(YFLAGS) $< -o $@

#-----------------------------------------------
# Dependencies erstellen
#-----------------------------------------------
ifeq "$(words $(C_SOURCES) $(CXX_SOURCES))" "0"
depend:
else
depend: $(C_SOURCES) $(CXX_SOURCES)
	$(CB_ADMIN)/utils/CB_Makedepend $(CFLAGS) $^
endif

#-----------------------------------------------
# Prolog files
#-----------------------------------------------
# Zunaechst die Prolog-Files, die aus DCGs gebaut wurden
# Diese befinden sich immer im variantenunabhaengigen Verzeichnis
# oder src-Verzeichnis dieses Verzeichnisses


# Normale Prolog-Dateien
# BIM/Master Prolog
$(OBJDIR)/%.wic: %.bim.pro
	$(PROLOG_COMP) $(PROLOG_OFLAG)$@ $(PROLOG_COMP_FLAGS) $<

$(CB_POOL)/$(CurrDir)$(OBJDIR)/%.wic: %.bim.pro
	$(PROLOG_COMP) $(PROLOG_OFLAG)$(OBJDIR)/$(notdir $@) $(PROLOG_COMP_FLAGS) $<


$(PROLOG_BIM_SOURCES): $(PROLOG_SOURCES)
	$(CB_PPP) BIM $?

# SWI Prolog
$(OBJDIR)/%.swo: %.swi.pl
	$(PROLOG_COMP) $(PROLOG_OFLAG)$@ --goal=list_undefined --stand_alone=false --autoload=false $(PROLOG_COMP_FLAGS) $<
	$@ < /dev/null

$(CB_POOL)/$(CurrDir)$(OBJDIR)/%.swo: %.swi.pl
	$(PROLOG_COMP) $(PROLOG_OFLAG)$(OBJDIR)/$(notdir $@) $(PROLOG_COMP_FLAGS) $<

#%.swi.pl: %.pro
#	$(CB_PPP) SWI $< $@
makePrologVariantSource: $(PROLOG_SOURCES)
	$(CB_PPP) $(PROLOG_VARIANT) $?

#-----------------------------------------------
# DCG files
# Die komplexeste Regel in diesem Makefile:
# Wir befinden uns im Variantenverzeichnis, die Sourcen
# sind im entsprechenden variantenunabhaengigen Verzeichnis.
# Zuerst wird ein Link auf das DCG-File angelegt, dann wird
# die Eingabe fuer den Prolog-Interpreter erzeugt und bei
# Bedarf dcg.pro compiliert. Nach Aufruf des Interpreters
# wird der Link und ein temp. File geloescht und die Ausgabe
# von dcg.pro in das var.-unabhaengige Verzeichnis geschoben.
#-----------------------------------------------
ifeq "$(PROLOG_VARIANT)" "BIM"
%_dcg.pro: %.dcg
	$(SYMLINK) $(word 1,$^) $(notdir $(word 1,$^))
	echo dcg. > $(TMPFILE)
	echo $(notdir $(word 1,$^)) >> $(TMPFILE)
	echo n >> $(TMPFILE)
	$(PROLOG_INTERPRETER) -Pq+ $(POOL_ROOT)/$(CurrDir)/$(OBJDIR)/dcg.wic < $(TMPFILE)
	$(RM) $(TMPFILE) $(notdir $(word 1,$^))
	$(MV) $(notdir $@) $(POOL_ROOT)/$(NotVariantDir)
endif

ifeq "$(PROLOG_VARIANT)" "SWI"
%_dcg.pro: %.dcg
	$(CP) $(word 1,$^) $(notdir $(word 1,$^))
	echo dcg. > $(TMPFILE)
	echo $(notdir $(word 1,$^)) >> $(TMPFILE)
	echo halt. >> $(TMPFILE)
	$(PROLOG_INTERPRETER) -f $(POOL_ROOT)/$(NotVariantDir)/dcg.pl < $(TMPFILE)
	$(RM) $(TMPFILE) $(notdir $(word 1,$^))
	$(MV) $(notdir $@) $(POOL_ROOT)/$(NotVariantDir)
endif



#-----------------------------------------------
# TeX files
#-----------------------------------------------
ifneq "$(findstring /$(CB_VARIANT)/,$(TEX_CAPABLE))" ""
$(OBJDIR)/%.dvi: %.tex
	cd $(OBJDIR) ; $(TEX) $(TEX_FLAGS) ../$< $(notdir $(basename $<))

$(OBJDIR)/%.pdf: %.tex
	cd $(OBJDIR) ; $(PDFTEX) $(TEX_FLAGS) ../$< $(notdir $(basename $<))
else
$(OBJDIR)/%.dvi: %.tex
	@echo "Your platform is not TEX_CAPABLE, skipping $<"
$(OBJDIR)/%.pdf: %.tex
	@echo "Your platform is not TEX_CAPABLE, skipping $<"
endif

#-----------------------------------------------
# latex2html (nur wenn CB_Make html aufgerufen wird)
#-----------------------------------------------
$(HTML_DIR)/%.html: %.tex
	$(CP) $< $(OBJDIR) ;\
	cd $(OBJDIR) ;\
	$(LATEX2HTML) -no_images -dir `pwd`/html $(notdir $<) ;\
	$(LATEX2HTML) -images_only -dir `pwd`/html $(notdir $<) ;\
	$(RM) $(notdir $<)

html: $(HTML_OBJECTS)

clean_html:
	/bin/rm -rf $(HTML_DIR) $(EXPDIR)/html


#-----------------------------------------------
# DVI files
#-----------------------------------------------
ifneq "$(findstring /$(CB_VARIANT)/,$(TEX_CAPABLE))" ""
$(OBJDIR)/%.ps: $(OBJDIR)/%.dvi
	$(DVIPS) $(DVIPS_FLAGS) $< -o $@
else
$(OBJDIR)/%.ps: $(OBJDIR)/%.dvi
	@echo "Your platform is not TEX_CAPABLE, skipping $<"
endif

#-----------------------------------------------
# Java
#-----------------------------------------------

$(POOL_ROOT)/$(JAVA_CUR_CLASSPATH)/%.class: %.java
	$(JAVAC) $(JAVA_FLAGS) $<

$(CB_POOL)/$(JAVA_CUR_CLASSPATH)/%.class: %.java
	$(JAVAC) $(JAVA_FLAGS) $<


# JBuildTree zum Compilieren benutzen
#compilejava: $(POOL_ROOT)/$(JAVA_CLASSPATH)
#	$(JAVA) szeiger.jbuildtree.JBuildTree -d $(POOL_ROOT)/$(JAVA_CLASSPATH) $(POOL_ROOT)/$(JAVA_CUR_CLASSPATH)/ $(JAVA_SOURCES)

# compile java and docs only if CB_VARIANT is capable of Java; see also config.mk
ifneq "$(findstring /$(CB_VARIANT)/,$(JAVA_CAPABLE))" ""
compilejava: $(POOL_ROOT)/$(JAVA_CLASSPATH) compilejava2

compilejava2: $(JAVA_SOURCES)
	$(JAVAC) -classpath $(CLASSPATH) $(JAVA_FLAGS) $?

ifneq "$(findstring /$(CB_VARIANT)/,$(JAVA_DOC_CAPABLE))" ""
javadoc:
	@echo Dokumentation wird im Verzeichnis $(POOL_ROOT)/java erstellt
	-cd $(POOL_ROOT)/java ; $(RM) -r docs
	-cd $(POOL_ROOT)/java ; $(MKDIR) docs
	-cd $(POOL_ROOT)/java ; $(JAVADOC) -d docs $(JAVA_PACKAGES)
else
javadoc:
endif
# inner if for JAVA_DOC_CAPABLE


# Javac zum Compilieren benutzen
# compilejava: $(POOL_ROOT)/$(JAVA_CLASSPATH) $(notdir $(JAVA_OBJECTS))


#GIF-Files nach java/classes/... kopieren, so dass sie ins JAR-File kommen
cpgifs: $(GIFS)
	$(TEST_DIR) $(POOL_ROOT)/$(JAVA_CUR_CLASSPATH) || $(MKDIR) $(POOL_ROOT)/$(JAVA_CUR_CLASSPATH)
	$(CP) $? $(POOL_ROOT)/$(JAVA_CUR_CLASSPATH)
else
compilejava:

javadoc:

cpgifs:

endif   
#outer if for JAVA_CAPABLE

$(POOL_ROOT)/$(JAVA_CLASSPATH):
	$(MKDIR) $@

runjava:
	$(JAVA) -ea $(JAVA_MAIN) $(JAVA_OPTIONS)





#-----------------------------------------------
# Export
#-----------------------------------------------
ifeq "$(words $(EXPORTFILES) $(HTML_EXPORTFILES))" "0"
export: $(EXPORT_DIRS)
else
ifeq "$(words $(HTML_EXPORTFILES))" "0"
export: $(EXPORT_DIRS) $(EXPDIR) $(EXPORTFILES)
	$(CP) -r $(filter-out EXPORT_%,$(filter-out $(EXPDIR),$^)) $(EXPDIR)
else
export: $(EXPORT_DIRS) $(EXPDIR) $(EXPORTFILES)
	$(CP) -r $(filter-out EXPORT_%,$(filter-out $(EXPDIR),$^)) $(EXPDIR)
	-$(MKDIR) $(EXPDIR)/html
	$(CP) $(HTML_EXPORTFILES) $(EXPDIR)/html
endif
endif

$(EXPDIR):
	$(MKDIR) $@

#-----------------------------------------------
# Directories/Komponenten/Module
#-----------------------------------------------
$(SubDirs): FORCE
	$(TEST_DIR) $(notdir $@) || $(MKDIR) $(notdir $@)
	$(TEST_DIR) $(notdir $@)/$(SRCDIR) || $(SYMLINK) ../$(POOL_ROOT)/$(SRCDIR)/$(CurrDir)$(notdir $@) $(notdir $@)/$(SRCDIR)
	$(TEST_FILE) $(notdir $@)/Makefile || $(SYMLINK) $(SRCDIR)/Makefile $(notdir $@)/Makefile
ifndef MAKEVARIANT
	$(TEST_NOTDIR) $(GLOBAL_POOL)/$(SRCDIR)/i86pc/$(CurrDir)$(notdir $@) || \
	($(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT) i86pc MAKEVARIANT=1 ; \
	$(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT)/i86pc/$(CurrDir) $(notdir $@) MAKEVARIANT=1)
	$(TEST_NOTDIR) $(GLOBAL_POOL)/$(SRCDIR)/sun4/$(CurrDir)$(notdir $@) || \
	($(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT) sun4 MAKEVARIANT=1; \
	$(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT)/sun4/$(CurrDir) $(notdir $@) MAKEVARIANT=1)
	$(TEST_NOTDIR) $(GLOBAL_POOL)/$(SRCDIR)/linux/$(CurrDir)$(notdir $@) || \
	($(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT) linux MAKEVARIANT=1; \
	$(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT)/linux/$(CurrDir) $(notdir $@) MAKEVARIANT=1)
	$(TEST_NOTDIR) $(GLOBAL_POOL)/$(SRCDIR)/windows/$(CurrDir)$(notdir $@) || \
	($(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT) windows MAKEVARIANT=1; \
	$(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT)/windows/$(CurrDir) $(notdir $@) MAKEVARIANT=1)
	$(TEST_NOTDIR) $(GLOBAL_POOL)/$(SRCDIR)/linux64/$(CurrDir)$(notdir $@) || \
	($(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT) linux64 MAKEVARIANT=1; \
	$(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT)/linux64/$(CurrDir) $(notdir $@) MAKEVARIANT=1)
	$(TEST_NOTDIR) $(GLOBAL_POOL)/$(SRCDIR)/linuxarm/$(CurrDir)$(notdir $@) || \
	($(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT) linuxarm MAKEVARIANT=1; \
	$(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT)/linuxarm/$(CurrDir) $(notdir $@) MAKEVARIANT=1)
	$(TEST_NOTDIR) $(GLOBAL_POOL)/$(SRCDIR)/mac/$(CurrDir)$(notdir $@) || \
	($(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT) mac MAKEVARIANT=1; \
	$(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT)/mac/$(CurrDir) $(notdir $@) MAKEVARIANT=1)
ifeq "$(findstring i86pc,$(CurrDir))" "i86pc"
	$(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT)/$(NotVariantDir) $(notdir $@) MAKEVARIANT=1
endif
ifeq "$(findstring sun4,$(CurrDir))" "sun4"
	$(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT)/$(NotVariantDir) $(notdir $@) MAKEVARIANT=1
endif
ifeq "$(findstring sun4,$(CurrDir))" "linux"
	$(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT)/$(NotVariantDir) $(notdir $@) MAKEVARIANT=1
endif
ifeq "$(findstring sun4,$(CurrDir))" "windows"
	$(MAKE) -$(MAKEFLAGS) -C $(POOL_ROOT)/$(NotVariantDir) $(notdir $@) MAKEVARIANT=1
endif
endif

# Alle SubDirectories anlegen
all_dirs: $(SubDirs)

# Verzeichnis fuer kompilierte Objektdateien
$(OBJDIR):
	$(MKDIR) $(notdir $@)

# Pool Structure anlegen
poolstruct: $(SRCDIR) $(POOL_DIRS) FORCE

POOL_%: FORCE
	$(TEST_DIR) $(subst POOL_,./,$@) || $(MAKE) -$(MAKEFLAGS) $(subst POOL_,./,$@)
	$(MAKE) -$(MAKEFLAGS) -C $(subst POOL_,./,$@) poolstruct

$(SRCDIR):
ifeq "$(POOL_ROOT)" "."
	$(TEST_DIR) $(SRCDIR) || $(SYMLINK) $(CB_ROOT)/src/ProductPOOL $(SRCDIR)
else
	$(TEST_DIR) $(SRCDIR) || $(SYMLINK) $(POOL_ROOT)/$(SRCDIR)/$(CurrDir) $(SRCDIR)
endif

poolroot: $(SRCDIR)
	$(TEST_FILE) Makefile || $(SYMLINK) $(SRCDIR)/Makefile

#---------------------------------------------------
# CVS Regel (nur fuer CheckOut)
#--------------------------------------------------
checkout commit:
	cd $(POOL_ROOT)/.. ;\
	$(CVS) $@ ProductPOOL/$(CurrDir)$(CVSFILE)


#-----------------------------------------------
# Test fuer CheckIn
#-----------------------------------------------
ifdef CHECKIN
ifeq ".c" "$(suffix $(CHECKIN))"
test_checkin: $(CHECKIN)
	$(CC_TEST)
endif

ifeq ".cc" "$(suffix $(CHECKIN))"
test_checkin: $(CHECKIN)
	$(CXX_TEST)
endif

ifeq ".l" "$(suffix $(CHECKIN))"
test_checkin: $(CHECKIN)
	$(LEX) -t $(LFLAGS) $< > LEX_TEST.c
	$(CC) $(CFLAGS) -c LEX_TEST.c -o /dev/null
	$(RM) LEX_TEST.c
endif

ifeq ".y" "$(suffix $(CHECKIN))"
test_checkin: $(CHECKIN)
	$(YACC) $(YFLAGS) $< -o YACC_TEST.c
	$(CC) $(CFLAGS) -c YACC_TEST.c -o /dev/null
	$(RM) YACC_TEST.c
endif

ifeq ".pro" "$(suffix $(CHECKIN))"
test_checkin: $(CHECKIN)
	$(PROLOG_TEST)
endif

ifeq ".tex" "$(suffix $(CHECKIN))"
test_checkin: $(CHECKIN)
	$(TEX_TEST)
endif

ifeq ".pm" "$(suffix $(CHECKIN))"
test_checkin: $(CHECKIN)
	$(PERL_TEST)
endif

# Alle anderen Files
ifeq "$(findstring $(suffix $(CHECKIN)),.c .cc .l .y .pro .tex .pm)" ""
test_checkin: $(CHECKIN)
	@echo "Don't know how to verify $< ."
endif

endif

#-----------------------------------------------
# Informationen zu momentanen Directory
#-----------------------------------------------

info: FORCE
	@echo "$(INFO_TXT)\n"
	@echo
	@echo "Die z.Z. eingestellten Directories sind:";\
	echo "aktuelles Directory  : $(shell pwd)";\
	echo "Variante             : $(CB_VARIANT)";\
	echo "LOCATION             : $(CB_LOCATION)";\
	echo "JAVA_CAPABLE         : $(JAVA_CAPABLE)";\
	echo "Globale Pool-Wurzel  : $(GLOBAL_POOL)";\
	echo "akt. Pool-Directory  : $(CurrDir)";\
	echo "Sub-Module           : $(SubDirs)";\
	echo "C-Sourcen            : $(C_SOURCES)";\
	echo "C++-Sourcen          : $(CXX_SOURCES)";\
	echo "Prolog-Sourcen       : $(PROLOG_SOURCES)";\
	echo "TeX-Sourcen (Main)   : $(TEX_SOURCES)";\
	echo "TeX-Sourcen (inkl.)  : $(TEX_SOURCES_INCL)";\
	echo "Makefiles            : $(MK_SOURCES)";\
	echo "Shell-Skripte        : $(SH_SOURCES)";\
	echo "Perl-Sourcen         : $(PERL_SOURCES)";\
	echo "Java-Sourcen         : $(JAVA_SOURCES)";\
	echo "Java-Main            : $(JAVA_MAIN)";\
	echo "Restliche Sourcen    : $(filter-out $(SOURCES),$(shell /bin/ls src))";\
	echo "Targets              : $(TARGETS)";\
	echo "Exportierte Files    : $(EXPORTFILES)"


#-----------------------------------------------
# Aufruf von Makefiles in SubDirs
#-----------------------------------------------

# Bei Windows wird der Test nicht ausgefuehrt,
# es sollten immer alle Verzeichnis vorhanden sein
ifeq "$(CB_VARIANT)" "windows"
MAKE_%: FORCE
	$(MAKE) -$(MAKEFLAGS) -C $(subst MAKE_,./,$@)

EXPORT_%: FORCE
	$(MAKE) -$(MAKEFLAGS) -C $(subst EXPORT_,./,$@) export

CLEAN_%: FORCE
	$(MAKE) -$(MAKEFLAGS) -C $(subst CLEAN_,./,$@) clean
else
MAKE_%: FORCE
	$(TEST_NOTDIR) $(subst MAKE_,./,$@) || $(MAKE) -$(MAKEFLAGS) -C $(subst MAKE_,./,$@)
# Exportieren von Dateien in allen Pools
EXPORT_%: FORCE
	$(TEST_NOTDIR) $(subst EXPORT_,./,$@) || $(MAKE) -$(MAKEFLAGS) -C $(subst EXPORT_,./,$@) export
# Aufraeumen in Sub-Directories
CLEAN_%: FORCE
	$(TEST_NOTDIR) $(subst CLEAN_,./,$@) || $(MAKE) -$(MAKEFLAGS) -C $(subst CLEAN_,./,$@) clean
endif

clean: $(CLEAN_DIRS) FORCE
	-$(RM) $(CLEAN_FILES) $(OBJDIR)/* $(EXPDIR)/*

install:
	 @$(CB_ADMIN)/bin/CB_Install

ifdef NotVariantDir
touch_src:
	@$(CB_ADMIN)/bin/touch_ifw $(POOL_ROOT)/$(NotVariantDir)/*
	@$(CB_ADMIN)/bin/touch_ifw *
else
touch_src:
	@$(CB_ADMIN)/bin/touch_ifw *
endif

FORCE:

# Alles, was an Objectfiles fuer Targets erstellt wurde,
# soll erhalten bleiben.
.PRECIOUS: $(TARGETS) $(OBJECTS) $(PROLOG_PP_OBJECTS)

endif  # ifndef RULES_MK_INC

