options {
  STATIC = false;
  LOOKAHEAD = 1;
  DEBUG_PARSER = false;
  ERROR_REPORTING = true;
}

PARSER_BEGIN(NotificationParser)

package i5.cb.api.notification;

import i5.cb.telos.frame.*;
import java.io.DataOutputStream;
import java.io.ByteArrayOutputStream;
import com.objectspace.jgl.Sequence;
import com.objectspace.jgl.SList;


/**
 * parses notification sent from a ConceptBase server
 * <li> First, construct a NotificationParser object: <br>
 *      <tt> NotificationParser parser = new NotificationParser( System.in ); </tt>
 * <li> Second, call the function that parses up to the nonterminal symbol
 *      you are expecting, generally Notification.
 *      <tt> Notification notification = parser.notification(); </tt>
 * <li> If you want to go on parsing, just repeat calling the function
 *      corresponding to the appropiate nonterminal. For a list of all
 *      nonterminals, see <a href="file:/home/radig/prcs-work/crda/java/i5/cb/api/notification/Notification.html">the grammar of notifications</a>
 * </ul>
 *
 * @author Christoph Radig
 */

public class NotificationParser
{
}  // class NotificationParser

PARSER_END(NotificationParser)


///////////////////////////////////////////////////////////////////////////////
// Scanner
///////////////////////////////////////////////////////////////////////////////

TOKEN :
{
  < PLUS: "plus" >
|
  < MINUS: "minus" >
|
  < SELECT: "select" >
|
  < DERIVE: "derive" >
|
  < SUBSTITUTE: "substitute" >
|
  < SELECTOR1: "!" | "^" | "@" >
|
  < SELECTOR2: "->" | "=>" >
|
  ":" | ";" | "," | "(" | ")" | "[" | "]" | "/" | "_"
|
  < ALPHANUM: (["a"-"z","A"-"Z","0"-"9"])+ >
|     
  < #Num : ["0"-"9"] >
|
  < NUMBER: ("-")? (<Num>)+
          | ("-")? ( (<Num>)+ "." (<Num>)* 
                     | (<Num>)* "." (<Num>)+ 
                   ) 
                   ( ["E","e"] (["-","+"])? (<Num>)+ )? >
|
 < SIMPLELABEL: (~[".","|","'","\"","$",":",";"," ","\n","@","\t","\r","\f",
                   "!","^","-",">","=",",","(",")","[","]","{","}","/"])+ >
}


<DEFAULT> MORE :
{
  "\"" : WithinString
|
  "$"  : WithinRule
}


<WithinString> MORE :
{
  <~["\"","\\"]>
|
  "\\\""
|
  "\\\\"
}

<WithinString> TOKEN :
{
  < STRINGLABEL: "\"" > : DEFAULT
}


<WithinRule> MORE :
{
  <~["$"]>
}

<WithinRule> TOKEN :
{
  < RULELABEL: "$" > : DEFAULT
}


///////////////////////////////////////////////////////////////////////////////
// Parser
///////////////////////////////////////////////////////////////////////////////

/**
 * parses a notification message (as part of an ConceptBase ipcanswer message)
 **/
Notification notification() :
{
  Notification r;
}
{
  "[" r=updateTerms() "]"  { return r; }
}


/**
 * parses a term that the server sends in response to an ASK with the
 * answer format VIEW
 **/
Sequence askVIEWTerm() :
{
  Sequence r;
}
{
  "[" r=terms() "]"  { return r; }
}


Notification updateTerms() :
{
  Notification r = new Notification();  // empty list
  UpdateTerm t;
}
{
  [ 
    t=updateTerm()  { r.add( t ); } 
    ( "," t=updateTerm() { r.add( t ); } )* 
  ]

  { return r; }
}  // updateTerms


UpdateTerm updateTerm() :
{
  Term t;
}
{
  <PLUS> "(" t=term() ")"  { return new UpdateTerm( UpdateTerm.PLUS, t ); }
|
  <MINUS> "(" t=term() ")"  { return new UpdateTerm( UpdateTerm.MINUS, t ); }
}  // updateTerm


Sequence terms() :
{
  Sequence r = new SList();
  Term t;
}
{
  [ 
    t=term()  { r.add( t ); } 
    ( "," t=term() { r.add( t ); } )* 
  ]

  { return r; }
}  // terms


Term term() :
{
  Term r;
  Label lView;
}
{
  lView=label() "("
  (
    LOOKAHEAD( 2 )
    r=viewTermX( lView )  { return r; }
  |
    r=attributeTermX( lView )  { return r; }
  )
}  // term


ViewTerm viewTerm() :
{
  Label lView;
  ViewTerm r;
}
{
  lView=label() "(" r=viewTermX( lView )  { return r; }
}  // viewTerm


ViewTerm viewTermX( Label lView ) :
{
  ObjectName o;
}
{
  o=objectName() ")"  

  // { return new ViewTerm( lView, o ); }

  // WORKAROUND fuer DeriveExpressions. Diese werden vom Server
  // nicht an der richtigen Stelle zurueckgegeben. (CQ ist anderer
  // Meinung). Daher werden die Bindings an das 'Funktor'-Objekt
  // verschoben:
  {
    ViewTerm r;

    if( o instanceof DeriveExp ) {
      DeriveExp de = (DeriveExp) o;
      ObjectName viewName = new DeriveExp( lView, de.getBindings() );
      r = new ViewTerm( viewName, de.getObjectName() );
    }
    else
      r = new ViewTerm( lView, o );

    return r;
  }
}  // viewTermX


AttributeTerm attributeTerm() :
{
  Label lView;
  AttributeTerm r;
}
{
  lView=label() "(" r=attributeTermX( lView )  { return r; }
}  // attributeTerm


AttributeTerm attributeTermX( Label lView ) :
{
  Label lAttrCategory, lLabel;
  ObjectName oSource, oDest;
}
{
  lAttrCategory=label() "," oSource=objectName() "," 
  lLabel=label() "," oDest=objectName() ")"

  // { return new AttributeTerm( lView, lAttrCategory, oSource, lLabel, oDest ); }

  // WORKAROUND fuer DeriveExpressions. Diese werden vom Server
  // nicht an der richtigen Stelle zurueckgegeben. (CQ ist anderer
  // Meinung). Daher werden die Bindings an das 'Funktor'-Objekt
  // verschoben:
  {
    AttributeTerm r;

    if( oSource instanceof DeriveExp ) {
      DeriveExp de = (DeriveExp) oSource;
      ObjectName viewName = new DeriveExp( lView, de.getBindings() );
      r = new AttributeTerm( viewName, lAttrCategory, de.getObjectName(), lLabel, oDest );
    }
    else
      r = new AttributeTerm( lView, lAttrCategory, oSource, lLabel, oDest );

    return r;
  }
}  // attributeTermX


ObjectName objectName() :
{
  Label l;
  ObjectName o1, o2;
  String s;
  SList subst;
}
{
  l=label()
    { return l; }
|
  <SELECT> "(" o1=objectName() "," s=selector() "," o2=objectName() ")"
    { return new SelectExp( o1, s, o2 ); }
|
  // Notifications may contain derived expressions that contain object names
  // that are no labels.
  <DERIVE> "(" o1=objectName() "," "[" subst=substitutions() "]" ")"
    { return new DeriveExp( o1, subst ); }
}  // objectName


String selector() :
{
  Token t;
}
{
  t=<SELECTOR1>  { return t.image; }
|
  t=<SELECTOR2>  { return t.image; }
}  // selector


SList substitutions() :
{
  SList r = new SList();
  Substitution s;
}
{
  [
    s=substitution()  { r.add( s ); }
    ( "," s=substitution() { r.add( s ); } )*
  ]
  
  { return r; }
}  // substitutions


Substitution substitution() :
{
  ObjectName o;
  Label l;
}
{
  <SUBSTITUTE> "(" o=objectName() "," l=label() ")"
    
  { return new Substitution( o, l ); }
}  // substitution


Label label() :
{
  Token t;
}
{
  t=<ALPHANUM>  { return new Label( t.image ); }
|
  t=<NUMBER>  { return new Label( t.image ); }
    // nicht ueberfluessig!
|
  t=<SIMPLELABEL>  { return new Label( t.image ); }
|
  t=<STRINGLABEL>  { return new Label( t.image ); }
|
  t=<RULELABEL>  { return new Label( t.image ); }
}


