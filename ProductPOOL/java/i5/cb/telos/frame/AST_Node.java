/*
The ConceptBase.cc Copyright

Copyright 1987-2025 The ConceptBase Team. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of
      conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE CONCEPTBASE TEAM ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE CONCEPTBASE TEAM OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the authors
and should not be interpreted as representing official policies, either expressed or implied,
of the ConceptBase Team.


The ConceptBase Team is represented by

Manfred Jeusfeld, University of Skovde, 54128 Skovde, Sweden


This license is a FreeBSD-style copyright license.
Legal home of the FreeBSD copyright license: http://www.freebsd.org/copyright/freebsd-license.html
*/

package i5.cb.telos.frame;

import java.io.DataOutputStream;
import java.io.Serializable;


/**
 * Abstract base of all nodes of the abstract syntax tree that
 * represents a telos frame
 * @author Christoph Radig
 */

public interface AST_Node
  extends Serializable, Cloneable
{
  /**
   * writes the text representation of this object (node and subnodes)
   * in 'Plain Aachen' syntax. 
   * Writing into an OutputStream is *much* more efficient than recursively
   * constructing a String. To write into a String, do the following: <br> <br>
   * <tt> ByteArrayOutputStream bos = new ByteArrayOutputStream(); <br>
   * DataOutputStream dos = new DataOutputStream( bos ); <br>
   * frames.writeTelos( dos ); <br>
   * String s = bos.toString(); </tt> <br>

   * @param os the OutputStream to write into <br>
   */
  void writeTelos( DataOutputStream os )
    throws java.io.IOException;
    // PRE( nonNull( os ) );


  /**
   * All AST nodes are cloneable. Most of them are immutable,
   * thus their clone() returns a shallow copy.
   */
  public Object clone();


  /**
   * @Deprecated
   */
  String toSMLFragment();
}

