/*
The ConceptBase.cc Copyright

Copyright 1987-2025 The ConceptBase Team. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of
      conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE CONCEPTBASE TEAM ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE CONCEPTBASE TEAM OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the authors
and should not be interpreted as representing official policies, either expressed or implied,
of the ConceptBase Team.


The ConceptBase Team is represented by

Manfred Jeusfeld, University of Skovde, 54128 Skovde, Sweden


This license is a FreeBSD-style copyright license.
Legal home of the FreeBSD copyright license: http://www.freebsd.org/copyright/freebsd-license.html
*/

package i5.cb.telos.examples;

import i5.cb.telos.frame.*;

import java.io.DataOutputStream;

import com.objectspace.jgl.HashSet;
import com.objectspace.jgl.Set;


class Example
{
  public static void test1()
    throws java.io.IOException
  {
    ObjectName[] isa2 = { new Label( "Employee" ), new Label( "Ausbeuter" ) };

    TelosFrame frame = 
      new TelosFrame( new Label( "Class" ), 
		      new Label( "Manager" ),
		      new ObjectNames(), new ObjectNames( isa2 ), null
		    );

    DataOutputStream dos = new DataOutputStream( System.out );
    frame.writeTelos( dos );

    // append Proposition to inSpec:

    frame.setInSpec( frame.inSpec().appendedBy( new Label( "Proposition" ) ) );

    frame.writeTelos( dos );
  }  // test1


  public static void test2()
    throws java.io.IOException
  {
    TelosFrame frame = new TelosFrame( new Label( "NE-VW 523" ) );
    
    // ObjectNames inSpec = new ObjectNames();
    // inSpec.append( new Label( "Golf" ) );
    // frame.setInSpec( inSpec );

    ObjectNames inSpec = new ObjectNames( new Label( "Golf" ) );
    frame.setInSpec( inSpec );

    Set set = new HashSet();
    set.add( new Label( "Golf" ) );
    set.add( new Label( "Auto" ) );

    frame.setInSpec( new ObjectNames( set.elements() ) );

    DataOutputStream dos = new DataOutputStream( System.out );
    frame.writeTelos( dos );

    System.out.println( "in spec: " );
    java.util.Enumeration en = frame.inSpec().elements();
    while( en.hasMoreElements() )
      System.out.println( en.nextElement() + " " );
    System.out.println();

  }  // test2


  public static void main( String[] args )
    throws java.io.IOException
  {
    test1();
    test2();
  }  // main
}  // class Example
