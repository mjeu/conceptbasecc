/**
The ConceptBase.cc Copyright

Copyright 1987-2020 The ConceptBase Team. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of
      conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE CONCEPTBASE TEAM ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE CONCEPTBASE TEAM OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the authors
and should not be interpreted as representing official policies, either expressed or implied,
of the ConceptBase Team.


The ConceptBase Team is represented by

Manfred Jeusfeld, University of Skovde, 54128 Skovde, Sweden
Matthias Jarke, RWTH Aachen, Informatik 5, Ahornstr. 55, 52056 Aachen, Germany
Christoph Quix, RWTH Aachen, Informatik 5, Ahornstr. 55, 52056 Aachen, Germany


This license is a FreeBSD-style copyright license.
Legal home of the FreeBSD copyright license: http://www.freebsd.org/copyright/freebsd-license.html
**/
/*************************************************************************
*
* File:         MetaSimplifier.pro
* Version:      2.3
*
*
* Date released : 96/02/12  (YY/MM/DD)
*
* SCCS-Source-Pool : /home/CBase/CB_NewStruct/ProductPOOL/serverSources/Prolog_Files/SCCS/s.MetaSimplifier.pro
* Date retrieved : 96/02/12 (YY/MM/DD)
**************************************************************************
*
* Das Modul MetaSimplifier implementiert den Algorithmus zur Vereinfachung
* von Metaformeln. Es stellt die partielle Auswertung von Metaformeln der
* bisherigen Formelauswertung hinzu
*
* Verfahrensbeschreibung in groben Zuegen
*
* Eingabe: Metaformel f, d.h. Formel mit Klassenvariablen kv
* Ausgabe: 3 Mengen:
*	Menge von generierten Formeln gf
*	Menge von Prozedurtriggern (Einfuegung) it
*	Menge von Prozedurtriggern (Loeschung)  dt
*
* Verfahren (fuer constraints)
*  1. Teste, ob Formel f Metaformel
*  2. Forme diese Formel in das Format
*     forall([x1,..,xk],[L1,...,Ln],subFormula) um,
*     so dass kv eine Teilmenge von [x1,..,xk]
*  3. Waehle eine Folge von Literalen aus [L1,..,Ln]
*     (OBDA [L1,...,Lk])
*     so dass alle Elemente aus kv nach durch die
*     Auswertung dieser Literale gebunden werden koennen
*     rufe 4. mit f und L = [L1,...Lk] auf
*  4. Werte diese Literale aus
*     4.0 Ist L = [], dann Ende
*       sonst
*     	Sei L= [Li,...,Lk] die noch auszuwertende Literalfolge
*     	4.1  Berechne die Extension von Li: Ext(Li) = [Li1,...,Lij]
*     	4.2  Erzeuge eine Menge von Formeln f_i
*            indem die Variablen von Li durch die jeweiligen
*            Konstanten aus Ext(Li) ersetzt werden
*      4.3   Fuege den folgenden Trigger in it hinzu
*                 "Wird die Extension von Li um ein neues Element Li(j+1)
*                  vergroessert, dann erzeuge die entsprechende Formel wie
*		   unter 4.2 und rufe 4.4 mit dieser neuen Formel auf "
*      4.4   Fuege fuer jede Formel f_im aus f_i in dt den folgenden Trigger hinzu
*                 "Wird L_im aus Ext(Li) geloescht, dann loesche f_im und alle aus
*                  der Formel f_im generierten Objekte"
*      4.5   Rufe 4 fuer jede Formel aus f_i mit [Li+1,...,Lk] auf
*
*
*  Fuer Regeln wird das Verfahren analog durchgefuehrt, ausser das
*  das Format anders gewaehlt ist. Waehrend in constraints nur
*  allquantifizierte Literale ersetzt werden, werden fuer Regeln
*  sowohl all- als auch existenzquantifizierte Literale ersetzt.
*
*
*
*
*
*/

:- module('MetaSimplifier',[
'metaSimplifier'/7
,'testIfMetaFormula'/3
]).
:- use_module('GlobalPredicates.swi.pl').
:- use_module('debug.swi.pl').


:- use_module('MetaBindingPath.swi.pl').

:- use_module('MetaFormulas.swi.pl').












:- use_module('MetaLiterals.swi.pl').
:- use_module('MetaRFormulas.swi.pl').






:- use_module('MetaRFormToAssText.swi.pl').
:- use_module('MetaTriggerGen.swi.pl').


:- use_module('GeneralUtilities.swi.pl').
:- use_module('MetaUtilities.swi.pl').





:- use_module('RangeformSimplifier.swi.pl').
:- use_module('ErrorMessages.swi.pl').
:- use_module('GlobalParameters.swi.pl').

:- use_module('SemanticOptimizer.swi.pl').


:- style_check(-singleton).






/*
testMS(forall([p,c,m,d,x],[In(x,PseudoClass),In(m,PseudoClass),In(p,Nec),In(x,c),P(p,c,m,d)],exists([y],[In(y,d),A(x,m,y)],TRUE)),_peFormulas,_trigger,_substList).
testMS(forall([p,c,m,d,x],[In(p,Single),In(x,c),P(p,c,m,d)],forall([y,z],[In(y,d),In(z,d),A(x,m,y),A(x,m,z)],and([Eq(y,z)]))),_peFormulas,_trigger,_substList).
testMS(forall([p,c,m,d,y],[In(p,RevSingle),In(y,d),P(p,c,m,d)],forall([x1,x2],[In(x1,c),In(x2,c),A(x1,m,y),A(x2,m,y)],and([Eq(x1,x2)]))),_peFormulas,_triggerList,_substList).
testMS(forall ([p,c,m,d],[In(p,NonCircular) ,P(p,c,m,d)],forall( [x,y], [In(x,c),A(x,m,y)],and([not(Eq(x,y))]))),_peFormulas,_triggerList,_substList).
testMS(forall([c],[In(c,NonEmptyClass)],exists([x],[In(x,c)],TRUE)),_peFormulas,_triggerList,_substList).
testMS(forall([c],[In(c,EmptyClass)],forall([x],[In(x,c)],FALSE)),_peFormulas,_triggerList,_substList).
testMS(forall( [p,c,m], [In(p,TransClosed),P(p,c,m,c)],forall([x,y],[In(x,c),In(y,c)], exists ([z],[In(z,c),A(x,m,z),A(z,m,y) ],and([A(x,m,y)])))),_peFormulas,_triggerList,_substList).
testMS(forall([x,m,y],[In(x,Pred),In(y,Pred),In(m,Module),In(x,m),A(x,uses,y)],or([In(y,m),A(m,imports,y)])),_peFormulas,_triggerList,_substList).


/* hat keinen Sinn, nur wegen "not"  */
testMS(forall([p,c,m,d,x,y],[In(p,Nec),In(x,c),P(p,c,m,d),In(y,d)],and([not(A(c,m,y)),A(x,m,y)])),_peFormulas,_trigger,_substList).

/* 	Funktor "rangeconstr" und loeschen von In Literalen mit den Systemklassen "Class"
	und "Proposition" aus der Suchmenge
*/
metaSimplifier(rangeconstr(forall([c,d,p],[In(c,Class),In(d,Class),In(p,Proposition),In(p,Necessary)],forall([m],[In(m,Proposition),P(p,c,m,d)],forall([x],[In(x,Proposition),In(x,c)],exists([y],[In(y,Proposition),In(y,d),A(x,m,y)],TRUE))))),_f,_t,_substList).
metaSimplifier(rangeconstr(forall([p],[In(p,Proposition),In(p,Necessary)],forall([m,x,c],[In(m,Proposition),In(x,Proposition),In(c,Class),In(x,c)],forall([d],[In(d,Class),P(p,c,m,d)],exists([y],[In(y,Proposition),In(y,d),A(x,m,y)],TRUE))))),_f,_t,_substList).


*/

testIfMetaFormula(_mode,_f,'redundant') :-
	simpleFormula(_f),
	isRedundant(_mode,_f),
	!.
testIfMetaFormula(_,_f,'simple') :-
	simpleFormula(_f),!.
testIfMetaFormula(_,_f,'meta') :-
	metaFormula(_f).



/*****************************************************
metaSimplifier(_f,_e,_fStrings,_iTrigs,_dTrigs,_substs,_classes)

Parameter
_f: Eingabeformel ( Metaformel)
_e: Instanzen bisher ausgewerteter Praedikate
_fStrings: generierte Formeln
_iTrigs: Trigger, die bei Einfuegungen feuern koennen
_dTrigs: Trigger, die bei Loeschungen feuern koennen
_substs: Ersetzungen
_classes: Klassen an die die generierten Formeln gehaengt werden

*****************************************************/

metaSimplifier(_f,_ePredsTillNow,[],[],[],[],[]) :-
	filterParameterFormula(_ePredsTillNow),!.

/** ticket #266: simple formulas that are redundant are not generated **/
metaSimplifier(rangeconstr(_rf),_ePredsTillNow,[],[],[],[],[]) :-
        simpleFormula(_rf),
        isRedundant(constraint,_rf),
        'WriteTrace'(high,'MetaSimplifier',['Formula ',nl,idterm(_rf),nl,'is always true and thus ignored']),
        !.

metaSimplifier(rangeconstr(_rf),_ePredsTillNow,_formulaStrings,_insertTriggerList,_deleteTriggerList,_substList,_classList) :-
	!,
	metaSimplify(_rf,_formulas1,_ePredList,_triggerList1,_substList),!,
	encloseConstraint(rangeconstr,_formulas1,_formulas,_triggerList1,_triggerList2),!,
	findFormulasClasses(_formulas,_classList),!,
	reCompileRFormulaList(_formulas,_formulaStrings),!,
	buildInsertTriggerList(_triggerList2,_ePredsTillNow,_insertTriggerList),
        !,
	buildDeleteTriggerList(_formulaStrings,_ePredsTillNow,_ePredList,_deleteTriggerList),
        !.

/** ticket #301: simple rules that are redundant are not generated **/
metaSimplifier(rangerule(_vars,_rf,_conclLit),_ePredsTillNow,[],[],[],[],[]) :-
        simpleFormula(rangerule(_vars,_rf,_conclLit)),
        isRedundant(rule,rangerule(_vars,_rf,_conclLit)),
        'WriteTrace'(high,'MetaSimplifier',['Rule ',idterm(rangerule(_vars,_rf,_conclLit)),' is redundant and thus ignored.']),
        !.


metaSimplifier(rangerule(_vars,_f,_lit),_ePredsTillNow,_formulaStrings,_insertTriggerList,_deleteTriggerList,_substList,_classList) :-
	!,
	metaSimplify(rangerule(_vars,_f,_lit),_formulas1,_ePredList,_triggerList1,_substList),!,
	encloseRule(rangerule,_formulas1,_vars,_substList,_lit,_rangeruleList,_triggerList1,_triggerList2),!,
	findFormulasClasses(_rangeruleList,_classList),!,
        reCompileRFormulaList(_rangeruleList,_formulaStrings),!,
	buildInsertTriggerList(_triggerList2,_ePredsTillNow,_insertTriggerList),
        !,
	buildDeleteTriggerList(_formulaStrings,_ePredsTillNow,_ePredList,_deleteTriggerList),
        !.

metaSimplifier(_formula,_,_,_,_,_,_) :-
	report_error('MSERR0','MetaSimplifier',[_formula]),
	fail.



metaSimplify(rangerule(_vars,_f,_lit),[_f],[[]],[],[subst([],[])]) :-
	simpleFormula(rangerule(_vars,_f,_lit)),!.



metaSimplify(_f,[_f],[[]],[],[subst([],[])]) :-
	simpleFormula(_f),!.

/** 	der Rule-Fall muss gesondert behandelt werden, da
	es sich bei einer Regel nicht um eine bereichsbeschraenkte
	Formel im eigentlichen Sinne handelt.
	Der MetaSimplify-Algorithmus arbeitet bei Regeln auf dem
	Bedingungsteil. Die Substitutionen, die fuer den Bedingungs-
	teil dabei gefunden werden, werden fuer das Folgerungsliteral
	nachgezogen. Trotzdem muss das Folgerungsliteral betrachtet
	werden, wenn die Metaformeleigenschaft getestet wird:
	Beispiel: Axiom 12 als Regel:
        rule
        ax12:
        $ forall x,c/VAR (exists p,d/VAR
             In(p,Proposition!IsA) and P(p,d,'*isa',c) and In(x,d)) ==> In(x,c)$
	end
	Erst durch das Folgerungsliteral wird c eine Metavariable.
**/
metaSimplify(rangerule(_litVars,_fInMF,_conclLit),_accForm,_ePredList,_triggerList,_substList) :-
	!,

	metaFormulaAnalysis(rangerule(_litVars,_fInMF,_conclLit),_mLits,_mVars,_status),
	/** erster Fehlerfall:
	   status \== 0: Formel enthaelt A(x,m,y) und x nicht durch
	   In(x,c) an Klasse bindbar **/

	(_status == 0;(report_error('MSERR1','MetaSimplifier',[_mLits]),fail)),!,
	delPseudoIns(_fInMF,_fT1MF),

        exploitFunctionalDependencies(_mVars,_fT1MF,_fFD),    /** ticket #276 **/

	convertIntoMFFormRule(_fFD,_f,_status2),

	/** zweiter Fehlerfall:
	   status2 \== 0
	   Formel laesst sich nicht in das Format forall x1,..,xk EPred(x1,..,xk) ==> phi
	   konvertieren (bei constraints)
	**/
	(_status2 == 0;(report_error('MSERR2','MetaSimplifier',[_fFD]),fail)),!,
	computeSearchSpaceRule(_f,_cons,_vars,_lits),
 	listDifference(_mVars,_vars,_testList),


	/** dritter Fehlerfall:
	   Es gibt Meta-Variablen, die nicht durch partielle Auswerung
	   gebunden werden koennen**/
	(empty(_testList);(report_error('MSERR3','MetaSimplifier',[_testList]),fail)),!,

        findBindingPath(_mVars,_cons,_vars,_lits,_bPath),
 
 	metaSimplifyFlist([_f],[_bPath],[[]],_ePredList1,_triggerList,_accForm1,[subst([],[])],_substList1),
	filterSimpleFormulas(rule,_mVars,_accForm1,_accForm,_substList1,_substList,_ePredList1,_ePredList).


metaSimplify(_fInMF,_accForm,_ePredList,_triggerList,_substList) :-

	metaFormulaAnalysis(_fInMF,_mLits,_mVars,_status),
	/* not_empty(_mVars), gilt, da nicht simpleFormula wg. cut*/

	/** erster Fehlerfall:
	   status \== 0: Formel enthaelt A(x,m,y) und x nicht durch
	   In(x,c) an Klasse bindbar **/

	(_status == 0;(report_error('MSERR1','MetaSimplifier',[_mLits]),fail)),!,

	delPseudoIns(_fInMF,_fT1MF),

        /** evaluate certain literals in _fInMF that can have only one solution **/
        /** ticket #276                                                         **/
        exploitFunctionalDependencies(_mVars,_fT1MF,_fFD),

	convertIntoMFForm(_fFD,_f,_status2),

	/** zweiter Fehlerfall:
	   status2 \== 0
	   Formel laesst sich nicht in das Format forall x1,..,xk EPred(x1,..,xk) ==> phi
	   konvertieren (bei constraints)
	**/
	(_status2 == 0;(report_error('MSERR2','MetaSimplifier',[_fFD]),fail)),!,
 	computeSearchSpace(_f,_cons,_vars,_lits),
 	listDifference(_mVars,_vars,_testList),

	/** dritter Fehlerfall:
	   Es gibt Meta-Variablen, die nicht durch partielle Auswerung
	   gebunden werden koennen**/
	(empty(_testList);(report_error('MSERR3','MetaSimplifier',[_testList]),fail)),!,

        findBindingPath(_mVars,_cons,_vars,_lits,_bPath),

 	metaSimplifyFlist([_f],[_bPath],[[]],_ePredList1,_triggerList,_accForm1,[subst([],[])],_substList1),
	filterSimpleFormulas(constraint,_mVars,_accForm1,_accForm,_substList1,_substList,_ePredList1,_ePredList),
        !.




/** first try to find a binding path _bPath with the optimistic cost level **/
findBindingPath(_mVars,_cons,_vars,_lits,_bPath) :-
   get_cb_feature(optimisticCostLevel,_mcost),   /** ticket #246 **/
   findBindingPathWithCostLevel(_mVars,_cons,_vars,_lits,_mcost,_bPath),
   !.

/** if not sucessful, try the maximum cost level **/
findBindingPath(_mVars,_cons,_vars,_lits,_bPath) :-
   get_cb_feature(maxCostLevel,_mcost),   /** ticket #246 **/
   findBindingPathWithCostLevel(_mVars,_cons,_vars,_lits,_mcost,_bPath),
   'WriteTrace'('veryhigh','MetaSimplifier',['Binding path found within the extended search space of maximum cost level']),
   !.

/** a binding path could not be found: **/
/**  vierter Fehlerfall:
	    Es gibt Meta-Variablen, fuer die keine
	    Literal-Auswertungsstrategie im Search-Space
	    gefunden wurde. Welche Auswertungsmuster zugelassen
	    sind, wird in MetaLiterals.pro festgelegt. Dabei duerfen
	    die Kosten fuer einen Auswertungsschritt nicht groesser
	    sein als im 5. Argument von findBindingPathsForVars
	    angegeben
	**/
findBindingPath(_mVars,_cons,_vars,_lits,_) :-
   report_error('MSERR4','MetaSimplifier',[_lits]),
   !,
   fail.


findBindingPathWithCostLevel(_mVars,_cons,_vars,_lits,_mcost,_bPath) :-        
	findBindingPathsForVars(_mVars,_cons,_vars,_lits,_mcost,_bPaths),
	not_empty(_bPaths),
 	selectCheapestPath(_mVars,_cons,_bPaths,_bPath,0),
        !.



/** rather complex partial evaluation of a meta formula where the formula is defined in the same **/
/** transaction where some data (extension for the ePreds) is told                               **/

metaSimplifyFlist([],_,[],[],[],[],_,[]) .
metaSimplifyFlist([_f|_formulas],[_bList|_bLists],[_fEPredsTillNow|_ePredListTillNow],_ePredList,_triggerList,_genForm,[_oldSubst|_oldSubsts],_substList) :-
	metaSimplifyFormula(_f,_bList,_fTriggerList,_genForm1,_fEPredsTillNow,_ePredList1,_oldSubst,_substList1),
	metaSimplifyFlist(_formulas,_bLists,_ePredListTillNow,_ePredList2,_formulasTriggerList,_genForm2,_oldSubsts,_substList2),
	append(_fTriggerList,_formulasTriggerList,_triggerList),
 	append(_genForm1,_genForm2,_genForm),
	append(_ePredList1,_ePredList2,_ePredList),
	append(_substList1,_substList2,_substList).


metaSimplifyFormula(_f,'Binds'(_,_,[],_),[],[_f],_fEPredsTillNow,[_fEPredsTillNow],_subst,[_subst]).
metaSimplifyFormula(_f,'Binds'(_c,_v,[_ePred|_bindingPath],_cost),
	[_trigInsert|_peTriggerList],_genFormulas,_fEPredsTillNow,_ePredList,_oldSubst,_substList) :-
        exploitFunctionalDependencies(_c,_f,_f1),   /** ticket #276 **/
	transformEquivalent(_ePred,_f1,_subFormula,_constants),
	computeExtension(_ePred,_constants,_extList),
	partialEvaluate(_subFormula,_ePred,_extList,_peFormulas,_oldSubst,_peSubstList,'Binds'(_c,_v,_bindingPath,_cost),_pePaths),
	saveDataForInsertTrigger(_fEPredsTillNow,_ePred,_extList,_constants,_subFormula,_oldSubst,_trigInsert),
	buildListOfLists(_fEPredsTillNow,_extList,_ePredListNew),
	metaSimplifyFlist(_peFormulas,_pePaths,_ePredListNew,_ePredList,_peTriggerList,_genFormulas,_peSubstList,_substList).














