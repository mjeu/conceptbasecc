{*
The ConceptBase.cc Copyright

Copyright 1987-2024 The ConceptBase Team. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of
      conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE CONCEPTBASE TEAM ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE CONCEPTBASE TEAM OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the authors
and should not be interpreted as representing official policies, either expressed or implied,
of the ConceptBase Team.


The ConceptBase Team is represented by

Manfred Jeusfeld, University of Skovde, 54128 Skovde, Sweden


This license is a FreeBSD-style copyright license.
Legal home of the FreeBSD copyright license: http://www.freebsd.org/copyright/freebsd-license.html
*}
{
*
* File: BigFlights.sml
* Author: Manfred Jeusfeld
* Creation: 6-Feb-2002
* ----------------------------------------------------------------------
* The two recursive queries of interest are here DeadEndCity and
* KindaClique. The first is without parameters. The latter can be called
* with any city as parameters, e.g. frankfurt.
* The queries will run only with ConceptBase V5.2.2 or later.
}



{********************}
{* The class level: *}
{********************}

Individual city in Class with 
   attribute
	con_to : city
end 

Attribute city!con_to in Class with 
   attribute
	departure: Integer;	
	arrival: Integer;
	flnr: String
end


{**********************}
{* The city instances *}
{**********************}


Individual leningrad in city  with
 con_to
       F1 : frankfurt;
       F626 : frankfurt;
       F750 : muenchen;
       F900 : hamburg;
       F1110 : duesseldorf
end 
Individual frankfurt in city  with
 con_to
       F4 : muenchen;
       F5 : muenchen;
       F8 : geneve;
       F10 : duesseldorf;
       F11 : hamburg;
       F35 : glasgow;
       F36 : saarbruecken;
       F37 : nice;
       F46 : zagreb;
       F49 : paris;
       F66 : hannover;
       F70 : paris;
       F73 : bucuresti;
       F80 : berlin;
       F86 : faro;
       F90 : berlin;
       F95 : berlin;
       F97 : berlin;
       F98 : amsterdam;
       F100 : berlin;
       F103 : london;
       F114 : berlin;
       F117 : antwerpen;
       F118 : birmingham;
       F119 : salzburg;
       F120 : hannover;
       F125 : koeln_bonn;
       F133 : amsterdam;
       F137 : berlin;
       F141 : nuernberg;
       F144 : hamburg;
       F147 : berlin;
       F149 : bremen;
       F161 : catania;
       F162 : leipzig;
       F172 : berlin;
       F174 : innsbruck;
       F178 : amsterdam;
       F182 : muenchen;
       F183 : antwerpen;
       F184 : porto;
       F187 : stockholm;
       F194 : hannover;
       F205 : helsinki;
       F210 : geneve;
       F211 : paris;
       F212 : valencia;
       F215 : muenchen;
       F224 : bremen;
       F232 : zuerich;
       F233 : sevilla;
       F238 : beograd;
       F246 : kobenhavn;
       F249 : nuernberg;
       F253 : split;
       F254 : porto;
       F267 : kobenhavn;
       F274 : beograd;
       F276 : bremen;
       F281 : sofia;
       F284 : budapest;
       F286 : birmingham;
       F291 : bremen;
       F298 : moskva;
       F313 : goeteborg;
       F324 : nuernberg;
       F332 : muenster;
       F333 : bucuresti;
       F334 : koeln_bonn;
       F339 : muenster;
       F346 : leningrad;
       F347 : manchester;
       F355 : paris;
       F356 : tenerife;
       F358 : tunis;
       F368 : bucuresti;
       F377 : london;
       F378 : paris;
       F382 : london;
       F386 : wien;
       F390 : newyork;
       F406 : chicago;
       F408 : wien;
       F413 : roma;
       F436 : paris;
       F437 : stockholm;
       F441 : hannover;
       F448 : koeln_bonn;
       F451 : basel;
       F457 : basel;
       F462 : muenster;
       F465 : laspalmas;
       F466 : stockholm;
       F471 : nuernberg;
       F472 : nuernberg;
       F476 : hannover;
       F477 : athine;
       F478 : hannover;
       F479 : helsinki;
       F481 : berlin;
       F488 : tripoli;
       F499 : duesseldorf;
       F509 : luxembourg;
       F511 : duesseldorf;
       F518 : duesseldorf;
       F529 : dallas;
       F535 : salzburg;
       F544 : duesseldorf;
       F563 : dallas;
       F566 : venezia;
       F573 : muenchen;
       F576 : hamburg;
       F577 : warszawa;
       F584 : ankara;
       F590 : luxembourg;
       F593 : roma;
       F606 : birmingham;
       F608 : graz;
       F609 : bogota;
       F612 : stockholm;
       F621 : kobenhavn;
       F622 : madrid;
       F623 : oslo;
       F628 : koeln_bonn;
       F638 : stuttgart;
       F646 : athine;
       F647 : linz;
       F650 : dublin;
       F651 : porto;
       F653 : madrid;
       F654 : oslo;
       F655 : porto;
       F656 : stuttgart;
       F657 : zuerich;
       F663 : budapest;
       F664 : stuttgart;
       F666 : stockholm;
       F670 : koeln_bonn;
       F672 : hamburg;
       F679 : stuttgart;
       F696 : manchester;
       F697 : thessaloniki;
       F698 : hamburg;
       F699 : zuerich;
       F708 : stuttgart;
       F711 : wien;
       F716 : ibiza;
       F717 : bogota;
       F718 : muenster;
       F724 : oslo;
       F729 : malta;
       F731 : stuttgart;
       F732 : luxembourg;
       F734 : koeln_bonn;
       F735 : tunis;
       F755 : luxembourg;
       F756 : luxembourg;
       F766 : london;
       F771 : lyon;
       F776 : bayreuth;
       F778 : palma;
       F782 : zagreb;
       F784 : zagreb;
       F795 : catania;
       F796 : koeln_bonn;
       F798 : koeln_bonn;
       F799 : hamburg;
       F810 : lisboa;
       F811 : hamburg;
       F814 : hamburg;
       F816 : thessaloniki;
       F818 : bologna;
       F820 : alger;
       F822 : casablanca;
       F826 : hamburg;
       F827 : dublin;
       F830 : lyon;
       F831 : stuttgart;
       F835 : hamburg;
       F836 : bayreuth;
       F839 : klagenfurt;
       F840 : hamburg;
       F843 : bayreuth;
       F844 : hamburg;
       F845 : genova;
       F847 : izmir;
       F852 : hamburg;
       F858 : hamburg;
       F861 : dublin;
       F863 : oslo;
       F868 : bruxelles;
       F869 : hamburg;
       F876 : wien;
       F884 : basel;
       F885 : hannover;
       F888 : bordeaux;
       F902 : barcelona;
       F907 : lisboa;
       F913 : kobenhavn;
       F919 : lisboa;
       F920 : zuerich;
       F924 : bogota;
       F928 : timisoara;
       F940 : helsinki;
       F941 : luxembourg;
       F947 : moskva;
       F949 : sanjuan;
       F951 : stuttgart;
       F954 : ibiza;
       F956 : sanjuan;
       F964 : bilbao;
       F965 : istanbul;
       F968 : moskva;
       F971 : atlanta;
       F972 : paris;
       F975 : athine;
       F980 : marseille;
       F981 : atlanta;
       F982 : marseille;
       F985 : bologna;
       F990 : moskva;
       F1000 : torino;
       F1003 : alger;
       F1009 : wien;
       F1012 : dubrovnik;
       F1013 : wien;
       F1014 : roma;
       F1018 : kobenhavn;
       F1019 : tunis;
       F1023 : paderborn;
       F1030 : london;
       F1040 : hamburg;
       F1047 : london;
       F1052 : birmingham;
       F1054 : hamburg;
       F1058 : london;
       F1066 : zuerich;
       F1068 : hamburg;
       F1069 : glasgow;
       F1071 : warszawa;
       F1077 : kobenhavn;
       F1083 : muenchen;
       F1084 : alger;
       F1088 : london;
       F1089 : antwerpen;
       F1091 : faro;
       F1093 : saarbruecken;
       F1094 : faro;
       F1099 : muenchen;
       F1101 : dortmund;
       F1104 : muenchen;
       F1108 : lapaz;
       F1117 : pisa;
       F1131 : paderborn;
       F1135 : casablanca;
       F1139 : casablanca;
       F1149 : napoli;
       F1150 : paris;
       F1156 : hof;
       F1157 : muenchen;
       F1168 : linz;
       F1169 : graz;
       F1172 : praha;
       F1173 : bruxelles;
       F1174 : bruxelles;
       F1179 : muenchen;
       F1195 : barcelona;
       F1198 : barcelona;
       F1206 : linz;
       F1216 : stuttgart;
       F1218 : sofia;
       F1223 : zagreb;
       F1224 : muenchen;
       F1230 : muenchen;
       F1233 : zagreb;
       F1234 : zuerich;
       F1237 : duesseldorf;
       F1240 : madrid;
       F1247 : pisa;
       F1248 : wien;
       F1260 : napoli;
       F1264 : venezia;
       F1270 : heraklion;
       F1274 : duesseldorf;
       F1275 : saarbruecken;
       F1280 : duesseldorf;
       F1286 : berlin;
       F1287 : duesseldorf;
       F1297 : lisboa;
       F1299 : newyork;
       F1301 : geneve;
       F1309 : duesseldorf;
       F1311 : praha;
       F1325 : berlin;
       F1332 : leningrad;
       F1351 : leipzig;
       F1354 : geneve;
       F1355 : saarbruecken;
       F1359 : muenchen;
       F1361 : goeteborg;
       F1373 : dortmund;
       F1376 : muenchen;
       F1379 : stuttgart;
       F1382 : muenchen;
       F1386 : muenchen;
       F1392 : innsbruck;
       F1400 : bruxelles;
       F1403 : duesseldorf;
       F1404 : toulouse;
       F1410 : hof;
       F1421 : faro;
       F1426 : muenchen;
       F1430 : helsinki;
       F1435 : duesseldorf;
       F1436 : berlin;
       F1437 : hannover;
       F1440 : hannover;
       F1446 : muenchen;
       F1450 : lisboa;
       F1452 : sanjuan;
       F1453 : hamburg;
       F1454 : hannover;
       F1456 : hof;
       F1457 : muenchen;
       F1458 : muenchen;
       F1459 : muenchen;
       F1462 : muenchen;
       F1463 : saarbruecken;
       F1466 : muenchen;
       F1471 : madrid
end 
Individual kobenhavn in city  with
 con_to
       F2 : hamburg;
       F26 : hamburg;
       F34 : hamburg;
       F121 : bremen;
       F185 : frankfurt;
       F272 : hannover;
       F275 : muenchen;
       F279 : duesseldorf;
       F310 : duesseldorf;
       F365 : hannover;
       F380 : duesseldorf;
       F389 : hannover;
       F416 : frankfurt;
       F445 : hamburg;
       F464 : frankfurt;
       F559 : muenchen;
       F574 : muenchen;
       F713 : duesseldorf;
       F797 : hannover;
       F860 : muenchen;
       F875 : hamburg;
       F933 : hamburg;
       F999 : hamburg;
       F1119 : frankfurt;
       F1143 : hamburg;
       F1178 : hamburg;
       F1189 : hamburg;
       F1217 : hamburg;
       F1326 : frankfurt;
       F1341 : stuttgart;
       F1387 : frankfurt;
       F1394 : hamburg;
       F1399 : stuttgart;
       F1449 : hamburg
end 
Individual hamburg in city  with
 con_to
       F168 : helsinki;
       F250 : berlin;
       F308 : duesseldorf;
       F314 : duesseldorf;
       F322 : berlin;
       F328 : duesseldorf;
       F329 : duesseldorf;
       F342 : muenchen;
       F399 : duesseldorf;
       F410 : frankfurt;
       F444 : duesseldorf;
       F517 : duesseldorf;
       F642 : duesseldorf;
       F669 : frankfurt;
       F690 : frankfurt;
       F695 : frankfurt;
       F725 : frankfurt;
       F742 : frankfurt;
       F769 : frankfurt;
       F807 : duesseldorf;
       F821 : frankfurt;
       F866 : duesseldorf;
       F871 : frankfurt;
       F872 : frankfurt;
       F874 : frankfurt;
       F877 : frankfurt;
       F880 : muenchen;
       F899 : frankfurt;
       F912 : newyork;
       F916 : muenchen;
       F937 : frankfurt;
       F938 : muenchen;
       F939 : frankfurt;
       F998 : leningrad;
       F1001 : frankfurt;
       F1005 : frankfurt;
       F1022 : muenchen;
       F1032 : muenchen;
       F1073 : muenchen;
       F1090 : muenchen;
       F1115 : london;
       F1118 : muenchen;
       F1127 : duesseldorf;
       F1176 : berlin;
       F1219 : berlin;
       F1244 : london;
       F1246 : london;
       F1250 : london;
       F1265 : berlin;
       F1268 : berlin;
       F1279 : berlin;
       F1282 : berlin;
       F1283 : muenchen;
       F1285 : berlin;
       F1300 : duesseldorf;
       F1303 : westerland;
       F1305 : helsinki;
       F1315 : london;
       F1339 : muenchen;
       F1358 : frankfurt;
       F1367 : frankfurt;
       F1388 : berlin
end 
Individual bremen in city  with
 con_to
       F3 : muenchen;
       F68 : berlin;
       F77 : frankfurt;
       F213 : frankfurt;
       F219 : frankfurt;
       F273 : frankfurt;
       F319 : berlin;
       F321 : berlin;
       F361 : berlin;
       F367 : berlin;
       F463 : berlin;
       F587 : london;
       F668 : wangerooge;
       F676 : muenchen;
       F962 : london;
       F1006 : wangerooge;
       F1420 : muenchen
end 
Individual muenchen in city  with
 con_to
       F18 : frankfurt;
       F23 : london;
       F27 : duesseldorf;
       F32 : duesseldorf;
       F41 : frankfurt;
       F44 : frankfurt;
       F54 : berlin;
       F85 : frankfurt;
       F92 : frankfurt;
       F96 : frankfurt;
       F101 : frankfurt;
       F146 : newyork;
       F176 : frankfurt;
       F191 : berlin;
       F196 : frankfurt;
       F198 : duesseldorf;
       F199 : hamburg;
       F207 : newyork;
       F209 : berlin;
       F226 : frankfurt;
       F239 : frankfurt;
       F240 : frankfurt;
       F247 : berlin;
       F260 : duesseldorf;
       F287 : duesseldorf;
       F303 : berlin;
       F317 : frankfurt;
       F326 : duesseldorf;
       F341 : london;
       F370 : bremen;
       F379 : duesseldorf;
       F434 : paderborn;
       F440 : duesseldorf;
       F443 : duesseldorf;
       F467 : duesseldorf;
       F475 : berlin;
       F534 : berlin;
       F614 : roma;
       F645 : duesseldorf;
       F680 : paderborn;
       F730 : paderborn;
       F738 : frankfurt;
       F773 : paderborn;
       F808 : duesseldorf;
       F812 : roma;
       F851 : frankfurt;
       F932 : london;
       F983 : leningrad;
       F1010 : duesseldorf;
       F1015 : bremen;
       F1039 : bremen;
       F1050 : duesseldorf;
       F1056 : london;
       F1086 : london;
       F1107 : hamburg;
       F1142 : hamburg;
       F1171 : hamburg;
       F1291 : hamburg;
       F1304 : frankfurt;
       F1312 : hamburg;
       F1314 : frankfurt;
       F1316 : chicago;
       F1317 : hamburg;
       F1327 : frankfurt;
       F1331 : frankfurt;
       F1338 : hamburg;
       F1352 : frankfurt;
       F1409 : hamburg;
       F1413 : frankfurt;
       F1414 : frankfurt;
       F1433 : frankfurt;
       F1434 : hamburg
end 
Individual stuttgart in city  with
 con_to
       F6 : hannover;
       F24 : duesseldorf;
       F30 : duesseldorf;
       F61 : newyork;
       F104 : hamburg;
       F128 : koeln_bonn;
       F130 : hamburg;
       F148 : torino;
       F190 : hamburg;
       F197 : berlin;
       F202 : koeln_bonn;
       F290 : paris;
       F320 : athine;
       F335 : muenchen;
       F344 : berlin;
       F396 : madrid;
       F428 : bremen;
       F432 : bremen;
       F469 : bruxelles;
       F495 : paris;
       F523 : koeln_bonn;
       F526 : bruxelles;
       F545 : hannover;
       F582 : hamburg;
       F585 : muenchen;
       F595 : bruxelles;
       F603 : duesseldorf;
       F605 : duesseldorf;
       F613 : london;
       F640 : koeln_bonn;
       F649 : london;
       F652 : milano;
       F674 : zuerich;
       F715 : paderborn;
       F726 : kobenhavn;
       F741 : wien;
       F757 : hannover;
       F761 : dortmund;
       F762 : kobenhavn;
       F781 : berlin;
       F805 : dortmund;
       F815 : zuerich;
       F834 : zuerich;
       F837 : london;
       F894 : paris;
       F895 : hamburg;
       F945 : berlin;
       F946 : hamburg;
       F988 : frankfurt;
       F1004 : leipzig;
       F1028 : sanfrancisco;
       F1033 : duesseldorf;
       F1063 : frankfurt;
       F1081 : berlin;
       F1111 : frankfurt;
       F1122 : bremen;
       F1126 : london;
       F1133 : frankfurt;
       F1144 : wien;
       F1148 : bremen;
       F1153 : frankfurt;
       F1155 : milano;
       F1158 : berlin;
       F1162 : frankfurt;
       F1208 : roma;
       F1226 : hannover;
       F1227 : hamburg;
       F1229 : hannover;
       F1256 : frankfurt;
       F1272 : duesseldorf;
       F1273 : duesseldorf;
       F1278 : duesseldorf;
       F1313 : berlin;
       F1320 : bruxelles;
       F1328 : hamburg;
       F1360 : hamburg;
       F1378 : oslo;
       F1397 : newyork;
       F1432 : hannover;
       F1439 : hamburg;
       F1442 : berlin
end 
Individual hannover in city  with
 con_to
       F12 : frankfurt;
       F13 : berlin;
       F14 : nuernberg;
       F25 : edinburgh;
       F47 : frankfurt;
       F48 : frankfurt;
       F69 : milano;
       F79 : milano;
       F105 : frankfurt;
       F108 : frankfurt;
       F131 : zuerich;
       F151 : muenchen;
       F181 : zuerich;
       F195 : london;
       F229 : berlin;
       F230 : birmingham;
       F234 : muenchen;
       F264 : berlin;
       F268 : berlin;
       F278 : muenchen;
       F296 : muenchen;
       F348 : stuttgart;
       F349 : koeln_bonn;
       F371 : paris;
       F374 : nuernberg;
       F391 : koeln_bonn;
       F415 : muenchen;
       F417 : london;
       F453 : koeln_bonn;
       F468 : nuernberg;
       F491 : duesseldorf;
       F500 : muenchen;
       F503 : stuttgart;
       F527 : koeln_bonn;
       F530 : berlin;
       F542 : stockholm;
       F546 : berlin;
       F553 : stockholm;
       F571 : duesseldorf;
       F579 : koeln_bonn;
       F604 : frankfurt;
       F637 : bruxelles;
       F644 : duesseldorf;
       F658 : koeln_bonn;
       F678 : frankfurt;
       F687 : muenchen;
       F706 : muenchen;
       F709 : wien;
       F723 : wien;
       F728 : duesseldorf;
       F786 : kobenhavn;
       F856 : frankfurt;
       F857 : oslo;
       F873 : oslo;
       F889 : london;
       F896 : london;
       F921 : amsterdam;
       F925 : amsterdam;
       F953 : amsterdam;
       F960 : amsterdam;
       F992 : paris;
       F1007 : duesseldorf;
       F1026 : wien;
       F1059 : duesseldorf;
       F1114 : berlin;
       F1136 : muenchen;
       F1140 : stuttgart;
       F1261 : paris;
       F1329 : zuerich;
       F1330 : zuerich;
       F1335 : kobenhavn;
       F1346 : zuerich;
       F1357 : muenchen;
       F1364 : helsinki;
       F1365 : kobenhavn;
       F1374 : stuttgart;
       F1381 : stuttgart;
       F1401 : kobenhavn;
       F1419 : helsinki;
       F1427 : stuttgart
end 
Individual paris in city  with
 con_to
       F7 : frankfurt;
       F65 : frankfurt;
       F67 : frankfurt;
       F82 : frankfurt;
       F91 : stuttgart;
       F271 : duesseldorf;
       F301 : frankfurt;
       F364 : frankfurt;
       F373 : berlin;
       F402 : duesseldorf;
       F433 : koeln_bonn;
       F454 : stuttgart;
       F459 : duesseldorf;
       F497 : bremen;
       F508 : muenchen;
       F538 : hamburg;
       F539 : berlin;
       F594 : hamburg;
       F630 : muenchen;
       F838 : stuttgart;
       F846 : muenchen;
       F850 : muenchen;
       F911 : hannover;
       F915 : duesseldorf;
       F929 : koeln_bonn;
       F936 : frankfurt;
       F997 : duesseldorf;
       F1043 : hannover;
       F1046 : hannover;
       F1055 : frankfurt;
       F1070 : berlin;
       F1096 : nuernberg;
       F1154 : hamburg;
       F1211 : bremen;
       F1377 : koeln_bonn;
       F1469 : bremen
end 
Individual geneve in city  with
 con_to
       F56 : frankfurt;
       F883 : duesseldorf;
       F898 : muenchen;
       F908 : frankfurt;
       F1243 : frankfurt;
       F1366 : frankfurt
end 
Individual koeln_bonn in city  with
 con_to
       F9 : wien;
       F58 : berlin;
       F111 : berlin;
       F143 : london;
       F175 : london;
       F203 : nuernberg;
       F280 : stuttgart;
       F289 : frankfurt;
       F330 : nuernberg;
       F359 : muenchen;
       F372 : hamburg;
       F426 : hamburg;
       F446 : hannover;
       F470 : muenchen;
       F487 : hamburg;
       F493 : hamburg;
       F496 : wien;
       F515 : hamburg;
       F519 : hannover;
       F520 : london;
       F537 : london;
       F599 : hannover;
       F610 : berlin;
       F616 : muenchen;
       F682 : hannover;
       F694 : frankfurt;
       F701 : muenchen;
       F702 : hamburg;
       F704 : paris;
       F733 : frankfurt;
       F747 : muenchen;
       F748 : frankfurt;
       F759 : zuerich;
       F764 : muenchen;
       F768 : nuernberg;
       F770 : muenchen;
       F791 : nuernberg;
       F832 : paris;
       F849 : hannover;
       F867 : madrid;
       F887 : paris;
       F904 : stuttgart;
       F906 : milano;
       F910 : hannover;
       F989 : muenchen;
       F993 : stuttgart;
       F994 : milano;
       F1027 : warszawa;
       F1092 : berlin;
       F1098 : hamburg;
       F1165 : muenchen;
       F1181 : muenchen;
       F1242 : warszawa;
       F1307 : berlin;
       F1336 : muenchen;
       F1348 : berlin;
       F1356 : berlin;
       F1429 : zuerich
end 
Individual wien in city  with
 con_to
       F55 : duesseldorf;
       F127 : frankfurt;
       F231 : hannover;
       F302 : nuernberg;
       F311 : frankfurt;
       F480 : hannover;
       F597 : stuttgart;
       F601 : hamburg;
       F737 : koeln_bonn;
       F1067 : frankfurt;
       F1116 : koeln_bonn;
       F1204 : frankfurt;
       F1255 : duesseldorf;
       F1266 : muenchen;
       F1276 : stuttgart;
       F1281 : muenchen;
       F1284 : muenchen;
       F1362 : frankfurt;
       F1393 : muenchen;
       F1412 : duesseldorf;
       F1441 : frankfurt
end 
Individual duesseldorf in city  with
 con_to
       F139 : muenchen;
       F258 : muenchen;
       F261 : roma;
       F265 : berlin;
       F283 : berlin;
       F318 : frankfurt;
       F336 : frankfurt;
       F352 : muenchen;
       F354 : muenchen;
       F362 : london;
       F369 : hamburg;
       F375 : muenchen;
       F392 : london;
       F397 : frankfurt;
       F398 : muenchen;
       F420 : berlin;
       F421 : muenchen;
       F423 : london;
       F449 : frankfurt;
       F452 : hamburg;
       F492 : london;
       F502 : roma;
       F504 : hamburg;
       F507 : hamburg;
       F531 : hamburg;
       F536 : berlin;
       F555 : berlin;
       F581 : berlin;
       F589 : muenchen;
       F600 : muenchen;
       F607 : berlin;
       F617 : berlin;
       F624 : london;
       F662 : london;
       F675 : london;
       F684 : muenchen;
       F688 : hamburg;
       F714 : hamburg;
       F767 : muenchen;
       F780 : muenchen;
       F800 : frankfurt;
       F890 : berlin;
       F909 : helsinki;
       F987 : hamburg;
       F1097 : muenchen;
       F1103 : berlin;
       F1112 : hamburg;
       F1124 : muenchen;
       F1201 : moskva;
       F1258 : frankfurt;
       F1288 : frankfurt;
       F1296 : moskva;
       F1323 : berlin;
       F1349 : frankfurt;
       F1384 : frankfurt;
       F1385 : hamburg;
       F1396 : frankfurt;
       F1425 : frankfurt;
       F1444 : leningrad;
       F1472 : hamburg
end 
Individual berlin in city  with
 con_to
       F19 : hannover;
       F22 : stuttgart;
       F39 : koeln_bonn;
       F57 : koeln_bonn;
       F60 : hamburg;
       F62 : muenchen;
       F72 : bremen;
       F78 : hannover;
       F87 : manchester;
       F102 : saarbruecken;
       F106 : saarbruecken;
       F113 : frankfurt;
       F116 : hannover;
       F129 : frankfurt;
       F135 : frankfurt;
       F153 : hannover;
       F173 : frankfurt;
       F186 : stuttgart;
       F204 : muenchen;
       F217 : muenchen;
       F223 : duesseldorf;
       F227 : muenchen;
       F235 : frankfurt;
       F241 : frankfurt;
       F242 : frankfurt;
       F243 : hamburg;
       F259 : bremen;
       F262 : stuttgart;
       F270 : bremen;
       F285 : duesseldorf;
       F292 : saarbruecken;
       F295 : bremen;
       F304 : duesseldorf;
       F305 : muenster;
       F306 : muenster;
       F316 : duesseldorf;
       F338 : muenster;
       F407 : duesseldorf;
       F422 : duesseldorf;
       F438 : hannover;
       F460 : frankfurt;
       F461 : paris;
       F482 : hannover;
       F485 : paris;
       F498 : stuttgart;
       F501 : stuttgart;
       F556 : hannover;
       F570 : duesseldorf;
       F586 : duesseldorf;
       F591 : manchester;
       F634 : bremen;
       F665 : koeln_bonn;
       F689 : london;
       F712 : muenchen;
       F727 : bremen;
       F789 : stuttgart;
       F804 : london;
       F881 : frankfurt;
       F922 : duesseldorf;
       F927 : london;
       F959 : stuttgart;
       F979 : muenchen;
       F1016 : nice;
       F1036 : koeln_bonn;
       F1132 : saarbruecken;
       F1152 : frankfurt;
       F1163 : koeln_bonn;
       F1182 : stuttgart;
       F1183 : hamburg;
       F1210 : hamburg;
       F1225 : hamburg;
       F1269 : koeln_bonn;
       F1295 : hamburg;
       F1308 : duesseldorf;
       F1322 : muenster;
       F1333 : nuernberg;
       F1340 : duesseldorf;
       F1342 : hamburg;
       F1343 : nuernberg;
       F1353 : koeln_bonn;
       F1370 : paris;
       F1380 : hamburg;
       F1390 : hamburg;
       F1391 : frankfurt;
       F1418 : muenchen;
       F1424 : hamburg;
       F1438 : frankfurt;
       F1443 : frankfurt;
       F1468 : frankfurt
end 
Individual nuernberg in city  with
 con_to
       F31 : london;
       F50 : berlin;
       F59 : duesseldorf;
       F123 : duesseldorf;
       F169 : frankfurt;
       F170 : duesseldorf;
       F189 : koeln_bonn;
       F294 : frankfurt;
       F351 : wien;
       F385 : frankfurt;
       F400 : hannover;
       F409 : frankfurt;
       F411 : frankfurt;
       F412 : newyork;
       F524 : hamburg;
       F596 : hamburg;
       F602 : duesseldorf;
       F629 : bremen;
       F677 : hamburg;
       F749 : hannover;
       F788 : zuerich;
       F879 : hamburg;
       F969 : bremen;
       F1011 : zuerich;
       F1031 : koeln_bonn;
       F1062 : hannover;
       F1075 : berlin;
       F1120 : muenchen;
       F1121 : koeln_bonn;
       F1129 : berlin;
       F1134 : hamburg;
       F1344 : paris;
       F1407 : duesseldorf
end 
Individual napoli in city  with
 con_to
       F15 : frankfurt;
       F569 : frankfurt
end 
Individual newyork in city  with
 con_to
       F16 : duesseldorf;
       F357 : frankfurt;
       F514 : hamburg;
       F572 : muenchen;
       F722 : muenchen;
       F1082 : frankfurt
end 
Individual juist in city  with
 con_to
       F17 : bremen;
       F244 : bremen
end 
Individual birmingham in city  with
 con_to
       F20 : frankfurt;
       F150 : duesseldorf;
       F163 : duesseldorf;
       F193 : duesseldorf;
       F393 : frankfurt;
       F533 : muenchen;
       F575 : frankfurt;
       F632 : muenchen;
       F643 : muenchen;
       F720 : duesseldorf;
       F891 : hannover;
       F1035 : frankfurt;
       F1235 : duesseldorf
end 
Individual nice in city  with
 con_to
       F21 : berlin;
       F218 : stuttgart;
       F299 : muenchen;
       F300 : muenchen;
       F484 : duesseldorf;
       F505 : duesseldorf;
       F532 : duesseldorf;
       F554 : duesseldorf;
       F639 : hamburg;
       F1389 : frankfurt;
       F1460 : hamburg
end 
Individual london in city  with
 con_to
       F94 : muenchen;
       F200 : frankfurt;
       F214 : frankfurt;
       F277 : bremen;
       F288 : duesseldorf;
       F293 : duesseldorf;
       F325 : duesseldorf;
       F327 : duesseldorf;
       F331 : frankfurt;
       F360 : frankfurt;
       F419 : muenchen;
       F510 : hamburg;
       F560 : hamburg;
       F620 : berlin;
       F635 : berlin;
       F660 : frankfurt;
       F691 : bremen;
       F777 : frankfurt;
       F787 : duesseldorf;
       F793 : muenchen;
       F842 : frankfurt;
       F984 : hamburg;
       F1008 : frankfurt;
       F1049 : hamburg;
       F1087 : duesseldorf;
       F1147 : hamburg;
       F1188 : berlin;
       F1293 : duesseldorf;
       F1369 : muenchen;
       F1428 : muenchen
end 
Individual edinburgh in city 
end 
Individual sofia in city  with
 con_to
       F28 : frankfurt;
       F255 : muenchen;
       F801 : frankfurt
end 
Individual helsinki in city  with
 con_to
       F29 : hamburg;
       F112 : hannover;
       F156 : hannover;
       F754 : duesseldorf;
       F882 : hamburg;
       F1100 : frankfurt;
       F1184 : frankfurt;
       F1277 : frankfurt
end 
Individual roma in city  with
 con_to
       F33 : muenchen;
       F154 : duesseldorf;
       F309 : duesseldorf;
       F543 : frankfurt;
       F583 : frankfurt;
       F1038 : frankfurt;
       F1048 : frankfurt;
       F1072 : muenchen;
       F1079 : muenchen
end 
Individual glasgow in city  with
 con_to
       F71 : muenchen;
       F345 : duesseldorf;
       F550 : frankfurt;
       F565 : duesseldorf;
       F667 : muenster;
       F794 : frankfurt;
       F870 : berlin;
       F1109 : duesseldorf;
       F1146 : berlin;
       F1228 : duesseldorf
end 
Individual saarbruecken in city  with
 con_to
       F633 : frankfurt;
       F683 : berlin;
       F685 : berlin;
       F802 : berlin;
       F948 : duesseldorf;
       F1196 : duesseldorf;
       F1368 : frankfurt;
       F1395 : frankfurt;
       F1405 : frankfurt;
       F1465 : frankfurt
end 
Individual milano in city  with
 con_to
       F38 : frankfurt;
       F40 : frankfurt;
       F89 : frankfurt;
       F110 : koeln_bonn;
       F381 : hannover;
       F387 : koeln_bonn;
       F427 : frankfurt;
       F429 : muenchen;
       F431 : hamburg;
       F456 : duesseldorf;
       F540 : hamburg;
       F914 : muenchen;
       F1029 : frankfurt;
       F1060 : stuttgart;
       F1138 : muenchen;
       F1232 : duesseldorf;
       F1245 : frankfurt;
       F1267 : duesseldorf;
       F1372 : stuttgart;
       F1422 : duesseldorf
end 
Individual oslo in city  with
 con_to
       F42 : duesseldorf;
       F63 : duesseldorf;
       F208 : muenchen;
       F228 : muenchen;
       F486 : hamburg;
       F686 : frankfurt;
       F692 : frankfurt;
       F775 : frankfurt;
       F824 : frankfurt;
       F1186 : muenchen;
       F1202 : stuttgart;
       F1205 : muenchen;
       F1215 : hannover;
       F1249 : hannover
end 
Individual bucuresti in city  with
 con_to
       F43 : muenchen;
       F263 : frankfurt;
       F366 : frankfurt;
       F760 : frankfurt
end 
Individual dublin in city  with
 con_to
       F45 : frankfurt;
       F74 : frankfurt;
       F126 : duesseldorf;
       F134 : duesseldorf;
       F236 : frankfurt
end 
Individual zagreb in city  with
 con_to
       F51 : frankfurt;
       F76 : muenchen;
       F705 : muenchen;
       F848 : frankfurt;
       F1130 : frankfurt;
       F1175 : frankfurt
end 
Individual bruxelles in city  with
 con_to
       F52 : frankfurt;
       F165 : stuttgart;
       F458 : stuttgart;
       F473 : duesseldorf;
       F615 : hannover;
       F743 : stuttgart;
       F859 : stuttgart;
       F886 : duesseldorf;
       F892 : duesseldorf;
       F903 : duesseldorf;
       F905 : hamburg;
       F952 : hamburg;
       F961 : duesseldorf;
       F976 : frankfurt;
       F1145 : muenchen;
       F1164 : frankfurt;
       F1190 : muenchen;
       F1292 : duesseldorf;
       F1402 : frankfurt
end 
Individual ljubljana in city  with
 con_to
       F53 : frankfurt;
       F138 : muenchen;
       F1151 : muenchen
end 
Individual lapaz in city  with
 con_to
       F64 : frankfurt;
       F337 : sanjuan
end 
Individual malaga in city  with
 con_to
       F75 : duesseldorf;
       F84 : duesseldorf;
       F625 : frankfurt;
       F745 : hamburg;
       F746 : hamburg
end 
Individual bogota in city  with
 con_to
       F81 : sanjuan;
       F1128 : frankfurt;
       F1192 : frankfurt;
       F1318 : sanjuan;
       F1350 : frankfurt
end 
Individual sanjuan in city  with
 con_to
       F414 : frankfurt;
       F893 : frankfurt;
       F926 : frankfurt;
       F1239 : bogota;
       F1411 : bogota
end 
Individual luxembourg in city  with
 con_to
       F83 : frankfurt;
       F164 : frankfurt;
       F592 : frankfurt;
       F744 : frankfurt;
       F917 : frankfurt;
       F935 : frankfurt
end 
Individual faro in city  with
 con_to
       F564 : frankfurt;
       F833 : frankfurt;
       F977 : frankfurt;
       F1231 : frankfurt
end 
Individual manchester in city  with
 con_to
       F107 : duesseldorf;
       F159 : berlin;
       F266 : frankfurt;
       F636 : muenster;
       F752 : frankfurt;
       F823 : duesseldorf;
       F855 : berlin;
       F973 : muenchen;
       F1051 : duesseldorf;
       F1159 : muenchen;
       F1207 : muenchen
end 
Individual budapest in city  with
 con_to
       F88 : frankfurt;
       F661 : frankfurt;
       F721 : duesseldorf;
       F1166 : muenchen;
       F1203 : muenchen
end 
Individual tanger in city  with
 con_to
       F93 : frankfurt;
       F1076 : frankfurt
end 
Individual amsterdam in city  with
 con_to
       F109 : frankfurt;
       F145 : frankfurt;
       F307 : frankfurt;
       F340 : hannover;
       F353 : muenchen;
       F923 : muenchen;
       F1002 : hannover;
       F1194 : hannover;
       F1214 : hannover;
       F1221 : frankfurt;
       F1262 : hamburg
end 
Individual moskva in city  with
 con_to
       F99 : muenchen;
       F222 : duesseldorf;
       F252 : frankfurt;
       F323 : frankfurt;
       F774 : duesseldorf;
       F1236 : muenchen;
       F1259 : frankfurt;
       F1448 : frankfurt
end 
Individual istanbul in city  with
 con_to
       F115 : frankfurt;
       F568 : muenchen;
       F803 : duesseldorf
end 
Individual antwerpen in city  with
 con_to
       F693 : frankfurt;
       F758 : frankfurt;
       F763 : frankfurt
end 
Individual salzburg in city  with
 con_to
       F179 : frankfurt;
       F403 : frankfurt;
       F765 : frankfurt
end 
Individual zuerich in city  with
 con_to
       F122 : hannover;
       F155 : frankfurt;
       F177 : hannover;
       F180 : hannover;
       F192 : nuernberg;
       F282 : muenchen;
       F404 : duesseldorf;
       F405 : hamburg;
       F489 : frankfurt;
       F490 : hamburg;
       F494 : frankfurt;
       F506 : muenchen;
       F558 : frankfurt;
       F588 : hamburg;
       F659 : stuttgart;
       F700 : stuttgart;
       F751 : duesseldorf;
       F865 : stuttgart;
       F930 : hannover;
       F943 : frankfurt;
       F974 : koeln_bonn;
       F995 : hannover;
       F1034 : muenchen;
       F1042 : nuernberg;
       F1061 : muenchen;
       F1074 : frankfurt;
       F1113 : muenchen;
       F1251 : duesseldorf;
       F1252 : duesseldorf;
       F1363 : koeln_bonn
end 
Individual laspalmas in city  with
 con_to
       F124 : duesseldorf;
       F418 : frankfurt;
       F561 : duesseldorf;
       F1106 : frankfurt
end 
Individual palma in city  with
 con_to
       F132 : frankfurt;
       F739 : duesseldorf;
       F1334 : hamburg
end 
Individual athine in city  with
 con_to
       F136 : stuttgart;
       F152 : frankfurt;
       F206 : muenchen;
       F216 : muenchen;
       F220 : hamburg;
       F363 : frankfurt;
       F611 : hamburg;
       F854 : frankfurt;
       F1271 : frankfurt;
       F1371 : hamburg;
       F1406 : hamburg
end 
Individual thessaloniki in city  with
 con_to
       F140 : muenchen
end 
Individual dortmund in city  with
 con_to
       F142 : muenchen;
       F160 : muenchen;
       F251 : muenchen;
       F424 : muenchen;
       F942 : muenchen;
       F986 : frankfurt;
       F1024 : stuttgart;
       F1041 : stuttgart;
       F1321 : frankfurt
end 
Individual torino in city  with
 con_to
       F1021 : stuttgart;
       F1213 : frankfurt;
       F1451 : frankfurt
end 
Individual beograd in city  with
 con_to
       F157 : frankfurt;
       F425 : muenchen;
       F430 : duesseldorf;
       F806 : muenchen;
       F1415 : frankfurt
end 
Individual stockholm in city  with
 con_to
       F158 : hamburg;
       F201 : hamburg;
       F435 : duesseldorf;
       F450 : frankfurt;
       F512 : frankfurt;
       F541 : frankfurt;
       F557 : frankfurt;
       F598 : hannover;
       F631 : hannover;
       F753 : frankfurt;
       F991 : muenchen;
       F1289 : duesseldorf;
       F1310 : hamburg
end 
Individual catania in city  with
 con_to
       F740 : frankfurt
end 
Individual leipzig in city  with
 con_to
       F256 : hamburg;
       F934 : muenchen;
       F963 : muenchen;
       F1080 : duesseldorf;
       F1306 : frankfurt;
       F1417 : frankfurt
end 
Individual casablanca in city  with
 con_to
       F166 : frankfurt;
       F167 : frankfurt;
       F996 : frankfurt;
       F1294 : muenchen
end 
Individual innsbruck in city  with
 con_to
       F171 : frankfurt;
       F1017 : duesseldorf;
       F1057 : frankfurt
end 
Individual porto in city  with
 con_to
       F442 : frankfurt;
       F710 : frankfurt;
       F1290 : frankfurt
end 
Individual ankara in city  with
 con_to
       F188 : muenchen
end 
Individual valencia in city  with
 con_to
       F237 : frankfurt
end 
Individual basel in city  with
 con_to
       F221 : muenchen;
       F350 : duesseldorf;
       F455 : frankfurt;
       F474 : muenchen;
       F528 : duesseldorf;
       F878 : frankfurt;
       F918 : muenchen;
       F1170 : frankfurt
end 
Individual madrid in city  with
 con_to
       F225 : muenchen;
       F388 : hamburg;
       F401 : duesseldorf;
       F1037 : frankfurt;
       F1078 : muenchen;
       F1085 : koeln_bonn;
       F1185 : duesseldorf;
       F1187 : duesseldorf;
       F1191 : duesseldorf;
       F1238 : stuttgart;
       F1455 : frankfurt;
       F1467 : frankfurt
end 
Individual sevilla in city  with
 con_to
       F707 : frankfurt
end 
Individual langeoog in city  with
 con_to
       F245 : bremen;
       F383 : bremen
end 
Individual hof in city  with
 con_to
       F248 : frankfurt;
       F792 : bayreuth;
       F1445 : frankfurt;
       F1447 : frankfurt;
       F1470 : frankfurt
end 
Individual split in city  with
 con_to
       F779 : frankfurt;
       F790 : muenchen
end 
Individual izmir in city  with
 con_to
       F257 : frankfurt
end 
Individual lisboa in city  with
 con_to
       F269 : frankfurt;
       F817 : frankfurt;
       F864 : frankfurt;
       F1209 : frankfurt
end 
Individual barcelona in city  with
 con_to
       F297 : frankfurt;
       F1197 : frankfurt;
       F1199 : frankfurt;
       F1241 : muenchen;
       F1337 : hamburg;
       F1416 : duesseldorf
end 
Individual muenster in city  with
 con_to
       F343 : frankfurt;
       F483 : frankfurt;
       F522 : berlin;
       F548 : berlin;
       F551 : berlin;
       F580 : frankfurt;
       F853 : berlin;
       F957 : london;
       F970 : manchester;
       F1020 : frankfurt;
       F1064 : muenchen;
       F1095 : muenchen;
       F1222 : muenchen;
       F1398 : glasgow
end 
Individual tunis in city  with
 con_to
       F312 : duesseldorf;
       F671 : muenchen;
       F1025 : frankfurt;
       F1044 : frankfurt;
       F1212 : frankfurt
end 
Individual goeteborg in city  with
 con_to
       F447 : frankfurt;
       F513 : frankfurt;
       F944 : hamburg;
       F1200 : duesseldorf
end 
Individual chicago in city  with
 con_to
       F315 : frankfurt
end 
Individual tenerife in city  with
 con_to
       F394 : duesseldorf;
       F901 : frankfurt
end 
Individual paderborn in city  with
 con_to
       F376 : muenchen;
       F681 : muenchen;
       F950 : muenchen;
       F966 : muenchen;
       F1220 : frankfurt;
       F1324 : frankfurt
end 
Individual santa_decompostela in city  with
 con_to
       F384 : frankfurt
end 
Individual helgoland in city  with
 con_to
       F395 : hamburg;
       F825 : bremen;
       F862 : hamburg;
       F1123 : bremen;
       F1464 : hamburg
end 
Individual rotterdam in city  with
 con_to
       F439 : hamburg;
       F516 : hamburg;
       F736 : hamburg;
       F931 : hamburg
end 
Individual tripoli in city 
end 
Individual tampere in city  with
 con_to
       F521 : hamburg;
       F648 : frankfurt
end 
Individual bristol in city  with
 con_to
       F525 : duesseldorf
end 
Individual dallas in city  with
 con_to
       F547 : atlanta;
       F1254 : frankfurt;
       F1461 : frankfurt
end 
Individual atlanta in city  with
 con_to
       F819 : dallas;
       F1141 : frankfurt;
       F1167 : frankfurt
end 
Individual praha in city  with
 con_to
       F549 : frankfurt;
       F1161 : frankfurt
end 
Individual marseille in city  with
 con_to
       F552 : frankfurt
end 
Individual lyon in city  with
 con_to
       F562 : frankfurt;
       F1383 : frankfurt
end 
Individual venezia in city  with
 con_to
       F618 : muenchen;
       F619 : frankfurt;
       F1105 : duesseldorf;
       F1319 : frankfurt
end 
Individual warszawa in city  with
 con_to
       F567 : frankfurt;
       F673 : frankfurt;
       F967 : koeln_bonn;
       F1302 : koeln_bonn;
       F1431 : hamburg
end 
Individual gdansk in city  with
 con_to
       F578 : hamburg
end 
Individual graz in city  with
 con_to
       F978 : frankfurt;
       F1160 : frankfurt
end 
Individual malmoe in city  with
 con_to
       F627 : hamburg;
       F703 : hamburg
end 
Individual wangerooge in city  with
 con_to
       F641 : bremen;
       F783 : bremen
end 
Individual linz in city  with
 con_to
       F1408 : frankfurt;
       F1423 : frankfurt
end 
Individual ibiza in city  with
 con_to
       F1298 : frankfurt;
       F1347 : frankfurt
end 
Individual norderney in city  with
 con_to
       F719 : bremen;
       F1345 : bremen
end 
Individual malta in city with
  con_to
       F88912: tripoli
end 
Individual timisoara in city  with
 con_to
       F772 : frankfurt
end 
Individual bayreuth in city  with
 con_to
       F785 : hof;
       F809 : hof;
       F813 : frankfurt;
       F828 : frankfurt;
       F829 : frankfurt;
       F1045 : frankfurt;
       F1253 : hof
end 
Individual bologna in city  with
 con_to
       F955 : frankfurt;
       F1263 : frankfurt
end 
Individual alger in city  with
 con_to
       F1137 : frankfurt
end 
Individual klagenfurt in city  with
 con_to
       F841 : frankfurt;
       F1065 : muenchen
end 
Individual genova in city  with
 con_to
       F1102 : frankfurt
end 
Individual bordeaux in city  with
 con_to
       F1375 : frankfurt
end 
Individual pisa in city  with
 con_to
       F897 : frankfurt;
       F1193 : frankfurt
end 
Individual heraklion in city  with
 con_to
       F958 : frankfurt
end 
Individual bilbao in city  with
 con_to
       F1177 : frankfurt
end 
Individual dubrovnik in city  with
 con_to
       F1125 : frankfurt
end 
Individual sanfrancisco in city 
end 
Individual westerland in city  with
 con_to
       F1053 : hamburg
end 
Individual borkum in city  with
 con_to
       F1180 : bremen;
       F1257 : bremen
end 
Individual toulouse in city 
end 



{* The recursive rule and query stuff *}

Class city with
  attribute
    trans_con_to: city
  rule
    r1: $ forall c1,c2/city (c1 con_to c2) ==> (c1 trans_con_to c2) $;
    r2: $ forall c1,c2/city
             (exists c/city (c1 con_to c) and (c trans_con_to c2))
          ==> (c1 trans_con_to c2) $
end


QueryClass DeadEndCity isA city with
  constraint
     c1: $ (forall n/city (~this con_to n)
            ==> (n in DeadEndCity)) $
end

QueryClass DeadStartCity isA city with
  constraint
     c1: $ (forall n/city (n con_to ~this)
            ==> (n in DeadStartCity)) $
end


GenericQueryClass KindaClique isA city with
  parameter
    startcity: city
  constraint
    c: $ (~this = ~startcity) or 
         (exists c/city (c in KindaClique[~startcity/startcity]) and (c con_to ~this)) $
end

{* this one takes about 30 sec with a parameter*}
GenericQueryClass ReachableFrom isA city with
  parameter,computed_attribute
    start: city
  constraint
    c: $ (~start trans_con_to ~this) $
end

{* this one takes about 400 sec when called without parameter *}
GenericQueryClass UnReachableFrom isA city with
  parameter,computed_attribute
    start: city
  constraint
    c: $ not (~start trans_con_to ~this) $
end


{* this one really takes about 500 sec and returns nil *}
QueryClass GoodStartCity isA city with 
  attribute,constraint
     c1 : $ forall c/city (c in ReachableFrom[~this/start]) $
end 



	
