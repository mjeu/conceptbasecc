{*
The ConceptBase.cc Copyright

Copyright 1987-2024 The ConceptBase Team. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of
      conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE CONCEPTBASE TEAM ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE CONCEPTBASE TEAM OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the authors
and should not be interpreted as representing official policies, either expressed or implied,
of the ConceptBase Team.


The ConceptBase Team is represented by

Manfred Jeusfeld, University of Skovde, 54128 Skovde, Sweden


This license is a FreeBSD-style copyright license.
Legal home of the FreeBSD copyright license: http://www.freebsd.org/copyright/freebsd-license.html
*}
{$set syntax=PlainAachen}

Individual leningrad in city 
end 
Individual frankfurt in city 
end 
Individual kobenhavn in city 
end 
Individual hamburg in city 
end 
Individual bremen in city 
end 
Individual muenchen in city 
end 
Individual stuttgart in city 
end 
Individual hannover in city 
end 
Individual paris in city 
end 
Individual geneve in city 
end 
Individual koeln_bonn in city 
end 
Individual wien in city 
end 
Individual duesseldorf in city 
end 
Individual berlin in city 
end 
Individual nuernberg in city 
end 
Individual napoli in city 
end 
Individual newyork in city 
end 
Individual juist in city 
end 
Individual birmingham in city 
end 
Individual nice in city 
end 
Individual london in city 
end 
Individual edinburgh in city 
end 
Individual sofia in city 
end 
Individual helsinki in city 
end 
Individual roma in city 
end 
Individual glasgow in city 
end 
Individual saarbruecken in city 
end 
Individual milano in city 
end 
Individual oslo in city 
end 
Individual bucuresti in city 
end 
Individual dublin in city 
end 
Individual zagreb in city 
end 
Individual bruxelles in city 
end 
Individual ljubljana in city 
end 
Individual lapaz in city 
end 
Individual malaga in city 
end 
Individual bogota in city 
end 
Individual sanjuan in city 
end 
Individual luxembourg in city 
end 
Individual faro in city 
end 
Individual manchester in city 
end 
Individual budapest in city 
end 
Individual tanger in city 
end 
Individual amsterdam in city 
end 
Individual moskva in city 
end 
Individual istanbul in city 
end 
Individual antwerpen in city 
end 
Individual salzburg in city 
end 
Individual zuerich in city 
end 
Individual laspalmas in city 
end 
Individual palma in city 
end 
Individual athine in city 
end 
Individual thessaloniki in city 
end 
Individual dortmund in city 
end 
Individual torino in city 
end 
Individual beograd in city 
end 
Individual stockholm in city 
end 
Individual catania in city 
end 
Individual leipzig in city 
end 
Individual casablanca in city 
end 
Individual innsbruck in city 
end 
Individual porto in city 
end 
Individual ankara in city 
end 
Individual valencia in city 
end 
Individual basel in city 
end 
Individual madrid in city 
end 
Individual sevilla in city 
end 
Individual langeoog in city 
end 
Individual hof in city 
end 
Individual split in city 
end 
Individual izmir in city 
end 
Individual lisboa in city 
end 
Individual barcelona in city 
end 
Individual muenster in city 
end 
Individual tunis in city 
end 
Individual goeteborg in city 
end 
Individual chicago in city 
end 
Individual tenerife in city 
end 
Individual paderborn in city 
end 
Individual santa_decompostela in city 
end 
Individual helgoland in city 
end 
Individual rotterdam in city 
end 
Individual tripoli in city 
end 
Individual tampere in city 
end 
Individual bristol in city 
end 
Individual dallas in city 
end 
Individual atlanta in city 
end 
Individual praha in city 
end 
Individual marseille in city 
end 
Individual lyon in city 
end 
Individual venezia in city 
end 
Individual warszawa in city 
end 
Individual gdansk in city 
end 
Individual graz in city 
end 
Individual malmoe in city 
end 
Individual wangerooge in city 
end 
Individual linz in city 
end 
Individual ibiza in city 
end 
Individual norderney in city 
end 
Individual malta in city 
end 
Individual timisoara in city 
end 
Individual bayreuth in city 
end 
Individual bologna in city 
end 
Individual alger in city 
end 
Individual klagenfurt in city 
end 
Individual genova in city 
end 
Individual bordeaux in city 
end 
Individual pisa in city 
end 
Individual heraklion in city 
end 
Individual bilbao in city 
end 
Individual dubrovnik in city 
end 
Individual sanfrancisco in city 
end 
Individual westerland in city 
end 
Individual borkum in city 
end 
Individual toulouse in city 
end 
