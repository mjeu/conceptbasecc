This directory contains examples on how
to use the Java API of ConceptBase.

The ExampleParser program connects to a server, retrieves
an object, parses the objects and prints each component of
it on the standard output.
Furthermore, an object is constructed on the client side 
and then told to the ConceptBase server. 

The ExampleOBI program uses the ObjectBaseInterface API
to access ConceptBase and retrieve information 
about objects.

You need JDK 7 or later to run this example.


PREPARATION
===========

** Create a copy called JavaClient of this directory at some other location
on your computer. Then, switch to the new directory:
  cd JavaClient



COMPILING
=========

Assuming that $CB_HOME is the ConceptBase installation directory on your
local computer, compile the example Java programs by:

   Linux: javac -classpath $CB_HOME/lib/classes/cb.jar ExampleParser.java ExampleOBI.java SimpleClient.java
   Windows: javac -classpath $CB_HOME/lib/classes/cb.jar ExampleParser.java ExampleOBI.java SimpleClient.java



RUNNING
=======

** Preparation: Make sure that a CBserver is running
   Start a ConceptBase server on your local computer (localhost) with
   port number 4001 or use the public CBserver.
   If you want to connect to a ConceptBase server on
   a remote host, then you have to replace in the Java programs ExampleParser.java
   and/or ExampleOBI.java the string "localhost" by the
   full host name of the remote host, e.g. "myhost.div-a.mydomain.org"


** Testing SimpleClient
   Linux: java -classpath $CB_HOME/lib/classes/cb.jar:. SimpleClient
   Windows: java -classpath $CB_HOME/lib/classes/cb.jar;. SimpleClient
   The simple client uses the string-based API to the ConceptBase server.
   This is much easier to work with than the full API. We recommend to
   use this style of interacting with the CBserver.



(older APIs; still working)
** Testing ExampleParser
   Linux: java -classpath $CB_HOME/lib/classes/cb.jar:. ExampleParser XY
   Windows: java -classpath $CB_HOME/lib/classes/cb.jar;. ExampleParser XY
   where XY is the name of some ConceptBase object, e.g. Class

** Testing ExampleOBI
   Linux: java -classpath $CB_HOME/lib/classes/cb.jar:. ExampleOBI
   Windows: java -classpath $CB_HOME/lib/classes/cb.jar;. ExampleOBI



