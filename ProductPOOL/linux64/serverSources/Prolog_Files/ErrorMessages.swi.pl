/**
The ConceptBase.cc Copyright

Copyright 1987-2025 The ConceptBase Team. All rights reserved.

Redistribution and use in source and binary forms, with or without modification, are permitted
provided that the following conditions are met:

   1. Redistributions of source code must retain the above copyright notice, this list of
      conditions and the following disclaimer.
   2. Redistributions in binary form must reproduce the above copyright notice, this list of
      conditions and the following disclaimer in the documentation and/or other materials
      provided with the distribution.

THIS SOFTWARE IS PROVIDED BY THE CONCEPTBASE TEAM ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES,
INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE CONCEPTBASE TEAM OR CONTRIBUTORS BE
LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
(INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,
OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

The views and conclusions contained in the software and documentation are those of the authors
and should not be interpreted as representing official policies, either expressed or implied,
of the ConceptBase Team.


The ConceptBase Team is represented by

Manfred Jeusfeld, University of Skovde, 54128 Skovde, Sweden


This license is a FreeBSD-style copyright license.
Legal home of the FreeBSD copyright license: http://www.freebsd.org/copyright/freebsd-license.html
**/
/*
*
* File:         ErrorMessages.pro
* Version:      11.3
* Last Change   : 19-Sep-2002, Manfred Jeusfeld (Tilburg University)
*
* SCCS-Source-Pool : /home/CBase/CB_NewStruct/ProductPOOL/SCCS/serverSources/Prolog_Files/s.ErrorMessages.pro
* Date retrieved : 97/07/09 (YY/MM/DD)
*
* -----------------------------------------------------
*
* This module contains all error messages of ConceptBase. See also
* module GlobalParameters for some settings of ErrorMessages.
*
*
*
* Exported predicates:
* --------------------
*
*   + report_error/3
*       Reports the error arg1 in the module arg2 by displaying an
*       error messages enriched by the information of arg3.
*
*   04-Aug-1989 MSt : adaptions for new CB architecture
*   24-Aug-1989 MSt : CB_terminalModuls no longer imported from GlobalParameters
*		      display_error/2 simplified : only sending of a message
*                     with method ERROR_REPORT
*
*   12-Mar-1990 HWN : strings are now lists of characters; bimstrings are
*		      lists of ASCII numbers
*
* 24-Feb-93/kvt: neue Fehlermeldung bei Verletzen der referentiellen Integritaet. cf. ErrorsCorrected.doc[5]
*
* Metaformel Aenderungen (10.1.96):
* neue Fehlermeldungen fuer
* NO_MAGIC_META,INSERT_AND_DELETE_REQUEST,MSERR0,...,MSERR4,TGF1,HMF1
*
* 9-Dez-1996 LWEB:
* neue Fehlermeldungen SEXPR3, SEXPR4, SIERR, ECA0 ... ECA7, MOD1, MOD2,
*SET_MODULE_ERROR.
* Fehlermeldungen werden mit dem Modulkontext ausgegeben, in dem sie aufgetreten sind
*
*/


:- module('ErrorMessages',[
'encodeAtom'/2
,'handle_error_message_queue'/1
,'report_error'/3
]).
:- use_module('GlobalPredicates.swi.pl').
:- use_module('debug.swi.pl').

:- use_module('GeneralUtilities.swi.pl').







:- use_module('CBserverInterface.swi.pl').


:- use_module('ScanFormatUtilities.swi.pl').

:- use_module('SearchSpace.swi.pl').

:- use_module('ExternalCodeLoader.swi.pl').

:- use_module('PrologCompatibility.swi.pl').














:- dynamic 'error_queue'/1 .


:- style_check(-singleton).





/** The following settings were originally imported from GlobalParameters */
/** Now, they are defined locally, because nobody else needs them.        */
/**  25-Apr-1991/MJf                                                      */

cb_errorWidth(40).       /*width of an error message line*/
cb_errorTab(2).          /*indentation of the error message long text*/
paramDelimiter('').      /*marking of parameters*/




/* ==================== */
/* Exported predicates: */
/* ==================== */




/* ********************   r e p o r t _ e r r o r   *********************** */
/*                                                                          */
/* report_error( _error_abbreviation, _error_module, _error_parameterlist)  */
/*                                                                          */
/* 	_error_abbreviation:	atom	(input),                           */
/*       	name of the error message that should be displayed,        */
/* 	_error_module:		ground	(input),                           */
/*		name of the module where the error appeared,               */
/*       _error_parameterlist:	term	(input),                           */
/*		list of parameters that should be inserted in the error    */
/*               message.                                                   */
/*                                                                          */
/* This procedure fetches the text of the error message with the name       */
/* _error_abbreviation, inserts in it the parameters of _error_parameterlist*/
/* where possible and displays the text together with a short message on an */
/* ascii terminal or in a window, depending on the actual modus.            */
/* report_error succeeds always, no backtracking.                           */
/*                                                                          */
/* ************************************************************************ */


report_error( _err_abbr, _err_module, _err_paramlist) :-

	atom( _err_abbr),
	ground( _err_module),
        	get_KBsearchSpace(_kb,_rbt),
	set_KBsearchSpace(newOB,'Now'),  /*damit alle Objektnamen gefunden werden*/
	structure_conv( _err_paramlist, _atomlist),
        set_KBsearchSpace(_kb,_rbt),   /*alte Setzungen*/
	cb_errorMessage( _err_abbr,_atomlist,_insertlist),
	!,
	'M_SearchSpace'(_mod),				/* 07-Aug-1995 LWEB */
        pc_atomconcat(_insertlist,_errorcom),
                         /*anstatt dem frueheren carriage_control*/
	id2name(_mod,_modname),
	pc_atomconcat( ['>>> Error ', _err_abbr, ' (', _err_module, ' on ',_modname,') '], _errorabbr),
	display_error( _errorabbr,_errorcom,error(_err_abbr,_err_module,_atomlist)),
	!.

/*21-Apr-1988/MJf: protecting 'report_error' from infinite loops*/

report_error( _err_abbr, _err_module, _err_paramlist) :-
   _err_abbr \== 'ERRERR',
   write('*** Error in ErrorMessages: illegal call'),nl,
   write(_err_abbr),nl,
   write(_err_module),nl,
   write(_err_paramlist),nl,
   !,
   report_error( 'ERRERR', 'ErrorMessages', []).

report_error(_,_,_).







/* =================== */
/* Private predicates: */
/* =================== */




/* ********************   s t r u c t u r e _ c o n v   ******************* */
/*                                                                          */
/* structure_conv( _structure_list, _atom_list)                             */
/*                                                                          */
/* 	_structure_list:    	list	(input),                           */
/*       	a list                                                     */
/* 	_atom_list:   		list of atoms	(output),                  */
/*		the same list but strings are now atoms and                */
/*               constructs are atoms consisting of <functor>(..).          */
/*                                                                          */
/* Auxiliary predicate:                                                     */
/*	str_conv( _structure (input), _atom (output))                      */
/*		converts only one element.                                 */
/*                                                                          */
/* ************************************************************************ */


structure_conv( [ _el | _rest], [ _new_el | _new_rest]) :-

	str_conv( _el, _new_el),
	structure_conv( _rest, _new_rest).

structure_conv( [], []) :- !.


str_conv(_string,_string):-                /*6-Aug-1992/MSt*/
	quotedAtom(_string),
        !.

str_conv(_bimstring,_atom) :-
	bimstring(_bimstring),
	'BimstringToString'(_bimstring,_string),
	atom2list(_atom,_string),
	!.


str_conv( _i, _atom) :-                   /*30-Aug-1988/MJf*/
	integer(_i),
	pc_inttoatom(_i,_atom),
	!.

str_conv( _r, _atom) :-                   /*30-Aug-1988/MJf*/
	float(_r),
	pc_floattoatom(_r,_atom),
	!.


/*object identifiers are converted to external object names in Telos syntax:*/

str_conv(telosStatement(_id), _oname) :-
        outTelosStatement(_id,_oname),
	!.

str_conv(objectName(_id), _oname) :-
        outObjectName(_id,_oname),
	!.

str_conv(formula(_f), _oname) :-
        outFormula(_f,_oname),
	!.

str_conv(uq(_s), _at) :-
        unquoteAtom(_s,_at),
	!.

str_conv( _atom, _atom) :-                  /*30-Jun-93/HWN*/
	atom(_atom),
	!.

str_conv( _term, _atom) :-                  /*2-Nov-1990/MJf*/
	pc_atom_to_term(_atom,_term),
	!.




/* *********************   d i s p l a y _ e r r o r   ******************** */
/*                                                        MSt 24-Aug-89     */
/* display_error(_errormsg1,_errormsg2)                                   */
/*                                                                          */
/*        a message with method ERROR_REPORT is send to                     */
/*	 sender ( stored in active_sender/1 ) from which the original      */
/*       TELL_OBJPROC or TELL_MODEL message was received.                   */
/*                                                                          */
/*	_errormsg1:		atom	(input),                           */
/*	_errormsg2:		atom	(input),                           */
/*                                                                          */
/* ************************************************************************ */

/** 29-Jun-2005/M.Jeusfeld: include functionality for the new parameter -e **/
/** (maximalErrors). It controls, how many error messages per transaction  **/
/** will be transmitted to the ConceptBase client.                         **/

display_error( _errormsg1, _errormsg2,_errorterm ):-
	active_sender(_s),
	thisToolId(_r),
/**	pc_atomconcat( [ _errormsg1, '\n', _errormsg2, '\n\n' ], _errormsg ), **/
	pc_atomconcat( [ _errormsg2, '\n\n' ], _errormsg ),  /** do not display part 1 (_errormsg1) **/
	pc_atom_to_term(_errtermatom,_errorterm),
	encodeAtom(_errormsg,_errormessage),
	encodeAtom(_errtermatom,_errtermmessage),
        getFlag(remainingErrorQueueSlots,_rn),   /** set in CBserverInterface by BEGIN_TRANSACTION **/
        bufferErrorMessage(_rn,_r,_s,_errormessage,_errtermmessage),
        decrementRemainingErrorQueueSlots(_rn),
	!.

display_error( _errormsg1, _errormsg2,_errorterm ) :-
	'WriteTrace'(minimal,'ErrorMessages',['ALERT! Strange problem while processing ',
                           display_error( _errormsg1, _errormsg2,_errorterm )]),
	!.

decrementRemainingErrorQueueSlots(zero) :- !.

decrementRemainingErrorQueueSlots(_rn) :-
       _rn > 0,
       _rn1 is _rn-1,
       setFlag(remainingErrorQueueSlots,_rn1),
       !.
decrementRemainingErrorQueueSlots(_).

/** no longer display error messages for the current transaction because all maximalErrors **/
/** have already been used                                                                 **/
bufferErrorMessage(zero,_,_,_,_) :- !.

bufferErrorMessage(_rn,_r,_s,_errormessage,_) :-
       error_queue(ipcmessage(_r,_s,'ERROR_REPORT',[_errormessage])),  /** message already queued **/
       !.  /** do nothing*/

/** we detect for the first time that we should buffer no more errors messages **/
bufferErrorMessage(0,_r,_s,_errormessage,_errtermmessage) :-
       'WriteTrace'(low,'ErrorMessages',['Error message for ', _s, ': \n',_errormessage]),
       encodeAtom('Subsequent error messages surpressed ...',_surpressmsg),
       assert(error_queue(ipcmessage(_r,_s,'ERROR_REPORT',[_surpressmsg]))),
       setFlag(remainingErrorQueueSlots,zero),
       !.

/** otherwise: buffer the error message **/
bufferErrorMessage(_rn,_r,_s,_errormessage,_errtermmessage) :-
       'WriteTrace'(low,'ErrorMessages',['Error message for ', _s, ': \n',_errormessage]),
       assert(error_queue(ipcmessage(_r,_s,'ERROR_REPORT',[_errormessage]))),
       assert(error_queue(ipcmessage(_r,_s,'ERROR_REPORT_SHORT',[_errtermmessage]))),
       !.


handle_error_message_queue(ok) :-
	retract(error_queue(ipcmessage(_r,_s,'ERROR_REPORT',[_errormessage]))),
	retract(error_queue(ipcmessage(_r,_s,'ERROR_REPORT_SHORT',[_]))),
	fail.

handle_error_message_queue(error) :-
	retract(error_queue(ipcmessage(_r,_s,'ERROR_REPORT',[_errormessage]))),
	queue_message( ipcmessage(_r,_s,'ERROR_REPORT',[_errormessage]), _ , _ ),
	fail.

handle_error_message_queue(error) :-
	retract(error_queue(ipcmessage(_r,_s,'ERROR_REPORT_SHORT',[_errormessageshort]))),
	queue_message( ipcmessage(_r,_s,'ERROR_REPORT_SHORT',[_errormessageshort]), _ , _ ),
	fail.

handle_error_message_queue(_).


/* Dieses Praedikat sollte vielleicht in GeneralUtilities stehen: */
encodeAtom(_atom,_encodedAtom) :-
	atom(_atom), var(_encodedAtom),
	pc_stringtoatom(_p_atom,_atom),
	encodeIpcString(_p_encodedAtom,_p_atom),
	pc_stringtoatom(_p_encodedAtom,_encodedAtom),
	memfree(_p_encodedAtom),
	!.


/* EncodeCharList erledigt fuer charlists die gleiche Aufgabe wie encodeIpcString fuer C-Strings. Man koennte an dieser Stelle also auch encodeIpcString benutzen.
*/
/*
EncodeCharList( _charlist, _encodedCharlist ) :-
	EncodeInnerCharList(_charlist,_enc1),
	append( [ '"' | _enc1 ], ['"'], _encodedCharlist ).


EncodeInnerCharList( [], [] ) :-
	!.

EncodeInnerCharList( [_a|_b], ['\\',_a | _d] ) :-
	( _a == '\\' ; _a == '"' ),
	!,
	EncodeInnerCharList(_b,_d),
	!.

EncodeInnerCharList( [_a|_b], [_a|_d] ) :-
	EncodeInnerCharList(_b,_d).
*/


/** Text of the various error messages: **/




/** 13-Jul-1993/MJf: the second argument contains the list of */
/** parameters of the error message. It should be of the form */
/** [_1,_2,...]. The parameters then occur as _1,_2, ... in   */
/** the text of the error message (third argument).           */


cb_errorMessage( 'SYNERR',[_1],
                 ['Syntax error ','Unable to parse ',_1,'.']).      /*09-Nov-1989*/

cb_errorMessage( tokensSYNERR2,[_1],
		 ['Syntax error near symbol ',_1,'.']).


cb_errorMessage( 'LINCE',[_1],
                 ['Unable to create ','the object ', _1,'.']).

cb_errorMessage( 'PLNCE',[_1],
                 ['Unable to (system-) ','create: ', _1, '.']).

cb_errorMessage( 'SAIOC1',[_1,_2,_3,_4,_5,_6],
                 ['Object ',_1,' is declared as instance of ',_4,
                  '. Then, Telos requires that ',
                  _2, ' is an instance of ', _5, ' AND ', _3,
                  ' is an instance of ',_6,'.']).

cb_errorMessage( 'ATTRIBUTE_MISMATCH', [_1,_2,_3,_4,_5,_6],
                 [ 'The object ', _1, ' has been declared as',
                   ' subclass of ',_2,'. But its attribute ', _4,
                   ' is not a proper refinement ',
                   'of the corresponding ', 'attribute ', _3,
                   ' because its value ', _6, ' is not a specialization of ', _5,'.']).

cb_errorMessage( 'ATTRIBUTE_UNSPECIALIZED',[_1,_2,_3,_4,_5,_6],
                 [ 'The object ', _1, ' has been declared as',
                   ' subclass of ',_2,'. But its attribute ', _4,
                   ' is not a specialization (isA) ',
                   'of the corresponding ', 'attribute ', _3,
                   '.']).

cb_errorMessage( 'SANC1', [_1,_2,_3,_4,_5],
		 [ 'Object P(',_1,',',_2,',',_3,',',_4,') violates ',
		   'Network constraint 1 ','because there ','exists already ',
		   'an object P(',_5,',',_2,',',_3,',',_4,').']).

cb_errorMessage( 'NAMECONFLICT', [_1,_2],
                 ['The object ', _1, ' already has a different ',
                  'attribute with the same label "',_2,'".']).

cb_errorMessage( 'NON_UNIQUE_ATTRIBUTE', [_1,_2,_3,_4],
                 ['You defined ',_1, ' as a subclass of ', _2, ' (either directly or indirectly). ',
                  'The class ',_2,' has an attribute named ', _4, ' and another superclass ', _3,
                  ' of ',_1, ' (being no super- or subclass of ',_2, ' ) defines a different',
                  ' attribute with the same name. This makes the attribute non-unique for ',
                  _1,'. Please define attribute ',_4,' for ',_1,' explicitly or re-order the',
                  ' specialization hierarchy!']).

cb_errorMessage( 'NOOBJECT', [_1,_2],
		[ _1,' refers to a nonexisting object ',
                  _2,'.']).

cb_errorMessage( 'WRONGSUBCLASS', [_1,_2,_3,_4,_5,_6],
                 ['Object ',_1,' is declared as subclass of ',_4,
                  '. Then, Telos requires that ',
                  _2, ' is a subclass of ', _5, ' AND ', _3,
                  ' is a subclass of ',_6,'.']).

cb_errorMessage( 'WRONGSYSTEMCLASS',[_1,_2,_3],
                 ['The object ',_1, ' cannot be an instance of the ',
                  'system class ',_2, ' since it was ',
                  'declared as an instance of ',_3,'.']).

cb_errorMessage( 'FPWSF',[_1],
                 [ 'Not a good "SMLfragment": ', _1, '.']).

cb_errorMessage( 'FPNTE',[_1],
                 [ 'Unable to transform the object ', _1, '.']).

cb_errorMessage( 'FPACNF',[_1,_2],
                 [ 'Could not find a ', 'fitting attribute class ',
	           'with label ', _2, ' for object ', _1, '.']).

cb_errorMessage( 'FPNSC',[_1,_2],
                 [ 'Unable to declare ', _2, ' as superclass of ',_1, '.']).

cb_errorMessage( 'FPFBL',[_1,_2],
                 [ 'The attribute label "', _2, '" used in ',_1,
                   ' is a reserved word of O-Telos assertion language.']).

cb_errorMessage( 'FPWL',[_1,_2],
                 [ 'A wrong level instantiation: ', 'the class has level ',
		    _2, ' and the instance level ',_1]).

cb_errorMessage( 'FPWLS',[_1,_2],
                 [ 'A wrong level ', 'in an isa relation: ',
		   _1, ' <--> ', _2]).

cb_errorMessage( 'FPTOKINST',[],
                 [ 'Tokens (level 0) may never have ', 'instances.']).

cb_errorMessage( 'PFNFE',[_1],
                 [ 'Unable to find an object ', _1, '. Either misspelled or not defined in the current module at the current rollbacktime.']).

cb_errorMessage( 'SEXPR1',[_1,_2,_3],
		 [ 'Too many or zero solutions of ',_1,_2,_3]).

cb_errorMessage( 'SEXPR2',[_1],
		 [ 'Unable to calculate a select expression for ',_1]).

cb_errorMessage( 'SEXPR3',[_1,_2],
		 [ 'The Object Name ',_1,' is ambigous in this Module context.',
		   'The following objects match : ',_2,
		   ' Please use a Module qualifier to specify an unique name.']).

cb_errorMessage( 'SEXPR4',[_1,_2,_3],
		 [ 'In the current Module context "',_1,
		   '", I cannot see an object ',_2,'@',_3,'. In case you tried to tell this object, use the correct Module context.']).

cb_errorMessage( 'LITUNDEF',[_1,_2],
		 [ 'The literal ', _1, '/', _2, ' is not ', 'defined.']).


cb_errorMessage( 'ERRERR',[],
                 ['Invalid ','call ','of ','an ','error ',
                  'message.']).


/** Error messages for integrated LTassertionCompiler                **/

cb_errorMessage( 'ATAP1',[_1],
                 [ 'Syntax error during parsing ','of assertion ',_1 ]).

cb_errorMessage( 'ATAC1',[_1],
                 ['Classification error : assertion ',_1,
                  'each-declarations are not','allowed in rules.']).

cb_errorMessage( 'ATAC2',[_1],
                 ['Classification error : assertion ',_1,
                  'rules must have a definite','conclusion .']).

cb_errorMessage( 'ATQO1',[_1],
                 ['Semantic error :',
                  'wrong ordering of quantifiers','in assertion ',_1,
                  'all conclusion variables must','be all-quantified ',
                  'scope of all other quantifiers','is limited to condition.']).

cb_errorMessage( 'ATFL1',[_1],
                 ['Semantic error :',
                  'illegal literal in assertion',_1]).

cb_errorMessage( 'ATVD1',[_1],
                 ['Semantic error : assertion ',_1,
                  'variables are not allowed','to be quantified or',
                  'declared more than once.']).

cb_errorMessage( 'ATCV1',[_1],
                 ['Semantic error : assertion',_1,
                  'illegal conclusion literal',
                  'only A, Isa, In',
                  'are allowed .']).

cb_errorMessage( 'FORMULA_UNPARSABLE',[_1],
                ['The formula ',_1, ' contains a syntax error.']).



/*19-Dec-1989\TW:-Many new error messages for UNTELL*/


cb_errorMessage( 'UNTELL1',[_1,_2,_3],
		 ['Unable to delete ', _1, ' as ',
                  _2, ' of ',
                  _3, '. Check spelling.']).

cb_errorMessage( 'UNTELL2',[_1,_2,_3,_4],
		 ['The object ', _1, ' has no attribute ',
                  'with label ',
                  _2, ' and destination ',
                  _3, ' and valid time ', _4,'.']).

cb_errorMessage( 'UNTELL3',[_1,_2,_3,_4],
		 ['Error  because ', _1, ' ',
                  'is destination in Proposition ',
                  _2,' with label ',_3,
                  ' and source ',_4, ' :',
                  'InstanceOf_constraint_1 is violated.']).

cb_errorMessage( 'UNTELL4',[_1,_2,_3,_4],
		 ['Error because ', _1, ' ',
                  'is source of ',
                  'Proposition ',_2,' ',
                  'with label ',_3,' ',
                  'and destination ',_4,': ',
                  'InstanceOf_constraint_1 ',
                  'is violated.']).

cb_errorMessage( 'UNTELL5',[_1,_2,_3,_4,_5],
		 [_1, ' can not be untold, because ',
                  _2,' is source of ',
                  _3,' and',_4,' is source of ',_5,
                  ' and ',_3,' is instance of',_5 ,'.']).

cb_errorMessage( 'UNTELL6',[_1],
		 ['Object ', _1, ' ',
                  'does not exist as actual proposition.']).

cb_errorMessage( 'UNTELL7',[_1,_2,_3],
		 ['Can not untell the relation ',
                  _1,' ',_2,' ',_3,'. ',
                  'Either ',_2,'is a wrong relation ',
                  'or a basic-relation','.']).

cb_errorMessage( 'UNTELL8',[_1,_2,_3,_4,_5],
		 [_1, ' can not be untold ',
                  'because ', _2, ' ',
                  'is destination of ', _3,' and ',
                  _4,' is destination of ',
                  _5,' and ',_3, ' ',
                  'is instance of ',_5, '.']).

cb_errorMessage( 'UNTELL9',[_1,_2,_3,_4,_5,_6,_7],
		 [_1, ' can not be untold, because ', _2,' is destination of ',
                  _3,' and',_4,' is destination of ',_5,
                  ' and ',_3,' and',_5,
                  ' are attributes of',_6,' and',_7,
                  ' which are isA-related.',
                  ': IsA_constraint_1 ',
                   'is violated.']).

cb_errorMessage( 'UNTELL10',[_1,_2,_3,_4,_5,_6],
		 [_1, ' can not be untold', ' ',
                  'because ', _2,' has instance ',
                  _3,' and ',_4,' is attribute of ',_3,' ',
                  'and instance of ',_5,' and ',_5, ' ',
                  'is attribute of ',_6,
                  ' : InstanceOf_constraint_1 ',
                   'is violated.']).

cb_errorMessage( 'UNTELL10b',[_1,_2,_3,_4,_5,_6,_7,_8],
		 [_1, ' can not be untold', ' ',
                  'because ', _2,' has instance ',
                  _3,' and ',_4,' is attribute of ',_8,' ',
                  'and instance of ',_5,' and ',_5, ' ',
                  'is attribute of ',_7,' and ',_2,
				  ' is no longer specialization of ',_6,
                  ' : InstanceOf_constraint_1 ',
                   'is violated.']).

/** 12-Jan-2005/M.Jeusfeld: This is obsolete! It is not an O-Telos axiom that
objects participating in an ISA relationship must have the same 'level'
cb_errorMessage( UNTELL11,[_1,_2,_3,_4],
		 [_1, ' can not be untold, because ',
                  _2,' is specialization of ',
                  _3,' and must have level ',_4,
                  '.']).
**/

cb_errorMessage( 'UNTELL12',[_1,_2,_3,_4],
		 [_1, ' can not be untold, because ',
                  _2,' is superclass of ',
                  _3,' and must have level ',_4,
                  '.']).

/*3-Jan-1990/MJf: */
cb_errorMessage( 'UNTELL13', [_1,_2,_3,_4,_5],
		 ['P(',_1,',',_2,',',_3,',',_4,')  cannot',
                  ' be deleted in the current module context.',
                  ' Switch to module ', _5,
                  ' for untelling the object.']).

/*3-Jan-1990/MJf: */
cb_errorMessage( 'UNTELL14',[_1,_2],
		 ['Unable to untell attribute ',_1, ' in class ',_2]).

/*30-Oct-2018/MJf: */
cb_errorMessage( 'UNTELL15',[_1],
		 ['Unable to purge module ',_1]).

/*30-Oct-2018/MJf: */
cb_errorMessage( 'UNTELL16',[_1],
		 ['Current module has a sub-module ',_1]).

/*30-Oct-2018/MJf: */
cb_errorMessage( 'UNTELL17',[_1],
		 ['Unable to delete proposition ',_1]).



/* * ERROR MESSAGES FOR QUERYPROCESSOR **/

/** This error message is no longer needed
cb_errorMessage(QLERR1,[_1],
		['Rollback time FromNowOn is only allowed',
		 'for the evaluation of permanently',
		 'stored queries. Check queries in ',_1]).
**/

cb_errorMessage('QLERR2',[_1,_2],
		['Object ',_1,' in Expression ',_2,
		 'is not an instance of GenericQueryClass.']).

cb_errorMessage('QLERR3',[_1],
		['Object ',_1,' must be an instance of QueryClass or Class to be queried.']).

cb_errorMessage('QLERR4',[_1,_2],
		['Object ',_1,' in DeriveExpression ',_2,
		 'is not a permanently stored instance',
		 'of GenericQueryClass.']).

cb_errorMessage('QLERR5',[_1],
		['Object ',_1,' is not a permanently stored instance',
		 'of QueryClass.']).

cb_errorMessage('QLERR6',[_1],
		['Unknown or forbidden query call ',_1]).

cb_errorMessage('QLERR7',[_1],
		['Input object ',_1,' does not represent a well formed',
		 'instance of QueryClass.']).

cb_errorMessage('QLERR8',[_1],
		['Syntax error in constraint of',_1]).

cb_errorMessage('QLERR9',[_1],
                            ['Queries cannot be compiled incrementally. Before ',
                               'changing query ',_1,' you must untell its complete ',
                               'definition.']).

cb_errorMessage('QLERR10',[_1],
                            ['Queries cannot be compiled incrementally. If you',
                               ' want to reach a recompilation of ',_1, ' you should',
                               ' either untell its complete definition or at least its ',
                               'instantiation link to QueryClass.']).

cb_errorMessage('QLERR11',[_1],
                            ['The query ',_1,' is not a specialization of any class,',
							 ' but you used retrieved_attribute in the query definition. ',
							 'This is prohibited.']).


cb_errorMessage('QLERR12',[_1],
         [_1,' must be an instance of Function (COUNT,PLUS,...) when used as argument in comparison literals.']).


cb_errorMessage('QLERR13',[_1],
		['A parameterized query call is not allowed here: ',_1]).


cb_errorMessage('QLERR14',[_1],
		['The query ',_1, ' has not enough properties (superclass,...) to be compiled.']).


/* Error message(s) of the IntegrityChecker:                                    */

cb_errorMessage( 'SIERR',_l,
		 ['The following ', 'attributes ', 'were used but ',
		  'never specified: ', _l, '.']).

cb_errorMessage( 'BDMCpFindClass1',[_1,_2],
		 ['The formula ', 'compiler ', 'can not find ',
		  'the class ', _1, '!', _2, '.']).

cb_errorMessage( 'BDMCpFindClass2',[_1,_2],
		 ['The formula ', 'compiler ', 'can not find ',
		  ' a class with ', 'the attribute ', _2 , 'so that ',
                  ' the object ', _1, ' is instance of it.']).

cb_errorMessage( 'WRONG_IC',[_1,_2,_3],
		 ['The new integrity constraint ', _1, ': ', _2, ' defined in module ', _3, ' is not satisfied by',
                  ' the database in the current module.']).

cb_errorMessage( 'WRONG_RULE',[_1],
		 ['The new rule ', _1, ' caused an integrity error.']).

cb_errorMessage( 'RULE_NEEDED',[_1],
		 ['Integrity error ', 'because of ', 'the deletion ',
                  'of the rule ', _1, '.']).

cb_errorMessage( 'OBJECT_INCONSISTENT',[_1,_2],
		 ['The integrity violation ', 'is caused by telling ', '(',_1,
                  ' in ',  _2, ').']).

cb_errorMessage( 'OBJECT_NEEDED',[_1,_2],
		 ['The integrity violation ', 'is caused by untelling ', '(',
                  _1,' in ', _2, ').']).

cb_errorMessage( 'DUEtoRULE',[_1],
		 ['The application ', 'of the rule ', _1,
		  ' leads to an ', 'integrity error.']).

/** Parameter 1 is 'Insert' or 'Delete' */
cb_errorMessage( 'IC_VIOLATION',['Insert',_2,_3,_4,_5],
		 ['Telling the fact ', _2,
		  ' (or making it derivable) clashes with the ',_5,' constraint of class ',_4,'. ', _3]).

cb_errorMessage( 'IC_VIOLATION',['Delete',_2,_3,_4,_5],
		 ['Untelling the fact ', _2,
		  ' (or making it underivable) violates the ',_5,' constraint of class ',_4,'. ', _3]).

/** Make more readable output when argument 2 is '$FALSE$' **/
cb_errorMessage( 'IC_NOTSAT',[_1,'$FALSE$'],
                 ['The expression ',_1,
                  ' is true in the updated database. This violates an integrity constraint!']) :- !.

/** This is the original error message form: **/
cb_errorMessage( 'IC_NOTSAT',[_1,_2],
                 ['The updated database regards ',_1,
                  ' as a true statement. In such cases an integrity constraint',
                  ' requires that another statement, ', _2, ', is also true.',
                 ' But that is not the case here!']).

/** Error message for IC violations that were not of the simplified form F1 ==> F2 **/
cb_errorMessage( 'IC_NOTSAT1',[_1],
                 ['The expression ',_1,
                  ' is violated by the updated database.']).



cb_errorMessage( 'MCNODIR',[_1],
		 ['There is ','no directory ','with path ' ,_1,'.'] ).

cb_errorMessage( 'MCSYSPWD',[],
		 ['You are ','not allowed ','to change ' ,
		  'the system application ','with this Server.'] ).

cb_errorMessage( 'MCIOERR',[_1],
                 ['I/O error: ','Unable to load the file ',_1,'.']).


/* Error message(s) of the assertion compiler: */


cb_errorMessage( 'NORULE',[_1],
                 ['Rule ', _1,' could not be compiled.']).

cb_errorMessage( 'NORULE2',[_1],
                 ['The current rule has an illegal format.']).

cb_errorMessage( 'NOSPEC',[_1,_2],
                 ['The formula ',_1,' could not be specialized for literal ',
                   _2, '. Probably, no concerned class was found.']).

cb_errorMessage( 'ASSSYNERR3',[_1],
		 ['You specified the conclusion ',_1,'.\nBut the conclusion literal must be either In/2 or A/3']).

cb_errorMessage( 'ASSSYNERR4',[_1,_2],
		 ['The variables ',_1, ' occuring in the conclusion literal must be forall-quantified over the whole rule, i.e. occur in ',_2,'.']).

cb_errorMessage( 'ASSSYNERR5',[_1],
		 ['The identifier ',_1,' must be used either as a constant or as a variable.']).

cb_errorMessage( 'ASSSYNERR6',[_1,_2],
		 ['Variable ',_1,' already used in other scope with type ',_2,'.']).


/* Error message(s) of the (new) Telos parser: */

cb_errorMessage( 'TELOSPARSEERR',[_1,_2,_3],
                 ['Syntax error ',_1,' in line ',_2,
		  ', parser message:\n',_3]).

cb_errorMessage('IPCERROR',[],['Error on reading an IPC-Message']).

cb_errorMessage('NOCONCERNEDCLASS',[_1],['No class found concerning ',_1,'.']).

cb_errorMessage('NOCONCERNEDCLASS',[_1,_2,_3],['No class of ',_1,' defines an attribute ',_2,
                '. The predicate ',_3,' is thus undefined.']).

cb_errorMessage('ERRORSELECTEXP',[_1],['Unable to replace the select expression ',_1]).

cb_errorMessage('NO_MAGIC_META',[_1],['The metaformula functionality is currently not implemented for Magic-Set rules. Rule ' ,_1,' is a Magic-Set rule and a Metaformula']).

cb_errorMessage('INSERT_AND_DELETE_REQUEST',[_1,_2],['During evaluation of the Metaformula-Trigger an attempt was made to insert and to delete assertions at the same time. Original mode: ',_1,' ; Error mode: ',_2]).

cb_errorMessage('MSERR0',[_1],['The Metsimplifier failed to process the metaformula ',_1]).


cb_errorMessage('MSERR1',[_1],['The Metaformula to be processed contains the following literals whose arguments contain Class-Variables: ',_1,'. This list contains an A-Literal, whose first argument is a class-Variable and cannot be bound.']).

cb_errorMessage('MSERR2',[_1],['The range-formula ',_1,' cannot be converted into a partially evaluatable formula by the system.']).

cb_errorMessage('MSERR3',[_1],['The meta-level variables ',_1,' cannot be instantiated using partial evaluation. A reason could be that the variables ', _1, ' are "exists"-quantified. ',
'Rewrite the formula or use a query class instead of an integrity constraint.']).

cb_errorMessage('MSERR4',[_1],['The meta-level variables cannot be instantiated using partial evaluation of
 literals ',_1,' because the cost-model prohibits any path suggested.']).


cb_errorMessage('TGF1',[_1],['The generated formula ', _1,' cannot be told.']).

cb_errorMessage('HMF1',[_1],['Error when processing Metaformula ', _1]).

cb_errorMessage('ISA_AXIOM',[_1,_2,_3,_4],[_4,' is specialization of ',_3,' ,Then Telos requires that ',_1,' is NOT a specialization of ',_2]).

cb_errorMessage('NOTVIEW',[_1,_2,_3],['The value ',_1,' of attribute ',_2,' of object ',_3,
									' can only be used for instances of View,',
									' but ',_3,' is not instance of View.']).


cb_errorMessage( 'ECA0',[_1,_2],
                 ['Violation of ECA rule ', _2, ' by event ',_1,'.\n']).

cb_errorMessage( 'ECA1',[_1,_2],
                 ['Violation in ask part of ECA rule ', _2, ' by event ',_1,'.\n']).

cb_errorMessage( 'ECA2',[_1,_2],
                 [_1, ' rejects current transaction.\n']).

cb_errorMessage( 'ECA3',[_1,_2],
	['Object ',_1,' is not an instance of QueryClass. Rule : ',_2,' .\n']).

cb_errorMessage( 'ECA4',[_1],
	['ECARULE ',_1,' can not be untelled.\n']).

cb_errorMessage( 'ECA5',[_1],
	['ECARULE ',_1,' does not exist.\n']).

cb_errorMessage( 'ECA6',[_1],
	['No regular defition of priority list in Rule ',_1,' .\n']).

cb_errorMessage( 'ECA7',[_1,_2,_3],
	['Priority error in ECArule ', _1, '.\n', _2, ' < ',_3,' and ',_3,' < ', _1,'\n==> ',_2,' < ', _1,', but you define ',_1,' < ',_2,' .\n']).

cb_errorMessage( 'ECA8',[_1],
	['ECArule ',_1,' exists. if you want to tell a new rule, first you must untell it.']).

cb_errorMessage( 'ECA9',[_1,_2],
	['ECArule ',_2,' in priority definition of ', _1,' does not exist.\n']).

cb_errorMessage( 'ECA10',[_1],
	['Bad query format in ASK : ',_1,' .\n']).


cb_errorMessage( 'ECA11',[_1],
	['Undefined variables in  ECArule : ',_1,' .\n']).

cb_errorMessage( 'ECA12',[_1,_2],
	['bad query format: ',_2,' variable as a parameter attribute in rule : ',_1,' .\n']).

cb_errorMessage( 'ECA13',[_1,_2,_3],
	['Error :dynamic cycle depth of ',_1,' is ',_2,' by event ',_3,' .\n']).

cb_errorMessage( 'ECA14',	[_n],
	['Error : ECA-Rule ',_n,' refers',' to one or more object names that',' do not exist.\n']).		/* LWEB */

cb_errorMessage( 'ECA15',[_1,_2],
                 [_1, ' rejects current transaction due to error in ',_2, '.\n']).

cb_errorMessage('ECA_TELL_ERR',[_n],
        ['Unable to tell ECArule ',_n,'.']).

cb_errorMessage('ECA_INV_EVENT',[_ecarule,_op,_lit],
	['Invalid event in ECA rule ',_ecarule,':\nOperation:',_op,'\nLiteral:',_lit]).

cb_errorMessage('ECA_INV_CONDITION',[_ecarule,_op],
	['Invalid condition in ECA rule ',_ecarule,':\nCondition:',_op]).

cb_errorMessage('ECA_INV_ACTION',[_ecarule,_op,_lit],
	['Invalid action in ECA rule ',_ecarule,':\nOperation:',_op,'\nLiteral:',_lit]).

cb_errorMessage('ECA_INCORRECT_VARIABLES',[_ecarule],
	['Incorrect use of variables in ECArule ',_ecarule,'! Condition and Ask actions may have only at most one
	free variable, other actions like TELL are not allowed to contain any free variable.']).

cb_errorMessage('ECA_TELL_UNTELL',[_ecarule,_tell,_untell],
	['The event of the ECA rule ',_ecarule,' is ',_tell,' and an action of the rule is',
	_untell,', but it is not allowed to use Tell as event and Untell as action on the same object in an ecarule together!']).

cb_errorMessage('ECA_INV_UPDATE',[_ecaid],
	['The ECA rule ',_ecaid,' can not be updated. Only updates on the active attribute are allowed.']).

cb_errorMessage('ECA_REJECT',[_eca,_msg],
	[_eca,': ',_msg]).

cb_errorMessage( 'MOD1',	[_n],
	['Error : Trying to set context to non-existing module ',_n,'.\n']).		/* LWEB */

cb_errorMessage( 'MOD2',	[_n],
	['Parse Error:  while parsing module directive. \n']).		/* LWEB */

cb_errorMessage( 'MOD3',	[_u,_m],
	['User ', _u,' was denied to switch to module ', _m, '.\n']).		/* M.Jeusfeld*/

cb_errorMessage( 'NPERMERR', [_u,_o,_r],
        ['User ', _u,' is not permitted for ',_o,' on resource ', _r, '.\n']).

cb_errorMessage('SET_MODULE_ERROR',[_1],['Double module directive occured at line ',_1,'.']).  /* LWEB */

cb_errorMessage('STRATIFICATION_VIOLATION1',[_sign,_pred,_argkey],
        ['To prove that ',_sign,_pred,_argkey,' is true one has to assume that its negation is also true. ',
         'This is paradoxical.']).

cb_errorMessage('STRATIFICATION_VIOLATION2',[_sign,_pred,_argkey1,_argkey2],
        ['The predicate ',_sign,_pred,_argkey2,' does not contain the paradoxical fact ',
        _pred,_argkey1,' that was derived in an earlier negated call.']).

cb_errorMessage('STRATIFICATION_VIOLATION3',[_sign,_pred,_argkey1,_argkey2],
        ['The predicate call ',_sign,_pred,_argkey2,' contains the paradoxical fact ',
        _pred,_argkey1,' that could not be derived in an earlier negated call.']).

cb_errorMessage('STRATIFICATION_VIOLATION4',[_sign,_pred,_argkey],
        ['The negated predicate ',_sign,_pred,_argkey,' is called recursively and cannot be shown to be true or false.']).

cb_errorMessage('QUERYCLASS_NOT_IN_IC',[_qclass,_formtype,_f],
       ['The query class ',_qclass,' can only occur in ', _formtype,' ',_f,
        ' if it is an instance of MSFOLrule.']).

cb_errorMessage('GENERIC_QUERYCLASS_NOT_IN_IC',[_qclass,_formtype,_f],
       ['The parameterized query class ',_qclass,' is not allowed in ', _formtype,' ',_f, '.']).

cb_errorMessage('WRONG_MODULE',[_op,_obj,_m1,_m2],
       ['The operation ',_op,' on ', _obj,
        ' was attempted in module ',_m1,
        '. Switch to module ', _m2,' and try again ...']).

cb_errorMessage('NO_RENAME',[_user,_module,_object],
       ['You are not authorized to rename ', _object,
        ' in module ',_module,'.']).

cb_errorMessage('DIVBYZERO',[],
       ['Attempt to divide by zero occurred.']).

cb_errorMessage('NOTALLNUMBERS',[],
       ['Argument class of aggregation function contains instances that are not numbers.']).

cb_errorMessage('METAQUERY_ERROR',[_q],
       ['The query ',_q,' has meta variables or meta predicates in its constraint',
        ' and it is defined as instance of MSFOLrule. This is not supported by',
        ' ConceptBase since such queries are not partially evaluated. Redefine',
        ' the query as a regular class with a deductive rule.']).

cb_errorMessage('NOFUNCTIONBODY',[_fun],
       ['The function call ', _fun, ' has no implementation!']).

cb_errorMessage('INDIVCC',[_cc],
       ['The label ',_cc, ' belongs to an individual object (not an attribute). ', 'It may not be used as label ',
        'of an attribution predicate.']).

cb_errorMessage('DERIVEARG',[_q],
       ['The formula contains an attribution predicate with a query call argument: ',_q]).

cb_errorMessage('FILELOADERR',[_1],['File ',_1,' could not be loaded.']). 

cb_errorMessage('MISSOBJ2',[_1,_2],['One of the following parameters is not a known object: ',_1,',',_2]). 

cb_errorMessage('MISSOBJ4',[_1,_2,_3,_4],['One of the following parameters is not a known object: ',
                                        _1,',',_2,',',_3,',',_4]). 

cb_errorMessage('FNILL',[_1],['Unable to extract alphanumeric filename from query ',_1]). 

cb_errorMessage('NO_EDB_QC',[_1,_2],['The object ',_1, ' may not be an explicit instance of the query class ', _2]). 

cb_errorMessage('WRONGVARLABEL',[_1],['The label \'',_1, '\' is a reserved word for formulas and may not be used as variable name.']). 

cb_errorMessage('ECA_INVALID_EVENTLIT',[_1],['The predicate ',_1, ' in the ON-part could not be assigned to a class attribute. Check the attribute label and the class memberships of the first argument.']). 

cb_errorMessage('IPC1',[],['Malformed method call.']). 

cb_errorMessage('IPC2',[],['Malformed ipcmessage.']). 





