
                  CONCEPTBASE.CC
                        by
               The ConceptBase Team
                   represented by
 * Manfred Jeusfeld, University of Skovde, 54128 Skovde, Sweden

               http://conceptbase.cc



This directory contains binaries and documentation of
the deductive object base manager

                ConceptBase.cc 

The system is distributed under a FreeBSD license, see
   CB-FreeBSD-License.txt
That file file must be included in any redistribution of the system!

Please refer to the file 
  doc/TechInfo/InstallationGuide.txt
for information on how to install and start the system.

Refer to the file
  doc/TechInfo/ReleaseNotes.txt
for detailed release notes of this version of ConceptBase.

The licenses for third-party components included in ConceptBase
are listed in
  doc/ExternalLicenses


Additional documentation (e.g. UserManual and Tutorial)
can be found in the directory "doc" and its subdirectories, or
online at
  http://conceptbase.cc


After a successful installation, you can integrate ConceptBase.cc into
your desktop environment, in particular set-up the graph editor to
be the default application for ConceptBase graph files. Instructions are
at
  http://conceptbase.sourceforge.net/CB-Mime.html


