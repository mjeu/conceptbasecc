Contributors to the ConceptBase.cc system include Masoud Asady, Lutz Bauer (module system), Markus Baumeister, Ulrich Bonn, 
Rainer Gallersdörfer (object store), Michael Gebhardt (user interface), Dagmar Genenger, Michael Gocek, Rainer Hermanns, 
Stefan Eherer, Matthias Jarke, Manfred Jeusfeld (CB server, logic foundation, function component), David Kensche, André Klemann, 
Eva Krüger (integrity component), Rainer Langohr, Farshad Lashgari (active rules), Tobias Latzke, Xiang Li, Yong Li, 
Thomas List (object store), Andreas Miethsam, Hans Nissen (module system), Martin Pöschmann, Christoph Quix (CB server, view component), 
Christoph Radig (object store), Tobias Schöneberg, Achim Schlosser, René Soiron (optimizer), Martin Staudt (CB server, query component), 
Kai von Thadden (query component), Hua Wang (answer formatting), Claudia Welter, Thomas Wenig. Some modules to which team members 
have contributed may have been removed from the source pool in the meantime.

ConceptBase implements a variant of the Telos knowledge representation language designed at the University of Toronto and partner
universities. We thank the designers of Telos, in particular Alex Borgida, John Mylopoulos, Sol Greenspan, Martin Stanley, and Manolis Koubarakis.

The ConceptBase Team can be contacted via Manfred Jeusfeld (manfred-dot-jeusfeld-at-acm-dot-org). 